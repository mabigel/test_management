import org.uszeged.inf.optimization.data.*;
import org.uszeged.inf.optimization.algorithm.optimizer.*;
import org.uszeged.inf.optimization.algorithm.optimizer.global.*;
import org.uszeged.inf.optimization.functions.*;
import org.uszeged.inf.optimization.util.Logger;
import org.uszeged.inf.optimization.util.FunctionTransform;

import org.apache.commons.cli.*;

import java.io.*;
import java.util.StringTokenizer;
import java.util.Arrays;
import java.lang.reflect.*;

import java.util.concurrent.atomic.AtomicLong;

class Calculate {

	File xmlFile;
	File bndFile;

	private static double lb[] = {}, ub[] = {};
	private static Function objectiveFunction = null;
	private static GlobalOptimizer<Point> optimizer;
	private static double globalOpt = Double.NaN;
	private static boolean hasGlobalOpt;
	private static int dim;
	private static String functionClass;

	private static void createFunction(String functionName) {

		Function f;

		try{
			objectiveFunction = (Function)Class.forName(functionName).newInstance();
			return;
		} catch(Exception e){}

		try{
			objectiveFunction = (Function)Class.forName("org.uszeged.inf.optimization.functions."+functionName).newInstance();
			return;
		} catch(Exception e){}

		Logger.error(Calculate.class,"Could not instantiate objective function \"{0}\"",functionName);
		throw new NullPointerException("Could not instantiate objective function \""+functionName+"\"");
	}

	public static void readBNDFile(String fileName) {

		BufferedReader br = null;

		try {
			String line;
			br = new BufferedReader(new FileReader(fileName));

			String printableFunctionName = br.readLine();
			Logger.info(Calculate.class,"readBNDFile(String) function name: {0}", printableFunctionName);

			functionClass = br.readLine();
			Logger.info(Calculate.class,"readBNDFile(String) function class: {0}", functionClass);


			dim = Integer.parseInt(br.readLine());
			Logger.info(Calculate.class,"readBNDFile(String) dimension: {0}", String.valueOf(dim));

			lb = new double[dim];
			ub = new double[dim];
			StringTokenizer st;

			st = new StringTokenizer(br.readLine(), " \t");
			if (st.countTokens() == 1){
				// bounds are uniform on all dimensions
				double lbval = Double.parseDouble(st.nextToken());
			 	double ubval = Double.parseDouble(br.readLine());
				Logger.info(Calculate.class,"readBNDFile(String) uniform bounds: [{0}, {1}]",
					String.valueOf(lbval),
					String.valueOf(ubval)
					);
				for(int i = 0; i < dim; i++) {
					lb[i] = lbval;
					ub[i] = ubval;
				}
			} else if (st.countTokens() == 2){
				// bounds specified for every dimension
				for(int i = 0; i < dim; i++) {
					if (i != 0){
						st = new StringTokenizer(br.readLine(), " \t");
					}
					lb[i] = Double.parseDouble(st.nextToken());
					ub[i] = Double.parseDouble(st.nextToken());
					Logger.info(Calculate.class,"readBNDFile(String) dim {2} bounds: [{0}, {1}]",
						String.valueOf(lb[i]),
						String.valueOf(ub[i]),
						String.valueOf(i+1)
						);
				}
			} else {
				Logger.error(Calculate.class,"readBNDFile(String) invalid bound format");
				throw new IllegalArgumentException("invalid bound format");
			}

			hasGlobalOpt = false;
			String opt = br.readLine();
			if (opt != null && opt.length() > 0){
				globalOpt = Double.parseDouble(opt);
				hasGlobalOpt = true;
			}
		} catch (IOException e) {
			System.out.println(e);
		} finally {
			try {
				br.close();
			} catch (IOException ex) {
				System.out.println(ex);
			}
		}
		return;
	}

	public static void main(String[] args) {
		Logger.setMinimumLogLevel(Logger.Level.INFO);
		Options options = generateOptions();

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("java -jar global.jar -cp libs/* Calculate [<args>...]", options);
            System.exit(1);
            return;
        }

		if (cmd.hasOption("loglevel")){
			String level = cmd.getOptionValue("loglevel").toUpperCase();
			Logger.Level l = Logger.Level.valueOf(level);
			if (l != null){
				Logger.setMinimumLogLevel(l);
			} else {
				Logger.warnObvious("Log level {0} is not recognized, INFO level used", level);
			}
		}

		createLogDirectory();
		try{
			Logger.setLogChannel("logs/GLOBAL_");
		}catch(UnsupportedEncodingException e){
			Logger.warnAttention(Calculate.class,"main(String[] args) unsupported character encoding");
		}catch(FileNotFoundException e){
			Logger.warnAttention(Calculate.class,"main(String[] args) log directory does not exits");
		}

		if (cmd.hasOption("hardness")) {
			ConstrainedFunction.longRun = Integer.parseInt(cmd.getOptionValue("hardness"));
		}

		String xmlFile = cmd.getOptionValue("optimizer");
		String bndFile = cmd.getOptionValue("function");

		Logger.info(Calculate.class,"main(String[]) optimizer file: {0}", xmlFile);
		Logger.info(Calculate.class,"main(String[]) function file: {0}", bndFile);

		readBNDFile(bndFile);

		Logger.info(Calculate.class,"main(String[]) random seed: {0}", String.valueOf(Vector.getRandSeed()));

		AbstractOptimizer.Builder optimizerBuilder = new AbstractOptimizer.Builder();
		optimizer = (GlobalOptimizer<Point>)optimizerBuilder.build(xmlFile);

		optimizer.setLowerBoundOfSearchSpace(new Vector(lb));
		optimizer.setUpperBoundOfSearchSpace(new Vector(ub));

		createFunction(functionClass);
		FunctionTransform.init(dim);


		optimizer.setObjectiveFunction(objectiveFunction);

		if (hasGlobalOpt) {
			globalOpt = FunctionTransform.singleton().rotValue(globalOpt);
			optimizer.setKnownGlobalOptimumValue(globalOpt);
		}
		optimizer.restart();
		optimizer.run();

		double value = FunctionTransform.singleton().inverseRotValue(
			optimizer.getOptimumValue()
			);

		System.out.println(
				  optimizer.getNumberOfFunctionEvaluations()
			+" "+ (optimizer.getRunTime()/1000000) // output in millisecs
			+" "+ value
			+" "+ optimizer.getRunStatistics()
			);
	}

	public static Options generateOptions(){
		Options options = new Options();

		Option opt;

		opt = new Option("o", "optimizer", true, "optimizer structure xml file");
		opt.setRequired(true);
		options.addOption(opt);

		opt = new Option("f", "function", true, "test function bnd file");
		opt.setRequired(true);
		options.addOption(opt);

		opt = new Option("l", "loglevel", true, "log level {TRACE|DEBUG|INFO|ERROR|SILENT}");
		opt.setRequired(false);
		options.addOption(opt);

		opt = new Option("h", "hardness", true, "the function evaluation is done 10^hardness times");
		opt.setRequired(false);
		options.addOption(opt);

		return options;
	}

	public static void createLogDirectory(){
		File theDir = new File("logs");

		// if the directory does not exist, create it
		if (!theDir.exists()) {

		    try{
		        theDir.mkdir();
		    }catch(SecurityException se){
		        Logger.warnAttention(Calculate.class,"createLogDirectory() \"logs\" directory not found and cannot be created");
		    }
		}
	}
}
