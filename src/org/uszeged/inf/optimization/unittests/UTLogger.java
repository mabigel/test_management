package org.uszeged.inf.optimization.unittests;

import org.uszeged.inf.optimization.util.Logger;
import java.io.PrintStream;
import java.io.OutputStream;

public class UTLogger{
    public static void main(String[] args){
        main();
    }
    public static void main(){
        int errorCount = 0;
        PrintStream backupPS = Logger.getLogChannel();
        Logger.Level backupMLL = Logger.getMinimumLogLevel();
        boolean error = true,state;

        do{
            UTOPrintStream utoPS = new UTOPrintStream(new UTOOutputStream());
            System.out.println("TESTMSG: Unit test of Logger class");



            // setLogChannel function
            do{try{error = true;
                System.out.println();
                System.out.println("TESTMSG: Test setLogChannel() function");



                // set new channel test
                System.out.println("TESTMSG: set new channel test");
                Logger.setMinimumLogLevel(Logger.Level.TRACE);
                Logger.setLogChannel(utoPS);
                Logger.logString(Logger.Level.SILENT,"Test1");

                // no text printed on new channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[SILENT] Test1\".");
                    break;
                }

                // set new channel test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



            error = false;}catch(Exception e){
                errorCount++;
                System.out.println("# TESTERROR: no exception expected, caught: "
                    +e.toString()+" "+e.getMessage());
            }}while(false);

            if (error){
                System.out.println("# TESTERROR: couldn't initialize with dependency injection further test cannot pass");
                break;
            }




            //log Function
            do{try{error = true;
                System.out.println();
                System.out.println("TESTMSG: Test log() function");



                // simple print test
                System.out.println("TESTMSG: simple print test");
                Logger.log(Logger.Level.SILENT,null,"Test1",new String[0]);

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[SILENT] Test1\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[SILENT] Test1")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[SILENT] Test1\", got \""+utoPS.text+"\"");
                    break;
                }

                // simple print test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



                // print with class test
                System.out.println("TESTMSG: print with class test");
                Logger.log(Logger.Level.SILENT,Logger.class,"log() Test1",new String[0]);

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[SILENT] Logger.log() Test1\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[SILENT] Logger.log() Test1")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[SILENT] Logger.log() Test1\", got \""+utoPS.text+"\"");
                    break;
                }

                // print with class test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



                // print with object class test
                System.out.println("TESTMSG: print with object class test");
                Logger.log(Logger.Level.SILENT,new Logger(),"log() Test1",new String[0]);

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[SILENT] Logger.log() Test1\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[SILENT] Logger.log() Test1")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[SILENT] Logger.log() Test1\", got \""+utoPS.text+"\"");
                    break;
                }

                // print with object class test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



                // on log level test
                System.out.println("TESTMSG: on log level test");
                Logger.setMinimumLogLevel(Logger.Level.INFO);
                Logger.log(Logger.Level.INFO,Logger.class,"log() Test1",new String[0]);

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[INFO] Logger.log() Test1\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[INFO] Logger.log() Test1")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[INFO] Logger.log() Test1\", got \""+utoPS.text+"\"");
                    break;
                }

                // on log level test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



                // above log level test
                System.out.println("TESTMSG: above log level test");
                Logger.setMinimumLogLevel(Logger.Level.INFO);
                Logger.log(Logger.Level.ERROR,Logger.class,"log() Test1",new String[0]);

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[ERROR] Logger.log() Test1\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[ERROR] Logger.log() Test1")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[ERROR] Logger.log() Test1\", got \""+utoPS.text+"\"");
                    break;
                }

                // above log level test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



                // below log level test
                System.out.println("TESTMSG: below log level test");
                Logger.log(Logger.Level.TRACE,Logger.class,"log() Test1",new String[0]);

                // text printed on channel
                if (utoPS.text != null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text print expected, got \""+utoPS.text+"\"");
                    break;
                }

                // below log level test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



                // correct arguments 1 test
                System.out.println("TESTMSG: correct arguments 1 test");
                Logger.log(Logger.Level.ERROR,Logger.class,"log() Test {0}",new String[]{"arg1"});

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[ERROR] Logger.log() Test arg1\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[ERROR] Logger.log() Test arg1")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[ERROR] Logger.log() Test arg1\", got \""+utoPS.text+"\"");
                    break;
                }

                // correct arguments 1 test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



                // correct arguments 2 test
                System.out.println("TESTMSG: correct arguments 2 test");
                Logger.log(Logger.Level.ERROR,null,"{1} is {0}",new String[]{"good!","Test"});

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[ERROR] Test is good!\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[ERROR] Test is good!")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[ERROR] Test is good!\", got \""+utoPS.text+"\"");
                    break;
                }

                // correct arguments 2 test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



                // not enough arguments test
                System.out.println("TESTMSG: not enough arguments test");
                state = true;
                try{
                    Logger.log(Logger.Level.ERROR,null,"{1} is {0}",new String[]{"good!"});
                }catch(IndexOutOfBoundsException e){
                    state = false;
                }

                // no exception thrown
                if (state){
                    errorCount++;
                    System.out.println("# TESTERROR: thrown IndexOutOfBoundsException expected");
                    break;
                }

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: text print expected");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[ERROR] log() argument index (1) out of bounds")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[ERROR] log() argument index (1) out of bounds\", got \""+utoPS.text+"\"");
                    break;
                }

                // not enough arguments test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



                // incorrect argument format test
                System.out.println("TESTMSG: incorrect argument format test");
                state = true;
                try{
                    Logger.log(Logger.Level.ERROR,null,"{0} is {a0}",new String[]{"good!"});
                }catch(NumberFormatException e){
                    state = false;
                }

                // no exception thrown
                if (state){
                    errorCount++;
                    System.out.println("# TESTERROR: thrown NumberFormatException expected");
                    break;
                }

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: text print expected");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[ERROR] log() invalid argument index format (a0)")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[ERROR] log() invalid argument index format (a0)\", got \""+utoPS.text+"\"");
                    break;
                }

                // incorrect argument format test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



                // empty argument test
                System.out.println("TESTMSG: empty argument test");
                state = true;
                try{
                    Logger.log(Logger.Level.ERROR,null,"{0} is {}",new String[]{"good!"});
                }catch(NumberFormatException e){
                    state = false;
                }

                // no exception thrown
                if (state){
                    errorCount++;
                    System.out.println("# TESTERROR: thrown NumberFormatException expected");
                    break;
                }

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: text print expected");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[ERROR] log() invalid argument index format ()")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[ERROR] log() invalid argument index format ()\", got \""+utoPS.text+"\"");
                    break;
                }

                // empty argument test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



            error = false;}catch(Exception e){
                errorCount++;
                System.out.println("# TESTERROR: no exception expected, caught: "
                    +e.toString()+" "+e.getMessage());
            }}while(false);
            utoPS.text = null;





            // error function
            Logger.setMinimumLogLevel(Logger.Level.TRACE);
            do{try{error = true;
                System.out.println();
                System.out.println("TESTMSG: Test error() function");



                // correct arguments with class test
                System.out.println("TESTMSG: correct arguments with class test");
                Logger.error(Logger.class,"{1} is {0}","good!","error()");

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[ERROR] Logger.error() is good!\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[ERROR] Logger.error() is good!")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[ERROR] Logger.error() is good!\", got \""+utoPS.text+"\"");
                    break;
                }

                // correct arguments with class test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



                // correct arguments with object test
                System.out.println("TESTMSG: correct arguments with object test");
                Logger.error(new Logger(),"{1} is {0}","good!","error()");

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[ERROR] Logger.error() is good!\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[ERROR] Logger.error() is good!")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[ERROR] Logger.error() is good!\", got \""+utoPS.text+"\"");
                    break;
                }

                // correct arguments with object test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



            error = false;}catch(Exception e){
                errorCount++;
                System.out.println("# TESTERROR: no exception expected, caught: "
                    +e.toString()+" "+e.getMessage());
            }}while(false);
            utoPS.text = null;




            // warnAttention function
            Logger.setMinimumLogLevel(Logger.Level.TRACE);
            do{try{error = true;
                System.out.println();
                System.out.println("TESTMSG: Test warnAttention() function");



                // correct arguments with class test
                System.out.println("TESTMSG: correct arguments with class test");
                Logger.warnAttention(Logger.class,"{1} is {0}","good!","warnAttention()");

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[WARN_ATTENTION] Logger.warnAttention() is good!\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[WARN_ATTENTION] Logger.warnAttention() is good!")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[WARN_ATTENTION] Logger.warnAttention() is good!\", got \""+utoPS.text+"\"");
                    break;
                }

                // correct arguments with class test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



                // correct arguments with object test
                System.out.println("TESTMSG: correct arguments with object test");
                Logger.warnAttention(new Logger(),"{1} is {0}","good!","warnAttention()");

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[WARN_ATTENTION] Logger.warnAttention() is good!\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[WARN_ATTENTION] Logger.warnAttention() is good!")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[WARN_ATTENTION] Logger.warnAttention() is good!\", got \""+utoPS.text+"\"");
                    break;
                }

                // correct arguments with object test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



            error = false;}catch(Exception e){
                errorCount++;
                System.out.println("# TESTERROR: no exception expected, caught: "
                    +e.toString()+" "+e.getMessage());
            }}while(false);
            utoPS.text = null;





            // warnObvious function
            Logger.setMinimumLogLevel(Logger.Level.TRACE);
            do{try{error = true;
                System.out.println();
                System.out.println("TESTMSG: Test warnObvious() function");



                // correct arguments with class test
                System.out.println("TESTMSG: correct arguments with class test");
                Logger.warnObvious(Logger.class,"{1} is {0}","good!","warnObvious()");

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[WARN_OBVIOUS] Logger.warnObvious() is good!\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[WARN_OBVIOUS] Logger.warnObvious() is good!")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[WARN_OBVIOUS] Logger.warnObvious() is good!\", got \""+utoPS.text+"\"");
                    break;
                }

                // correct arguments with class test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



                // correct arguments with object test
                System.out.println("TESTMSG: correct arguments with object test");
                Logger.warnObvious(new Logger(),"{1} is {0}","good!","warnObvious()");

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[WARN_OBVIOUS] Logger.warnObvious() is good!\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[WARN_OBVIOUS] Logger.warnObvious() is good!")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[WARN_OBVIOUS] Logger.warnObvious() is good!\", got \""+utoPS.text+"\"");
                    break;
                }

                // correct arguments with object test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



            error = false;}catch(Exception e){
                errorCount++;
                System.out.println("# TESTERROR: no exception expected, caught: "
                    +e.toString()+" "+e.getMessage());
            }}while(false);
            utoPS.text = null;






            // info function
            Logger.setMinimumLogLevel(Logger.Level.TRACE);
            do{try{error = true;
                System.out.println();
                System.out.println("TESTMSG: Test info() function");



                // correct arguments with class test
                System.out.println("TESTMSG: correct arguments with class test");
                Logger.info(Logger.class,"{1} is {0}","good!","info()");

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[INFO] Logger.info() is good!\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[INFO] Logger.info() is good!")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[INFO] Logger.info() is good!\", got \""+utoPS.text+"\"");
                    break;
                }

                // correct arguments with class test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



                // correct arguments with object test
                System.out.println("TESTMSG: correct arguments with object test");
                Logger.info(new Logger(),"{1} is {0}","good!","info()");

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[INFO] Logger.info() is good!\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[INFO] Logger.info() is good!")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[INFO] Logger.info() is good!\", got \""+utoPS.text+"\"");
                    break;
                }

                // correct arguments with object test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



            error = false;}catch(Exception e){
                errorCount++;
                System.out.println("# TESTERROR: no exception expected, caught: "
                    +e.toString()+" "+e.getMessage());
            }}while(false);
            utoPS.text = null;





            // debug function
            Logger.setMinimumLogLevel(Logger.Level.TRACE);
            do{try{error = true;
                System.out.println();
                System.out.println("TESTMSG: Test debug() function");



                // correct arguments with class test
                System.out.println("TESTMSG: correct arguments with class test");
                Logger.debug(Logger.class,"{1} is {0}","good!","debug()");

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[DEBUG] Logger.debug() is good!\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[DEBUG] Logger.debug() is good!")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[DEBUG] Logger.debug() is good!\", got \""+utoPS.text+"\"");
                    break;
                }

                // correct arguments with class test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



                // correct arguments with object test
                System.out.println("TESTMSG: correct arguments with object test");
                Logger.debug(new Logger(),"{1} is {0}","good!","debug()");

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[DEBUG] Logger.debug() is good!\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[DEBUG] Logger.debug() is good!")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[DEBUG] Logger.debug() is good!\", got \""+utoPS.text+"\"");
                    break;
                }

                // correct arguments with object test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



            error = false;}catch(Exception e){
                errorCount++;
                System.out.println("# TESTERROR: no exception expected, caught: "
                    +e.toString()+" "+e.getMessage());
            }}while(false);
            utoPS.text = null;




            // trace function
            Logger.setMinimumLogLevel(Logger.Level.TRACE);
            do{try{error = true;
                System.out.println();
                System.out.println("TESTMSG: Test trace() function");



                // correct arguments with class test
                System.out.println("TESTMSG: correct arguments with class test");
                Logger.trace(Logger.class,"{1} is {0}","good!","trace()");

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[TRACE] Logger.trace() is good!\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[TRACE] Logger.trace() is good!")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[TRACE] Logger.trace() is good!\", got \""+utoPS.text+"\"");
                    break;
                }

                // correct arguments with class test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



                // correct arguments with object test
                System.out.println("TESTMSG: correct arguments with object test");
                Logger.trace(new Logger(),"{1} is {0}","good!","trace()");

                // no text printed on channel
                if (utoPS.text == null){
                    errorCount++;
                    System.out.println("# TESTERROR: no text printed, expected \"[TRACE] Logger.trace() is good!\".");
                    break;
                }

                // wrong text printed on channel
                if (!utoPS.text.equals("[TRACE] Logger.trace() is good!")){
                    errorCount++;
                    System.out.println("# TESTERROR: wrong text printed, expected \"[TRACE] Logger.trace() is good!\", got \""+utoPS.text+"\"");
                    break;
                }

                // correct arguments with object test done
                utoPS.text = null;
                System.out.println("TESTMSG: OK");



            error = false;}catch(Exception e){
                errorCount++;
                System.out.println("# TESTERROR: no exception expected, caught: "
                    +e.toString()+" "+e.getMessage());
            }}while(false);
            utoPS.text = null;






            Logger.setLogChannel(backupPS);
            Logger.setMinimumLogLevel(backupMLL);
        }while(false);

        System.out.println();
        System.out.println("TESTMSG: Error count = "+errorCount);
        if (errorCount == 0){
            System.out.println("TESTMSG: All test cases passed!");
        }
    }
}

/**
 * Unit test object to inject dependency.
 */
class UTOPrintStream extends PrintStream{
    public String text = null;
    public UTOPrintStream(OutputStream o){super(o);}
    public void println(String s){
        text = s;
    }
}

/**
 * Unit test object to inject dependency.
 */
class UTOOutputStream extends OutputStream{
    public void write(int s){
        //sink
    }
}
