package org.uszeged.inf.optimization.algorithm.clustering;

import java.util.List;

import org.uszeged.inf.optimization.data.Cluster;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Interface for clusterizer classes.
 *
 * @author Balázs L. Lévai
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            type of objects the clusterizer cluster
 */
public interface Clusterizer<T extends Vector> {

	/* Keys for common configuration parameters of clusterizers. */

	public static final String PARAM_MAX_SAMPLE_SIZE = "PARAM_MAX_SAMPLE_SIZE";
	public static final String PARAM_NEW_SAMPLE_SIZE = "PARAM_NEW_SAMPLING_SIZE";

	/**
	 * Tries to cluster every unclustered sample into existing clusters.
	 */
	public void clusterize();

	/**
	 * Tries to cluster a sample.
	 *
	 * @param sample
	 *            a sample to cluster
	 * @return the cluster of the sample or null if it cannot be clustered
	 */
	public Cluster<T> clusterize(T sample);

	/**
	 * Tries to cluster every unclustered sample into a single cluster.
	 *
	 * @param cluster
	 *            the cluster to use for clustering
	 */
	public void clusterize(Cluster<T> cluster);

	public void add(T sample);

	public void add(List<T> samples);

	public void add(Cluster<T> cluster);

	public boolean remove(T sample);

	public boolean remove(List<T> sample);

	public boolean remove(Cluster<T> cluster);

	public List<Cluster<T>> getClusters();

	public List<T> getUnclustered();

	/**
	 * Removes all clusters and unclustered samples from the clusterizer.
	 */
	public void restart();

}
