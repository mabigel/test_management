package org.uszeged.inf.optimization.algorithm.clustering;

import java.util.List;
import java.util.Map;

import org.uszeged.inf.optimization.util.ErrorMessages;
import org.uszeged.inf.optimization.util.Logger;
import org.uszeged.inf.optimization.data.Cluster;
import org.uszeged.inf.optimization.data.Point;

/**
 * The class implements a variant of single linkage clustering fitting the needs
 * of the global optimizer algorithm Global
 * (http://www.inf.u-szeged.hu/actacybernetica/edb/vol08n4/pdf/
 * Csendes_1988_ActaCybernetica.pdf).
 *
 * @author Balázs L. Lévai
 * @version 1.0
 * @since 1.0
 */
public class GlobalSingleLinkageClusterizer extends AbstractClusterizer<Point> {

	private long numberOfSamples;

	/**
	 * An unclustered sample will be inserted into a cluster if it is closer to
	 * any sample of the cluster than the critical distance.
	 */
	private double criticalDistance;

	/**
	 * Parameter of the critical distance.
	 */
	private double alpha;



	public GlobalSingleLinkageClusterizer(double alpha) {

		super();
		this.alpha = alpha;
	}

	public void clusterize() {
		updateCriticalDistance();
		super.clusterize();

	}

	public Cluster<Point> clusterize(Point pointUnclustered) {

		updateCriticalDistance();
		for (Cluster<Point> cluster : getClusters()) {
			if (distance(cluster.getCenter(), pointUnclustered) <= criticalDistance / 10.0d) {
				cluster.getElements().add(pointUnclustered);
				if (pointUnclustered.getFunctionValue() < cluster.getCenter()
						.getFunctionValue()) {
					cluster.setCenter(pointUnclustered);
				}
				return cluster;
			}
		}
		return null;

	}

	protected Cluster<Point> clusterize(Point pointUnclustered,
			List<Cluster<Point>> clusters) {

		for (Cluster<Point> cluster : clusters) {
			for (Point pointClustered : cluster.getElements()) {
				if (distance(pointUnclustered, pointClustered) <= criticalDistance
						&& pointClustered.getFunctionValue() < pointUnclustered
								.getFunctionValue()) {
					cluster.getElements().add(pointUnclustered);
					if (pointUnclustered.getFunctionValue() < cluster
							.getCenter().getFunctionValue()) {
						cluster.setCenter(pointUnclustered);
					}
					return cluster;
				}
			}
		}
		return null;

	}

	protected void clusterize(List<Point> unClustered,
			Map<Point, Cluster<Point>> clustered,
			Map<Point, Cluster<Point>> clusteredBuffer) {

		for (Point pointUnclustered : unClustered) {
			for (Point pointClustered : clustered.keySet()) {
				if (distance(pointClustered, pointUnclustered) <= criticalDistance
						&& pointClustered.getFunctionValue() < pointUnclustered
								.getFunctionValue()) {
					Cluster<Point> cluster = clustered.get(pointClustered);
					cluster.getElements().add(pointUnclustered);
					if (pointUnclustered.getFunctionValue() < cluster
							.getCenter().getFunctionValue()) {
						cluster.setCenter(pointUnclustered);
					}
					clusteredBuffer.put(pointUnclustered, cluster);
					break;
				}
			}
		}

	}

	/**
	 * Updates the critical distance as it depends on the number of samples.
	 */
	private void updateCriticalDistance() {

		numberOfSamples = 0L;
		for (Cluster<Point> c : getClusters()) {
			numberOfSamples += c.getElements().size();
		}
		numberOfSamples += getUnclustered().size();

		int dimension = getUnclustered().get(0).getDimension();
		criticalDistance = Math.pow(
				1.0d - Math.pow(alpha, 1.0d / (numberOfSamples - 1)),
				1.0d / dimension);

	}

	/**
	 * Calculates distance based on infinity norm.
	 *
	 * @param a
	 *            an object to measure distance from
	 * @param b
	 *            an object to measure distance to
	 * @return the distance of a and b
	 */
	private double distance(Point a, Point b) {
		int dimension = a.getDimension();
		double max = Double.MIN_VALUE;
		double abs = 0.0d;
		for (int i = 1; i <= dimension; i++) {
			abs = Math.abs(a.getCoordinate(i) - b.getCoordinate(i));
			if (max < abs) {
				max = abs;
			}
		}
		return max;

	}

	static public class Builder {

		private double alpha = Double.NaN;

		public void setAlpha(double alpha) {
			if(Double.isNaN(alpha)) {
				Logger.error(null,"GlobalSingleLinkageClusterizer.Builder.setAlpha(double) alpha must be a number!");
				throw new IllegalArgumentException(
						ErrorMessages.ALPHA_MUST_BE_NUMBER);
			}
			if (alpha < 0.0d || 1.0d < alpha) {
				Logger.error(GlobalSingleLinkageClusterizer.class,"Builder.setAlpha(double) alpha ({0}) must respect bounds 0 <= alpha <= 1",String.valueOf(alpha));
				throw new IllegalArgumentException(
						ErrorMessages.ARGUMENT_OUT_OF_BOUNDS(
								"Alpha", 0, 1));
			}

			this.alpha = alpha;
		}



		public GlobalSingleLinkageClusterizer build() {

			if(Double.isNaN(alpha)) {
				Logger.error(null,"GlobalSingleLinkageClusterizer.Builder.build() alpha must be set!");
				throw new IllegalArgumentException(
						ErrorMessages.ALPHA_MUST_BE_NUMBER);
			}

			Logger.info(GlobalSingleLinkageClusterizer.class,"Builder.build() GlobalSingleLinkageClusterizer created");
			Logger.trace(GlobalSingleLinkageClusterizer.class,"Builder.build() {0} := {1}",
				"PARAM_ALPHA",
				String.valueOf(alpha)
				);

			GlobalSingleLinkageClusterizer clusterizer = new GlobalSingleLinkageClusterizer(alpha);
			return clusterizer;
		}

	}



}
