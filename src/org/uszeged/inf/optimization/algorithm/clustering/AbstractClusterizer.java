package org.uszeged.inf.optimization.algorithm.clustering;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.uszeged.inf.optimization.data.Cluster;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.util.ErrorMessages;
import org.uszeged.inf.optimization.util.Logger;

/**
 * Base class for clusterizer classes providing basic implementation of expected
 * functionality.
 *
 * @author Balázs L. Lévai
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            type of objects the clusterizer cluster
 */
abstract class AbstractClusterizer<T extends Vector> implements Clusterizer<T> {

	/**
	 * Auxiliary variable to hold recently clustered samples.
	 */
	private Map<T, Cluster<T>> clustered;

	/**
	 * Auxiliary variable to hold recently clustered samples.
	 */
	private Map<T, Cluster<T>> clusteredBuffer;
	private List<Cluster<T>> clusters;
	private List<T> unClustered;

	protected AbstractClusterizer() {

		this.clusters = new ArrayList<Cluster<T>>();
		this.unClustered = new ArrayList<T>();
		this.clustered = new HashMap<T, Cluster<T>>();
		this.clusteredBuffer = new HashMap<T, Cluster<T>>();

	}

	/**
	 * Follows the template method design pattern using #Cluster<T> clusterize(T
	 * sample, List<Cluster<T>> clusters) and #void clusterize(List<T>
	 * unClustered, Map<T, Cluster<T>> clustered, Map<T, Cluster<T>>
	 * clusteredBuffer).
	 *
	 * @see org.uszeged.inf.optimization.algorithm.clustering.Clusterizer#clusterize()
	 */
	public void clusterize() {

		// Trying to cluster unclustered samples
		for (T sample : unClustered) {
			Cluster<T> cluster = clusterize(sample, clusters);

			// Noting the clusters of clustered samples
			if (cluster != null) {
				clustered.put(sample, cluster);
			}
		}
		unClustered.removeAll(clustered.keySet());

		// Iteratively trying to cluster unclustered samples only to the newly
		// clustered ones to improve performance
		while (clustered.size() > 0) {
			clusterize(unClustered, clustered, clusteredBuffer);
			unClustered.removeAll(clusteredBuffer.keySet());
			clustered.clear();
			clustered.putAll(clusteredBuffer);
			clusteredBuffer.clear();
		}

		Logger.trace(this,"clusterize() number of unclustered samples = {0}",
			String.valueOf(unClustered.size())
			);
	}

	/**
	 * Follows the template method design pattern using #Cluster<T> clusterize(T
	 * sample, List<Cluster<T>> clusters).
	 *
	 * @see org.uszeged.inf.optimization.algorithm.clustering.Clusterizer#clusterize(org.uszeged.inf.optimization.data.Vector)
	 */
	public Cluster<T> clusterize(T sample) {

		Cluster<T> cluster = clusterize(sample, clusters);
		if (cluster != null) {
			clustered.put(sample, cluster);
			return cluster;
		} else {
			return null;
		}

	}

	/**
	 * Follows the template method design pattern using #void clusterize(List<T>
	 * unClustered, Map<T, Cluster<T>> clustered, Map<T, Cluster<T>>
	 * clusteredBuffer).
	 *
	 * @see org.uszeged.inf.optimization.algorithm.clustering.Clusterizer#clusterize(org.uszeged.inf.optimization.data.Cluster)
	 */
	public void clusterize(Cluster<T> cluster) {

		if (!clusters.contains(cluster)) {
			add(cluster);
		}
		for (T element : cluster.getElements()) {
			clustered.put(element, cluster);
		}
		while (clustered.size() > 0) {
			clusterize(unClustered, clustered, clusteredBuffer);
			unClustered.removeAll(clusteredBuffer.keySet());
			clustered.clear();
			clustered.putAll(clusteredBuffer);
			clusteredBuffer.clear();
		}

	}

	public void add(T sample) {
		unClustered.add(sample);
	}

	public void add(List<T> sample) {
		unClustered.addAll(sample);
	}

	public void add(Cluster<T> cluster) {

		if (cluster == null) {
			throw new NullPointerException(ErrorMessages.NULL_CLUSTER);
		} else if (cluster.getElements().size() == 0) {
			throw new NullPointerException(ErrorMessages.EMPTY_CLUSTER);
		} else if (cluster.getCenter() == null) {
			cluster.setCenter(cluster.getElements().get(0));
		}
		clusters.add(cluster);

	}

	public boolean remove(Vector sample) {
		return unClustered.remove(sample);
	}

	public boolean remove(List<T> sample) {
		return unClustered.removeAll(sample);
	}

	public boolean remove(Cluster<T> cluster) {
		return clusters.remove(cluster);
	}

	public List<Cluster<T>> getClusters() {
		return clusters;
	}

	public List<T> getUnclustered() {
		return unClustered;
	}


	public void restart() {
		clusters.clear();
		unClustered.clear();
	}

	/**
	 * Operation in template method design pattern for other methods in the
	 * class. It should try to cluster a single sample into the existing
	 * clusters.
	 *
	 * @param sample
	 *            a sample to cluster
	 * @param clusters
	 *            the existing clusters
	 * @return the cluster which the sample was inserted into or null if the
	 *         sample remain unclustered
	 */
	protected abstract Cluster<T> clusterize(T sample, List<Cluster<T>> clusters);

	/**
	 * Operation in template method design pattern for other methods in the
	 * class. It should try to cluster a set of samples comparing to a given set
	 * of already clustered samples.
	 *
	 * @param unClustered
	 *            the samples to cluster
	 * @param clustered
	 *            the already clustered samples
	 * @param clusteredBuffer
	 *            auxiliary variable
	 */
	protected abstract void clusterize(List<T> unClustered,
			Map<T, Cluster<T>> clustered, Map<T, Cluster<T>> clusteredBuffer);
}
