package org.uszeged.inf.optimization.algorithm.gradient;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.util.ErrorMessages;
import org.uszeged.inf.optimization.util.Logger;


/**
 * Base class for gradient computing classes providing basic implementation of expected
 * functionality.
 *
 * @author Dániel Zombori
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            type of objects the optimizer optimizes over
 */
public abstract class AbstractGradient<T extends Vector> implements Gradient<T> {

	protected Function objectiveFunction;
	protected long numberOfFunctionEvaluations;
	protected GradientConfiguration<T> configuration;
	protected boolean isGradientComputed;
	protected boolean isRunnable;
	protected T gradient;
	protected T startingPoint;
	protected double startingValue;
	protected int dimensionality;

	//Implementation of actual gradient computing algorithm
	public abstract void run();

	public AbstractGradient() {

		this.objectiveFunction = null;
		this.dimensionality = 0;
		this.numberOfFunctionEvaluations = 0;
		this.configuration = new GradientConfiguration<T>();

	}

	public void reset() {
		isRunnable = false;
		this.dimensionality = 0;
		this.numberOfFunctionEvaluations = 0;
        isGradientComputed = false;
		gradient = null;
		startingPoint = null;
	}

	public void restart() {
		isRunnable = true;
        isGradientComputed = false;
		gradient = null;
		this.numberOfFunctionEvaluations = 0;
		if (objectiveFunction == null) {
			Logger.error(this,"restart() objective function is null");
			throw new IllegalArgumentException(
					ErrorMessages.NULL_OBJECTIVE_FUNCTION);
		}
		if (startingPoint == null) {
			Logger.error(this,"restart() starting point is null");
			throw new IllegalArgumentException(
					ErrorMessages.NULL_STARTING_POINT);
		}

		this.dimensionality = startingPoint.getDimension();
		objectiveFunction.isDimensionAcceptable(this.dimensionality);
	}

	public void setObjectiveFunction(Function objectiveFunction) {
		isRunnable = false;
		if (objectiveFunction == null) {
			Logger.error(this,"restart() objective function is null");
			throw new IllegalArgumentException(
					ErrorMessages.NULL_OBJECTIVE_FUNCTION);
		}
		this.objectiveFunction = objectiveFunction;
	}

	public void setStartingPoint(T x, double v) {
		isRunnable=false;
		if (x == null) {
			Logger.error(this,"restart() starting point is null");
			throw new IllegalArgumentException(
						ErrorMessages.NULL_STARTING_POINT);
		}

		startingPoint = x;
		startingValue = v;
	}

	public Function getObjectiveFunction() {
		return this.objectiveFunction;
	}

	public long getNumberOfFunctionEvaluations() {
		return numberOfFunctionEvaluations;
	}

	public boolean isGradientComputed(){
		return isGradientComputed;
	}

    public T getGradient(){
        if (!isGradientComputed){
			Logger.error(this,"getGradient() gradient is not computed");
		 	throw new NullPointerException(ErrorMessages.GRADIENT_NOT_COMPUTED);
		}
        return this.gradient;
    }

	public AbstractGradient<T> getSerializableInstance(){
		try{
			AbstractGradient<T> obj = (AbstractGradient<T>) this.clone();
			obj.configuration.addAll(this.configuration);
			obj.isRunnable = false;
			obj.gradient = null;
			return obj;
		}catch(CloneNotSupportedException e){
			Logger.error(this,"getSerializableInstance() CloneNotSuppertedException: {0}",
				e.toString()
				);
			e.printStackTrace();
		}
		return null;
	}
}
