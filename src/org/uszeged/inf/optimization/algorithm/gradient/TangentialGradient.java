package org.uszeged.inf.optimization.algorithm.gradient;

import org.uszeged.inf.optimization.algorithm.gradient.GradientConfiguration;
import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.util.ErrorMessages;
import org.uszeged.inf.optimization.util.Logger;

/**
 * Tangential gradient approximation
 *
 * @author Dániel Zombori
 * @version 1.0
 * @since 1.0
 */

public class TangentialGradient extends AbstractGradient<Vector> {

	/* Default values of algorithm parameters */

    // Point offset scale for derivate approximation
	private static final double DEFAULT_DELTA_H_SCALE = 1.0e-15d;
    // Self documented
	private static final double DEFAULT_MIN_DELTA_H = 1.0e-15d;

	/* Algorithm parameters */
	public static final String PARAM_DELTA_H_SCALE = "DELTA_H_SCALE";
	public static final String PARAM_MIN_DELTA_H = "MIN_DELTA_H";

	private Vector x;
	private double fx;

	private double minDeltaH;

	private double deltaHScale;

	// Prevent direct instantiation of TangentialGradient objects
	private TangentialGradient() {
		super();
	}

	public void reset() {
		super.reset();
	}

	public void restart() {
		super.restart();
		x = new Vector(super.startingPoint);
		gradient = new Vector(dimensionality);
		fx = super.startingValue;
	}

	public void run() {
		if (!isRunnable) {
			Logger.error(this,"run() gradient is not parameterized correctly");
			throw new IllegalArgumentException(
						ErrorMessages.GRADIENT_NOT_PARAMETERIZED_YET);
		}
        double[] xv = x.getCoordinates();
        double[] dxv = gradient.getCoordinates();
        for (int d = 0; d < dimensionality; d++) {
            final double old_x = xv[d];

            double h = deltaHScale*old_x;

            // set step h using x[d] as f scale factor along d-th dimension:
            h = Math.max(Math.abs(h),minDeltaH);

            // convert h to a representable number:
            //h = Double.longBitsToDouble (Double.doubleToLongBits (h));
            xv[d] = old_x + h;
            final double fxh = objectiveFunction.evaluate(x);
            numberOfFunctionEvaluations++;
            xv[d] = old_x;

            dxv[d] = (fxh - fx) / h;
        }

		super.gradient = new Vector(dxv);
		this.isGradientComputed = true;

		Logger.trace(this,"run() computed gradient = {0}",
			String.valueOf(super.gradient)
			);
	}

	public TangentialGradient getSerializableInstance(){
		TangentialGradient obj =
		 		(TangentialGradient) super.getSerializableInstance();
		return obj;
	}

	/**
	 * Helper class to instantiate and configure TangentialGradient objects
	 * properly following the Builder design pattern.
	 *
	 * @author Dániel Zombori
	 * @version 1.0
	 * @since 1.0
	 */
	public static class Builder implements Gradient.Builder<Vector> {

		private TangentialGradient obj;
		private GradientConfiguration<Vector> configuration;

		public Builder() {
			this.configuration = new GradientConfiguration<Vector>();
		}

		public void setGradientConfiguration(
				GradientConfiguration<Vector> configuration) {
			this.configuration = configuration;
		}

		public void setMinDeltaH(double deltaH) {
			if (deltaH == Double.NaN)
					throw new IllegalArgumentException(
					ErrorMessages.MIN_DELTA_H_NAN);
			if (deltaH < 0.0)
			 		throw new IllegalArgumentException(
					ErrorMessages.MIN_DELTA_H_NEGATIVE);
			this.configuration.addDouble(PARAM_MIN_DELTA_H, deltaH);
		}

		public void setDeltaHScale(double scale) {
			if (scale == Double.NaN)
			 		throw new IllegalArgumentException(
					ErrorMessages.DELTA_H_SCALE_NAN);
			if (scale <= 0.0)
					throw new IllegalArgumentException(
					ErrorMessages.DELTA_H_SCALE_NOT_POSITIVE);
			this.configuration.addDouble(PARAM_DELTA_H_SCALE, scale);
		}

		/**
		 * Creates and configures a TangentialGradient object based on
		 * parameters given earlier to the builder.
		 *
		 * @return an TangentialGradient local optimizer
		 */
		public TangentialGradient build() {

			this.obj = new TangentialGradient();
			this.obj.configuration.addAll(this.configuration);

			if (!this.obj.configuration
					.containsKey(PARAM_MIN_DELTA_H)) {
				this.obj.configuration.addDouble(
						PARAM_MIN_DELTA_H,
						DEFAULT_MIN_DELTA_H);
			}

			if (!this.obj.configuration
					.containsKey(PARAM_DELTA_H_SCALE)) {
				this.obj.configuration.addDouble(
						PARAM_DELTA_H_SCALE,
						DEFAULT_DELTA_H_SCALE);
			}

			/* Load variables */

			this.obj.minDeltaH = this.obj.configuration
					.getDouble(PARAM_MIN_DELTA_H);
			this.obj.deltaHScale = this.obj.configuration
					.getDouble(PARAM_DELTA_H_SCALE);

			return this.obj;
		}

	}

}
