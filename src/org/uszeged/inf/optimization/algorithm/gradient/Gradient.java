package org.uszeged.inf.optimization.algorithm.gradient;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Interface for gradient computing classes.
 *
 * @author Dániel Zombori
 * @since 1.0
 * @version 1.0
 */
public interface Gradient<P extends Vector> extends Cloneable {

	/* Keys for common configuration parameters of gradient computing. */

	public static final String PARAM_STARTING_POINT = "PARAM_STARTING_POINT";
	public static final String PARAM_STARTING_VALUE = "PARAM_STARTING_VALUE";

	/**
	 * Implementation of an actual gradient computing algorithm.
	 */
	public void run();

	/**
	 * Resets the algorithm to its default state based on its configuration.
	 */
	public void reset();
	public void restart();
	public void setStartingPoint(P x, double fx);
	public void setObjectiveFunction(Function f);

	public Function getObjectiveFunction();

	public long getNumberOfFunctionEvaluations();

	public boolean isGradientComputed();
	public P getGradient();

	public Gradient<P> getSerializableInstance();

	public interface Builder<Q extends Vector>{
		public <R extends Gradient<Q>> R build();
	}
}
