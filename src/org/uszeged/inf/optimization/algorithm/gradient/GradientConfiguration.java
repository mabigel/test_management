package org.uszeged.inf.optimization.algorithm.gradient;

import org.uszeged.inf.optimization.util.DataSet;

/**
 * Class to manage gradient computing configuration.
 *
 * @author Dániel Zombori
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            additional object type you also wish use as configuration beside
 *            primitive types
 */
public class GradientConfiguration<T> extends DataSet<T> {

	public GradientConfiguration() {
		super();
	}

	public GradientConfiguration(GradientConfiguration<T> oc) {
		super(oc);
	}

	public void addAll(GradientConfiguration<T> oc) {
		super.addAll(oc);
	}
}
