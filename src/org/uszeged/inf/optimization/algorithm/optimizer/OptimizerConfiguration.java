package org.uszeged.inf.optimization.algorithm.optimizer;

import org.uszeged.inf.optimization.util.DataSet;

/**
 * Class to manage optimizer configuration.
 * 
 * @author Balázs L. Lévai
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            additional object type you also wish use as configuration beside
 *            primitive types
 */
public class OptimizerConfiguration<T> extends DataSet<T> {

	public OptimizerConfiguration() {
		super();
	}

	public OptimizerConfiguration(OptimizerConfiguration<T> oc) {
		super(oc);
	}

	public void addAll(OptimizerConfiguration<T> oc) {
		super.addAll(oc);
	}

}
