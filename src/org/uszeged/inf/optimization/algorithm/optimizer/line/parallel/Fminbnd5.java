package org.uszeged.inf.optimization.algorithm.optimizer.line.parallel;

import org.uszeged.inf.optimization.algorithm.optimizer.OptimizerConfiguration;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.util.ErrorMessages;
import org.uszeged.inf.optimization.util.Fminbnd5n;
import org.uszeged.inf.optimization.util.Logger;

import java.util.Map;


/**
 * Fminbnd5 is a linesearch optimizer, that fits a curve on
 * five points, and evaluates that curve's minimum point
 *
 * @author Földi Dominik
 * @version 1.0
 */
public class Fminbnd5 extends AbstractParallelLineSearch<Vector>{

	public static final String PARAM_MULTISTEP = "MULTISTEP";

	/**
	 * Starting and history of possible local optimum vector.
	 */
	private Vector[] x = new Vector[5];

	private double[] fx = new double[5];

	private double stepLength;

	/**
	 * If this value is false, the run method will only step
	 * two times before the curve fit
	 */
	private boolean multiStep;

	// Prevent direct instantiation of LineSearchImpl objects
	private Fminbnd5(){
		super();
	}

	public void reset(){
		super.reset();
	}

	public void restart(){
		super.restart();

		for(int i = 0; i < 5; i++) {
			this.x[i] = new Vector(super.startingPoint.getDimension());
			this.x[i].setCoordinates(super.startingPoint.getCoordinates());
			this.fx[i] = super.startingValue;
		}

		super.optimum = new Vector(this.x[1]);
		super.optimumValue = this.fx[1];

		stepLength = super.initStepLength;

	}

	/**
	 * Steps in the search direction moving step length and evaluates the
	 * function in the new point.
	 */
	private void step() {

		x[0].setCoordinates(stepDirection.getCoordinates());
		x[0].map((left, right) -> left * right, stepLength)
		.map((left, right) -> left + right, x[1])
		.map(Fminbnd5::boundEnforcer);
		fx[0] = objectiveFunction.evaluate(x[0]);
		++numberOfFunctionEvaluations;

	}

	/**
	 * Moving as far as could in the search direction until the function stops
	 * decreasing.
	 */
	public void run() {
		if (!isRunnable) {
			Logger.error(this,"run() optimizer is not parameterized correctly");
			throw new IllegalArgumentException(
					ErrorMessages.LINESEARCH_NOT_PARAMETERIZED_YET);
		}

		// The loop runs as long as it founds better
		// optimums in the given direction
		step();
		int numberOfStep=0;
		while(fx[0] < fx[1]) {
			success = true;
			// Set the optimum to the latest found optimum
			super.optimum.setCoordinates(x[0].getCoordinates());
			super.optimumValue = fx[0];

			for(int i=4; i>0 ;i--) {
				x[i].setCoordinates(x[i-1].getCoordinates());
				fx[i]=fx[i-1];
			}

			stepLength = 2 * stepLength;
			// First the method steps one step forward
			step();

			numberOfStep++;
		// Check if the fitted curve's minimum value is better than
		// the value that we get after the last successful step
		}

		maxSuccessfulStepLength = stepLength / 2;

		super.optimum.setCoordinates(x[1].getCoordinates());
		super.optimumValue = fx[1];

		// Finally we fit a curve on the last five vector,
		// and evaluate the curve's minimum
		boolean fitIsOk = true;
		if (numberOfStep>=5) {
			Vector xk = new Vector(x[0].getCoordinates());
			double fxk;

			for(int i=0; i < x[0].getDimension(); ++i){

				double[] temp = new double[5];
				for (int j=0; j < 5; j++) {
					temp[j] = x[j].getCoordinates()[i];

					//System.out.println(temp[j]+" "+fx[j]);
				}

				double f = Fminbnd5n.fit(temp,fx);
				if (f == Double.NaN) {
					fitIsOk=false;
					break;
				}
				xk.setCoordinate(i+1, f);
			}

			if (fitIsOk) {
				xk.map(Fminbnd5::boundEnforcer);
				fxk = objectiveFunction.evaluate(xk);
				++numberOfFunctionEvaluations;

				if (fxk < super.optimumValue) {
				//System.out.println("itt");
					super.optimum.setCoordinates(xk.getCoordinates());
					super.optimumValue = fxk;
				}
			}
		}

		Logger.trace(this,"run() optimum: {0} : {1}",
			String.valueOf(super.optimumValue),
			super.optimum.toString()
			);
	}

	/**
	 * Auxiliary mapper function to bound double values to the range of [-1,1].
	 *
	 * @param value
	 *            an argument value
	 * @return the bounded value
	 */
	private static double boundEnforcer(double value) {

		if (value > 1.0d) {
			return 1.0d;
		} else if (value < -1.0d) {
			return -1.0d;
		} else {
			return value;
		}

	}

	public Fminbnd5 getSerializableInstance(){
		Fminbnd5 obj = (Fminbnd5) super.getSerializableInstance();
		if (x != null)obj.x = new Vector[x.length];
		if (fx != null)obj.fx = new double[fx.length];
		if(stepDirection != null) obj.stepDirection = new Vector(stepDirection);
		return obj;
	}

	public static class Builder {

		private Fminbnd5 lineSearch;
		private OptimizerConfiguration<Vector> configuration;

		public Builder(){
			this.configuration = new OptimizerConfiguration<Vector>();
		}

		public void setMultiStep(boolean multiStep){
			this.configuration.addString(PARAM_MULTISTEP,
				multiStep ? "true" : "false");
		}

		public Fminbnd5 build(){

			this.lineSearch = new Fminbnd5();
			this.lineSearch.configuration.addAll(this.configuration);

			// MULTISTEP
			if (!this.configuration.containsKey(PARAM_MULTISTEP)){
				this.configuration.addString(PARAM_MULTISTEP, "false");
			}
			this.lineSearch.multiStep =
				this.configuration.getString(PARAM_MULTISTEP).equals("true");
			Logger.info(this,"build() MULTISTEP = {0}",
				String.valueOf(this.lineSearch.multiStep));

			// Build finished
			Logger.trace(this,"build() Fminbnd5 created");

			return this.lineSearch;
		}
	}

}
