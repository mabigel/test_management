package org.uszeged.inf.optimization.algorithm.optimizer.line.parallel;

import org.uszeged.inf.optimization.algorithm.optimizer.line.AbstractLineSearch;
import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.util.ErrorMessages;

/**
 * Base class for linesearch classes providing basic implementation of
 * expected functionality.
 *
 * @author Dominik Földi
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            type of objects the linesearch search over
 */
public abstract class AbstractParallelLineSearch<T extends Vector> extends
		AbstractLineSearch<T> implements ParallelLineSearch<T>{

	public AbstractParallelLineSearch<T> getSerializableInstance(){
		AbstractParallelLineSearch<T> obj =
				(AbstractParallelLineSearch<T>)super.getSerializableInstance();
		return obj;
	}
}
