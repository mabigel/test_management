package org.uszeged.inf.optimization.algorithm.optimizer.line;

import org.uszeged.inf.optimization.algorithm.optimizer.OptimizerConfiguration;
import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.util.ErrorMessages;
import org.uszeged.inf.optimization.util.Logger;

import java.util.Map;

/**
 * UnirandiLineSearch implements the probabilistic local optimizer algorithm UNIRANDI's
 * linesearch method.
 *
 * @author Dominik Földi
 * @version 1.0
 * @since 1.0
 */
public class LineSearchImpl extends AbstractLineSearch<Vector>{

	/**
	 * Starting vector.
	 */
	private Vector x;

	/**
	 * Possible local minimum place of the kth iteration.
	 */
	private Vector xk;

	/**
	 * Starting local minimum value.
	 */
	private double fx;

	/**
	 * Possible local minimum value of the kth iteration.
	 */
	private double fxk;

	private double stepLength;

	// Prevent direct instantiation of LineSearchImpl objects
	private LineSearchImpl(){
		super();
	}

	public void reset(){
		super.reset();
		xk = null;
	}

	public void restart(){
		super.restart();

		this.x = new Vector(super.startingPoint);
		super.optimum = new Vector(this.x);
		this.xk = new Vector(this.x.getDimension());

		fx = super.startingValue;
		super.optimumValue = this.fx;

		stepLength = super.initStepLength;
	}

	/**
	 * Steps in the search direction moving step length and evaluates the
	 * function in the new point.
	 */
	private void step() {
		xk.setCoordinates(stepDirection.getCoordinates());
		xk.map((left, right) -> left * right, stepLength)
				.map((left, right) -> left + right, x)
				.map(LineSearchImpl::boundEnforcer);
		fxk = objectiveFunction.evaluate(xk);
		++numberOfFunctionEvaluations;

	}

	/**
	 * Moving as far as could in the search direction until the function stops
	 * decreasing.
	 */
	public void run() {

		if (!isRunnable) {
			Logger.error(this,"run() optimizer is not parameterized correctly");
			throw new IllegalArgumentException(
						ErrorMessages.LINESEARCH_NOT_PARAMETERIZED_YET);
		}
		step();
		if (fxk < fx) success = true;
		while (fxk < fx) {
			x.setCoordinates(xk.getCoordinates());
			fx = fxk;
			stepLength = 2 * stepLength;
			step();
		}

		maxSuccessfulStepLength = stepLength / 2;
		super.optimum.setCoordinates(x.getCoordinates());
		super.optimumValue = fx;

		Logger.trace(this,"run() optimum: {0} : {1}",
			String.valueOf(super.optimumValue),
			super.optimum.toString()
			);
	}

	/**
	 * Auxiliary mapper function to bound double values to the range of [-1,1].
	 *
	 * @param value
	 *            an argument value
	 * @return the bounded value
	 */
	private static double boundEnforcer(double value) {

		if (value > 1.0d) {
			return 1.0d;
		} else if (value < -1.0d) {
			return -1.0d;
		} else {
			return value;
		}

	}

	public LineSearchImpl getSerializableInstance(){
		LineSearchImpl obj = (LineSearchImpl) super.getSerializableInstance();
		obj.x = null;
		obj.xk = null;
		if (stepDirection != null)obj.stepDirection = new Vector(stepDirection);
		return obj;
	}

	public static class Builder {

		private LineSearchImpl lineSearch;
		private OptimizerConfiguration<Vector> configuration;

		public Builder(){
			this.configuration = new OptimizerConfiguration<Vector>();
		}

		public LineSearchImpl build(){


			this.lineSearch = new LineSearchImpl();
			this.lineSearch.configuration.addAll(this.configuration);

			// Build finished
			Logger.trace(this,"build() LineSearchImpl created");

			return this.lineSearch;
		}
	}

}
