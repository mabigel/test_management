package org.uszeged.inf.optimization.algorithm.optimizer.line.parallel;

import org.uszeged.inf.optimization.algorithm.optimizer.OptimizerConfiguration;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.util.ErrorMessages;
import org.uszeged.inf.optimization.util.Fminbndn;
import org.uszeged.inf.optimization.util.Logger;
import java.util.Map;

/**
 * Fminbnd is a linesearch optimizer, that fits a curve on
 * three points, and evaluates that curve's minimum point
 *
 * @author Földi Dominik
 * @version 1.0
 */
public class Fminbnd extends AbstractParallelLineSearch<Vector>{

	public static final String PARAM_MULTISTEPNUMBER = "MULTISTEPNUMBER";

	public static final long DEFAULT_PARAM_MULTISTEPNUMBER = 1;

	/**
	 * Starting and history of possible local optimum vector.
	 */
	private Vector[] x = new Vector[3];

	private double[] fx = new double[3];

	private double stepLength;

	/**
	 * If this value is false, the run method will only step
	 * two times before the curve fit
	 */
	private long multiStepNumber;

	// Prevent direct instantiation of LineSearchImpl objects
	private Fminbnd(){
		super();
	}

	public void reset(){
		super.reset();
	}

	public void restart(){
		super.restart();

		this.x[1] = new Vector(super.startingPoint);
		super.optimum = new Vector(this.x[1]);

		this.x[0] = new Vector(this.x[1].getDimension());
		this.x[2] = new Vector(this.x[1].getDimension());

		this.fx[1] = super.startingValue;
		super.optimumValue = this.fx[1];

		stepLength = super.initStepLength;

		super.sumOfStepLengths = 0;
	}

	/**
	 * Steps in the search direction moving step length and evaluates the
	 * function in the new point.
	 */
	private void step() {

		x[0].setCoordinates(stepDirection.getCoordinates());
		x[0].map((left, right) -> left * right, stepLength)
		.map((left, right) -> left + right, x[1])
		.map(Fminbnd::boundEnforcer);
		fx[0] = objectiveFunction.evaluate(x[0]);
		++numberOfFunctionEvaluations;

	}

	/**
	 * Moving as far as could in the search direction until the function stops
	 * decreasing.
	 */
	public void run() {

		if (!isRunnable) {
			Logger.error(this,"run() optimizer is not parameterized correctly");
			throw new IllegalArgumentException(
					ErrorMessages.LINESEARCH_NOT_PARAMETERIZED_YET);
		}

		// The loop runs as long as it founds better
		// optimums in the given direction
		step();
		boolean twoStep=false;
		while(fx[0] < fx[1]) {
			success = true;
			super.sumOfStepLengths += stepLength;
			// Set the optimum to the latest found optimum
			super.optimum.setCoordinates(x[0].getCoordinates());
			super.optimumValue = fx[0];

			x[2].setCoordinates(x[1].getCoordinates());
			fx[2]=fx[1];
			x[1].setCoordinates(x[0].getCoordinates());
			fx[1]=fx[0];


			stepLength = 2 * stepLength;
			// First the method steps one step forward
			step();

			twoStep=true;
		// Check if the fitted curve's minimum value is better than
		// the value that we get after the last successful step
		}

		maxSuccessfulStepLength = stepLength / 2;

		//super.optimum.setCoordinates(x[1].getCoordinates());
		//super.optimumValue = fx[1];

		// Finally we fit a curve on the last three vector,
		// and evaluate the curve's minimum
		if (twoStep) {
			Vector xk = new Vector(x[0].getCoordinates());
			double fxk = Double.POSITIVE_INFINITY;
			int iteration =0;
			if (iteration < multiStepNumber) {
		
				for(int i=1; i <= x[0].getDimension(); ++i){

					double[] xVectorCoordinates = new double[3];
					for(int j = 0; j < 3; j++) {
						xVectorCoordinates[j] = x[j].getCoordinate(i);
					}

					double f = Fminbndn.fit(xVectorCoordinates, fx);
					xk.setCoordinate(i, f);

				}
				fxk = objectiveFunction.evaluate(xk);
				++numberOfFunctionEvaluations;
		
				int shift=(xk.getCoordinate(1) < x[1].getCoordinate(1))?1:0;
				if (fxk < fx[1]) {
					x[0]=new Vector(x[0+shift].getCoordinates());
					x[2]=new Vector(x[1+shift].getCoordinates());
					x[1]=new Vector(xk.getCoordinates());
				} else {
					x[2*shift]=new Vector(xk.getCoordinates());
				}
				
				++iteration;
			}
			if (fxk < super.optimumValue) {
				super.optimum.setCoordinates(xk.getCoordinates());
				super.optimumValue = fxk;
			}
		}
		
		
		Logger.trace(this,"run() optimum: {0} : {1}",
			String.valueOf(super.optimumValue),
			super.optimum.toString()
			);
	}

	/**
	 * Auxiliary mapper function to bound double values to the range of [-1,1].
	 *
	 * @param value
	 *            an argument value
	 * @return the bounded value
	 */
	private static double boundEnforcer(double value) {

		if (value > 1.0d) {
			return 1.0d;
		} else if (value < -1.0d) {
			return -1.0d;
		} else {
			return value;
		}

	}

	public Fminbnd getSerializableInstance(){
		Fminbnd obj = (Fminbnd) super.getSerializableInstance();
		if (x != null)obj.x = new Vector[x.length];
		if (fx != null)obj.fx = new double[fx.length];
		if(stepDirection != null) obj.stepDirection = new Vector(stepDirection);
		return obj;
	}

	public static class Builder {

		private Fminbnd lineSearch;
		private OptimizerConfiguration<Vector> configuration;

		public Builder(){
			this.configuration = new OptimizerConfiguration<Vector>();
		}

		public void setMultiStepNumber(long multiStepNumber){
			this.configuration.addLong(PARAM_MULTISTEPNUMBER,
				multiStepNumber);
		}

		public Fminbnd build(){

			this.lineSearch = new Fminbnd();
			this.lineSearch.configuration.addAll(this.configuration);

			// MULTISTEP
			if (!this.configuration.containsKey(PARAM_MULTISTEPNUMBER)){
				this.configuration.addLong(PARAM_MULTISTEPNUMBER, DEFAULT_PARAM_MULTISTEPNUMBER);
			}
			this.lineSearch.multiStepNumber =
				this.configuration.getLong(PARAM_MULTISTEPNUMBER);
			Logger.info(this,"build() MULTISTEPNUMBER = {0}",
				String.valueOf(this.lineSearch.multiStepNumber));

			// Build finished
			Logger.trace(this,"build() Fminbnd created");

			return this.lineSearch;
		}
	}

}
