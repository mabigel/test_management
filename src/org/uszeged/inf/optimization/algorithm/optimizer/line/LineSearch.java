package org.uszeged.inf.optimization.algorithm.optimizer.line;

import org.uszeged.inf.optimization.algorithm.optimizer.Optimizer;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Interface for LineSearch classes.
 *
 * @author Dominik Földi
 * @since 1.0
 * @version 1.0
 * @param <T> type of objects the line search search over
 */
public interface LineSearch<T extends Vector> extends Optimizer<T>{

	/* Keys for common configuration parameters of line search optimizers. */
	public static final String PARAM_STARTING_POINT = "PARAM_STARTING_POINT";
	public static final String PARAM_STARTING_VALUE = "PARAM_STARTING_VALUE";
	public static final String PARAM_STEP_DIRECTION = "PARAM_STEP_DIRECTION";

	public void reset();

	public void restart();

	public void setStartingPoint(T x,double v);

	public void setInitStepLength(double l);

	public void setStepDirection(T direction);

	public T getLineOptimum();

	public double getLineOptimumValue();

	/**
	 * Get the last successful step length
	 */
	public double getMaxSuccessfulStepLength();

	/**
	 * Check if found a better possible optimum
	 */
	public boolean isSucceed();
	
	public double getSumOfStepLengths();
}
