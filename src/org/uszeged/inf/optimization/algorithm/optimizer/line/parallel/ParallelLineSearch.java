package org.uszeged.inf.optimization.algorithm.optimizer.line.parallel;

import org.uszeged.inf.optimization.algorithm.optimizer.line.LineSearch;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Interface for serializable LineSearch classes.
 *
 * @author Dániel Zombori
 * @since 1.0
 * @version 1.0
 * @param <T> type of objects the line search search over
 */
public interface ParallelLineSearch<T extends Vector> extends LineSearch<T>,Cloneable{

	public ParallelLineSearch<T> getSerializableInstance();
}
