package org.uszeged.inf.optimization.algorithm.optimizer.local.parallel;

import org.uszeged.inf.optimization.algorithm.optimizer.OptimizerConfiguration;
import org.uszeged.inf.optimization.algorithm.optimizer.line.parallel
		.ParallelLineSearch;
import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.util.ErrorMessages;
import org.uszeged.inf.optimization.util.Logger;

import java.util.Map;

/**
 * UnirandiCLS (Unirandi with Custom Line Search) implements the probabilistic
 * local optimizer algorithm UNIRANDI
 * (http://users.abo.fi/atorn/ProbAlg/Page52.html). with custom Line Search type.
 * The implementation follows the previous MATLAB and C versions of Tibor Csendes
 * and his colleagues as part of their global optimizer GLOBAL
 * (http://www.inf.u-szeged.hu/~csendes/Reg/regform.php).
 *
 * The actual number of function evaluations made during optimization can be
 * larger than the prescribed maximum.
 *
 * @author Balázs L. Lévai, Dominik Földi
 * @version 2.0
 * @since 1.0
 */

public class UnirandiCLS extends AbstractParallelLocalOptimizer<Vector> {

	/* Algorithm parameters and parameter values */

	public static final String PARAM_DIRECTION_RANDOMIZATION = "DIRECTION_RANDOMIZATION";
	public static final String PARAM_DIRECTION_RANDOMIZATION_UNIT_CUBE = "UNIT_CUBE";
	public static final String PARAM_DIRECTION_RANDOMIZATION_NORMAL_DISTRIBUTION = "NORMAL_DISTRIBUTION";

	/* Default values of algorithm parameters */

	private static final long DEFAULT_MAX_FUNCTION_EVALUATIONS = 1000L;
	private static final long DEFAULT_MIN_FUNCTION_EVALUATIONS = 100L;
	private static final double DEFAULT_RELATIVE_CONVERGENCE = 1E-12d;
	private static final double DEFAULT_MIN_INIT_STEP_LENGTH = 0.001d;
	private static final double DEFAULT_MAX_INIT_STEP_LENGTH = 1.0d;
	private static final double DEFAULT_DELTA_F = 1.0d;
	private static final long DEFAULT_ITERATION_CONDITION = 0L;
	private static final String DEFAULT_DIRECTION_RANDOMIZATION = PARAM_DIRECTION_RANDOMIZATION_NORMAL_DISTRIBUTION;

	/**
	 * Starting vector.
	 */
	private Vector x;

	/**
	 * Starting local minimum value.
	 */
	private double fx;

	/**
	 * Possible local minimum place and value of the kth iteration
	 */
	private ParallelLineSearch<Vector> lineSearch;

	/**
	 * Search direction generated randomly.
	 */
	private Vector stepDirection;

	private double stepLength;

	/**
	 * Relative change in the function value comparing the last best value and
	 * the newly found one.
	 */
	private double deltaF;

	/**
	 * Still waiting for further aid from Sherlock to find out the purpose of
	 * this variable.
	 */
	private long iterationCondition;

	private long maxFunctionEvaluations;

	private double relativeConvergence;

	private String directionRandomizationValue;

	// Prevent direct instantiation of Unirandi2 objects
	private UnirandiCLS() {
		super();
	}

	public void reset() {
		super.reset();
	}

	public void restart() {
		super.restart();

		this.x = new Vector(super.startingPoint);
		this.stepDirection = new Vector(this.x.getDimension());
		this.stepLength = configuration.getDouble(PARAM_INIT_STEP_LENGTH);
		this.fx = super.startingValue;

		super.optimum = new Vector(this.x);
		super.optimumValue = this.fx;

		lineSearch.setObjectiveFunction(super.objectiveFunction);
	}

	public void run() {

		// prevent run if parameters not set properly
		if (!isRunnable) {
			Logger.error(this,"run() optimizer is not parameterized correctly");
			throw new IllegalArgumentException(
						ErrorMessages.GLOBAL_NOT_PARAMETERIZED_YET);
		}

		// maxFunctionEvaluations handled as a soft condition
		while (true) {
			if (numberOfFunctionEvaluations >= maxFunctionEvaluations){
				Logger.trace(this,"run() stopped with too much evaluations");
				break;
			}

			// generate random search direction in form of a unit length vector
			generateDirection();

			// reset linesearch parameters to new ones
			lineSearch.setStartingPoint(x, fx);
			lineSearch.setStepDirection(stepDirection);
			lineSearch.setInitStepLength(stepLength);
			lineSearch.restart();

			// run it with the new parameters
			lineSearch.run();
			numberOfFunctionEvaluations +=
			 		lineSearch.getNumberOfFunctionEvaluations();

			if (lineSearch.isSucceed()) {

				// on success save values
				stepLength = Math.abs(lineSearch.getMaxSuccessfulStepLength());
				x.setCoordinates(lineSearch.getLineOptimum().getCoordinates());
				deltaF = (fx - lineSearch.getLineOptimumValue()) /
						Math.abs(lineSearch.getLineOptimumValue());
				fx = lineSearch.getLineOptimumValue();
				continue;
			}

			// search the opposite direction if we have not found better points
			// yet in the search direction
			stepDirection.map((left)->-left);

			// reset linesearch parameters to new ones
			lineSearch.setStartingPoint(x, fx);
			lineSearch.setStepDirection(stepDirection);
			lineSearch.setInitStepLength(stepLength);
			lineSearch.restart();

			// run it with the new parameters
			lineSearch.run();
			numberOfFunctionEvaluations +=
					lineSearch.getNumberOfFunctionEvaluations();



			if (lineSearch.isSucceed()) {

				// on success save values and start new iteration
				stepLength = Math.abs(lineSearch.getMaxSuccessfulStepLength());
				x.setCoordinates(lineSearch.getLineOptimum().getCoordinates());
				deltaF = (fx - lineSearch.getLineOptimumValue()) /
						Math.abs(lineSearch.getLineOptimumValue());
				fx = lineSearch.getLineOptimumValue();
				continue;
			}

			// not have the faintest idea why this is necessary, but an order is
			// an order
			// update (Dániel Zombori): it provides that every stepLength will
			// be tried with two random directions
			// TODO: it could be a parameter
			iterationCondition = iterationCondition + 1;
			if (iterationCondition < 2) {
				continue;
			}

			// decrease step length supposing we were not successful in this
			// iteration only because we moved too much
			stepLength = stepLength / 2;
			iterationCondition = 0;

			// checking stopping criteria, too little change in function values
			// or in vectors
			if ( deltaF < relativeConvergence
					|| Math.abs(stepLength) < relativeConvergence) {

				Logger.trace(this,"run() stopped with relative convergence relative_dFx = {0}, dPos = {1}",
					String.valueOf(deltaF),String.valueOf(stepLength)
					);
				break;
			}

		}

		// save optimum point
		super.optimumValue = fx;
		super.optimum.setCoordinates(x.getCoordinates());

		Logger.trace(this,"run() optimum: {0} : {1}",
			String.valueOf(super.optimumValue),
			super.optimum.toString()
			);
	}

	/**
	 * Generates random direction in form of normalized random vector whose
	 * coordinates' expected value is 0.
	 */
	private void generateDirection() {

		switch(directionRandomizationValue){
			case PARAM_DIRECTION_RANDOMIZATION_UNIT_CUBE:
				Vector.randomize(stepDirection);
				stepDirection.map((value) -> value - 0.5d);
			break;

			case PARAM_DIRECTION_RANDOMIZATION_NORMAL_DISTRIBUTION:
				Vector.gaussianRandomize(stepDirection);
			break;
		}

		// vector's length must be 1
		Vector.normalize(stepDirection);
	}

	// Creates an exact copy of optimizer with link copy
	public UnirandiCLS getSerializableInstance(){
		Logger.trace(this,"getSerializableInstance() invoked");
		UnirandiCLS obj = (UnirandiCLS) super.getSerializableInstance();
		obj.lineSearch = this.lineSearch.getSerializableInstance();
		obj.x = new Vector(x);
		obj.stepDirection = new Vector(stepDirection);
		return obj;
	}

	/**
	 * Helper class to instantiate and configure Unirandi objects properly
	 * following the Builder design pattern.
	 *
	 * @author Balázs L. Lévai
	 * @version 1.0
	 * @since 1.0
	 */
	public static class Builder {

		private UnirandiCLS unirandi;
		private ParallelLineSearch<Vector> lineSearch;
		private OptimizerConfiguration<Vector> configuration;

		public Builder() {

			this.configuration = new OptimizerConfiguration<Vector>();
			this.lineSearch = null;

		}

		public void setLineSearchFunction(
				ParallelLineSearch<Vector> lineSearch){
			if (lineSearch == null){
				Logger.error(this,"setLineSearchFunction() LineSearch cannot be null!");
				throw new IllegalArgumentException(
						ErrorMessages.NULL_LINE_SEARCH_OPTIMIZER);
			}
			this.lineSearch = lineSearch;
		}

		public void setOptimizerConfiguration(
				OptimizerConfiguration<Vector> configuration) {
			this.configuration = configuration;
		}

		public void setInitStepLength(double stepLength) {

			if (stepLength < DEFAULT_MIN_INIT_STEP_LENGTH) {
				stepLength = DEFAULT_MIN_INIT_STEP_LENGTH;
			} else if (stepLength > DEFAULT_MAX_INIT_STEP_LENGTH) {
				stepLength = DEFAULT_MAX_INIT_STEP_LENGTH;
			}
			this.configuration.addDouble(PARAM_INIT_STEP_LENGTH, stepLength);

		}

		public void setDirectionRandomization(String type) {
			String param = null;

			if (type == null){
				Logger.error(null,"Unirandi.Builder.setDirectionRandomization(String) Direction randomization type cannot be null!");
				throw new NullPointerException("Direction randomization type cannot be null!");
			}

			type = type.toUpperCase();

			if (type.equals(PARAM_DIRECTION_RANDOMIZATION_UNIT_CUBE)){
				param = PARAM_DIRECTION_RANDOMIZATION_UNIT_CUBE;
			}

			if (type.equals(PARAM_DIRECTION_RANDOMIZATION_NORMAL_DISTRIBUTION)){
				param = PARAM_DIRECTION_RANDOMIZATION_NORMAL_DISTRIBUTION;
			}

			if (param == null){
				Logger.error(null,"Unirandi.Builder.setDirectionRandomization(String) Unknown parameter for direction randomization: {0}",type);
				throw new IllegalArgumentException("Unknown parameter for direction randomization: "+type);
			}

			this.configuration.addString(PARAM_DIRECTION_RANDOMIZATION, param);
		}

		public void setMaxFunctionEvaluations(long maxEvaluations) {

			if (maxEvaluations < DEFAULT_MIN_FUNCTION_EVALUATIONS) {
				maxEvaluations = DEFAULT_MIN_FUNCTION_EVALUATIONS;
			}
			this.configuration.addLong(PARAM_MAX_FUNCTION_EVALUATIONS,
					maxEvaluations);

		}

		public void setRelativeConvergence(double convergence) {

			if (convergence < DEFAULT_RELATIVE_CONVERGENCE) {
				convergence = DEFAULT_RELATIVE_CONVERGENCE;
			}
			this.configuration.addDouble(PARAM_RELATIVE_CONVERGENCE,
					convergence);

		}

		public void setStartingPoint(Vector startingPoint,double startingValue) {

			if (startingPoint == null) {
				Logger.error(this,"setStartingPoint(Vector,double) StartingPoint cannot be null!");
				throw new IllegalArgumentException(
						ErrorMessages.NULL_STARTING_POINT);
			}
			this.configuration.addObject(PARAM_STARTING_POINT, startingPoint);
			this.configuration.addDouble(PARAM_STARTING_VALUE, startingValue);
		}

		/**
		 * Creates and configures an Unirandi object based on parameters given
		 * earlier to the builder.
		 *
		 * @return an Unirandi local optimizer
		 */
		public UnirandiCLS build() {

			this.unirandi = new UnirandiCLS();
			this.unirandi.configuration.addAll(this.configuration);

			if (this.lineSearch == null) {
				Logger.error(this,"build() LineSearch must be set!");
				throw new IllegalArgumentException(
						ErrorMessages.NULL_LINE_SEARCH_OPTIMIZER);
			}
			this.unirandi.lineSearch = this.lineSearch;
			Logger.info(this,"build() LINE_SEARCH = {0}",
				this.unirandi.lineSearch.getClass().getCanonicalName());

			if (!this.unirandi.configuration
					.containsKey(PARAM_INIT_STEP_LENGTH)) {
				this.unirandi.configuration.addDouble(PARAM_INIT_STEP_LENGTH,
						DEFAULT_MIN_INIT_STEP_LENGTH);
			}
			this.unirandi.stepLength = this.unirandi.configuration
					.getDouble(PARAM_INIT_STEP_LENGTH);
			Logger.info(this,"build() INIT_STEP_LENGTH = {0}",
				String.valueOf(this.unirandi.stepLength));

			if (!this.unirandi.configuration
					.containsKey(PARAM_MAX_FUNCTION_EVALUATIONS)) {
				this.unirandi.configuration.addLong(
						PARAM_MAX_FUNCTION_EVALUATIONS,
						DEFAULT_MAX_FUNCTION_EVALUATIONS);
			}
			this.unirandi.maxFunctionEvaluations = this.unirandi.configuration
					.getLong(PARAM_MAX_FUNCTION_EVALUATIONS);
			Logger.info(this,"build() MAX_FUNCTION_EVALUATIONS = {0}",
				String.valueOf(this.unirandi.maxFunctionEvaluations));

			if (!this.unirandi.configuration
					.containsKey(PARAM_RELATIVE_CONVERGENCE)) {
				this.unirandi.configuration.addDouble(
						PARAM_RELATIVE_CONVERGENCE,
						DEFAULT_RELATIVE_CONVERGENCE);
			}
			this.unirandi.relativeConvergence = this.unirandi.configuration
					.getDouble(PARAM_RELATIVE_CONVERGENCE);
			Logger.info(this,"build() RELATIVE_CONVERGENCE = {0}",
				String.valueOf(this.unirandi.relativeConvergence));

			if (!this.unirandi.configuration
					.containsKey(PARAM_DIRECTION_RANDOMIZATION)) {
				this.unirandi.configuration.addString(
						PARAM_DIRECTION_RANDOMIZATION,
						DEFAULT_DIRECTION_RANDOMIZATION);
			}
			this.unirandi.directionRandomizationValue = this.unirandi.configuration
					.getString(PARAM_DIRECTION_RANDOMIZATION);
			Logger.info(this,"build() DIRECTION_RANDOMIZATION = {0}",
				String.valueOf(this.unirandi.directionRandomizationValue));

			this.unirandi.deltaF = DEFAULT_DELTA_F;
			Logger.info(this,"build() DELTA_F = {0}",
				String.valueOf(this.unirandi.deltaF));

			this.unirandi.iterationCondition = DEFAULT_ITERATION_CONDITION;
			Logger.info(this,"build() ITERATION_CONDITION = {0}",
				String.valueOf(this.unirandi.iterationCondition));

			this.unirandi.x = new Vector(0);
			this.unirandi.stepDirection = new Vector(0);

			// Build finished
			Logger.trace(this,"build() UnirandiCLS created");

			return this.unirandi;
		}

	}

}
