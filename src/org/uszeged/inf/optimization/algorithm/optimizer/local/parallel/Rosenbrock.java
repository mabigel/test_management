package org.uszeged.inf.optimization.algorithm.optimizer.local.parallel;

import java.util.Arrays;
import java.util.Map;

import org.uszeged.inf.optimization.algorithm.optimizer.OptimizerConfiguration;
import org.uszeged.inf.optimization.algorithm.optimizer.line.parallel.ParallelLineSearch;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.util.ErrorMessages;
import org.uszeged.inf.optimization.util.VectorOperations;
import org.uszeged.inf.optimization.util.Logger;

/**
 * Rosenbrock implements Rosenbrock's direct search method,
 * that he created for solving his famous Banana function.
 *
 * @author Dominik Földi
 * @version 1.0
 * @since 1.0
 */
public class Rosenbrock extends AbstractParallelLocalOptimizer<Vector>{

	/* Default values of algorithm parameters */
	private static final long DEFAULT_MAX_FUNCTION_EVALUATIONS = 1000L;
	private static final long DEFAULT_MIN_FUNCTION_EVALUATIONS = 100L;
	private static final double DEFAULT_RELATIVE_CONVERGENCE = 1E-12d;
	private static final double DEFAULT_MIN_INIT_STEP_LENGTH = 0.001d;
	private static final double DEFAULT_MAX_INIT_STEP_LENGTH = 1.0d;
	//private static final long DEFAULT_ITERATION_CONDITION = 0L;

	/**
	 * Starting vector.
	 */
	private Vector x;

	/**
	 * Starting local minimum value.
	 */
	private double fx;

	/**
	 * Dimension of the Vector field.
	 */
	private int dimension;

	/**
	 * Possible local optimums.
	 */
	private Vector xk;

	/**
	 * Possible local optimum values;
	 */
	private double fxk;

	/**
	 * Linearly independent orthogonal vectors.
	 * The method will search along these vectors.
	 */
	private Vector[] directions;

	/**
	 * Length of the last steps.
	 */
	private double stepLength;

	/**
	 * Method for searching in directions.
	 */
	private ParallelLineSearch<Vector> lineSearch;

	/**
	 *
	 */
	private long maxFunctionEvaluations;

	/**
	 *
	 */
	private double relativeConvergence;

	//Prevent direct instantiation of Rosenbrock objects
	private Rosenbrock(){
		super();
	}

	public void reset() {
		super.reset();
	}

	public void restart() {
		super.restart();

		this.x = new Vector(super.startingPoint);
		this.fx = super.startingValue;
		this.dimension = x.getDimension();

		this.xk = new Vector(x);
		this.fxk = fx;

		this.directions = generateBasis(dimension);
		this.stepLength =  this.configuration
					.getDouble(PARAM_INIT_STEP_LENGTH);

		super.optimum = new Vector(this.x);
		super.optimumValue = this.fx;

		lineSearch.setObjectiveFunction(super.objectiveFunction);
	}

	public void run() {

		// prevent run if parameters not set properly
		if (!isRunnable) {
			Logger.error(this,"run() optimizer is not parameterized correctly");
			throw new IllegalArgumentException(
						ErrorMessages.LOCAL_NOT_PARAMETERIZED_YET);
		}

		// maxFunctionEvaluations handled as a soft condition
		// rotation of coordinates will happen at the end of
		// this iteration
		while (true) {
			if (numberOfFunctionEvaluations >= maxFunctionEvaluations){
				Logger.trace(this,"run() stopped with too much evaluations");
				break;
			}

			// Iteration condition
			// If there is no success in any direction
			// Then comes the rotation of coordinates
			int noSuccess = 0;


			xk.setCoordinates(x.getCoordinates());
			fxk = fx;
			// Search through every dimensions
			for (int i = 0; i < dimension; ++i) {

				// variable that checks if the opposite direction
				// tried already
				int opposite = 0;

				// if the first direction is bad, than
				// search in the opposite
				do {
					//System.out.println(directions[i]);
					opposite++;
					// reset linesearch parameters to new ones
					lineSearch.setStartingPoint(xk, fxk);
					lineSearch.setStepDirection(directions[i]);
					lineSearch.setInitStepLength(stepLength);
					lineSearch.restart();
					// run it with the new parameters
					lineSearch.run();

					numberOfFunctionEvaluations += lineSearch.getNumberOfFunctionEvaluations();

					if (lineSearch.isSucceed()) {
						xk.setCoordinates(lineSearch.getLineOptimum().getCoordinates());
						fxk = lineSearch.getLineOptimumValue();
						//System.out.println("xk:"+xk);

					} else {
						noSuccess++;

						// search the opposite direction if we have not found better points
						// yet in the search direction
						//stepLengths[i]*=-1;
						directions[i].map((left) -> -left);
					}
				} while (!lineSearch.isSucceed() && opposite <= 1);

			}

			if (noSuccess < 2*dimension) {
				// Sum of the starting vector and the best vector,
				Vector bestVector = new Vector(x);
				bestVector.map((left, right) -> - right + left, xk);

				//Copy xk to x
				x.setCoordinates(xk.getCoordinates());
				fx = fxk;

				// Rotate the coordinates,
				// the best vector will be the first direction
				//System.out.println("bestVector"+bestVector);

				directions = gramSchmidt(bestVector);
			} else {
				stepLength=stepLength/2.0;
				if (Math.abs(stepLength) < relativeConvergence) {
					Logger.trace(this,"run() stopped with relative convergence dPos = {0}",
						String.valueOf(stepLength)
						);
					break;
				}
			}
		}

		this.optimum.setCoordinates(x.getCoordinates());
		this.optimumValue = fx;

		Logger.trace(this,"run() optimum: {0} : {1}",
			String.valueOf(super.optimumValue),
			super.optimum.toString()
			);
	}

	/**
	 * Generates n-dimensional standard basis.
	 * @param n Dimension
	 */
	private Vector[] generateBasis(int n){

		Vector[] basis = new Vector[n];

		for(int i = 0; i<n; ++i){

			double[] coordinates = new double[n];

			for(int j = 0; j<n; ++j){
				if(j != i)
					coordinates[j] = 0;
				else
					coordinates[j] = 1;
			}

			basis[i] = new Vector(coordinates);
		}

		return basis;
	}

	/**
	 * Implementation of the Gram-Schmidt orthonormalization process.
	 * @param v The first vector of the orthonormalized basis
	 * @return An orthonormalized array of Vectors
	 */
	private Vector[] gramSchmidt(Vector v){

		// Copy the matrix
				Vector u[] = new Vector[v.getDimension()];

				// The first vector of the orthonormal basis
				// is the parameter
				u[0] = new Vector(v);

				// https://en.wikipedia.org/wiki/Gram%E2%80%93Schmidt_process
				for(int i = 1; i< u.length; ++i){
					// Generating the next vector
					Vector randVector = Vector.randomVector(v.getDimension());
					u[i] = new Vector(randVector);
					for(int j =0; j < i; ++j)
						u[i].setCoordinates(VectorOperations.subtractVectors(u[i],
								VectorOperations.projection(u[j], randVector)).getCoordinates());
				}

				// Normalize all of the new directions
				for(int i=0;i<u.length;++i)
					Vector.normalize(u[i]);

				return u;
	}

	public Rosenbrock getSerializableInstance(){
		Rosenbrock obj = (Rosenbrock) super.getSerializableInstance();
		obj.lineSearch = this.lineSearch.getSerializableInstance();
		obj.x = new Vector(x);
		obj.xk = xk.clone();
		obj.directions = directions.clone();
		return obj;
	}

	/**
	 * Helper class to instantiate and configure Rosenbrock objects properly
	 * following the Builder design pattern.
	 *
	 * @author Dominik Földi
	 * @version 1.0
	 * @since 1.0
	 */
	public static class Builder {

		private Rosenbrock rosenbrock;
		private ParallelLineSearch<Vector> lineSearch;
		private OptimizerConfiguration<Vector> configuration;

		public Builder() {

			this.configuration = new OptimizerConfiguration<Vector>();
			this.lineSearch = null;

		}

		public void setLineSearchFunction(
				ParallelLineSearch<Vector> lineSearch){
			if (lineSearch == null){
				Logger.error(null,"Rosenbrock.Builder.setLineSearchFunction() LineSearch cannot be null!");
				throw new IllegalArgumentException(
						ErrorMessages.NULL_LINE_SEARCH_OPTIMIZER);
			}
			this.lineSearch = lineSearch;
		}

		public void setOptimizerConfiguration(
				OptimizerConfiguration<Vector> configuration) {
			this.configuration = configuration;
		}

		public void setInitStepLength(double stepLength) {

			if (stepLength < DEFAULT_MIN_INIT_STEP_LENGTH) {
				stepLength = DEFAULT_MIN_INIT_STEP_LENGTH;
			} else if (stepLength > DEFAULT_MAX_INIT_STEP_LENGTH) {
				stepLength = DEFAULT_MAX_INIT_STEP_LENGTH;
			}
			this.configuration.addDouble(PARAM_INIT_STEP_LENGTH, stepLength);

		}

		public void setMaxFunctionEvaluations(long maxEvaluations) {

			if (maxEvaluations < DEFAULT_MIN_FUNCTION_EVALUATIONS) {
				maxEvaluations = DEFAULT_MIN_FUNCTION_EVALUATIONS;
			}
			this.configuration.addLong(PARAM_MAX_FUNCTION_EVALUATIONS,
					maxEvaluations);

		}

		public void setRelativeConvergence(double convergence) {

			if (convergence < DEFAULT_RELATIVE_CONVERGENCE) {
				convergence = DEFAULT_RELATIVE_CONVERGENCE;
			}
			this.configuration.addDouble(PARAM_RELATIVE_CONVERGENCE,
					convergence);

		}

		public void setStartingPoint(Vector startingPoint,double startingValue) {

			if (startingPoint == null) {
				Logger.error(null,"Rosenbrock.Builder.setStartingPoint(Vector,double) StartingPoint cannot be null!");
				throw new IllegalArgumentException(
						ErrorMessages.NULL_STARTING_POINT);
			}
			this.configuration.addObject(PARAM_STARTING_POINT, startingPoint);
			this.configuration.addDouble(PARAM_STARTING_VALUE, startingValue);
		}

		/**
		 * Creates and configures a Rosenbrock object based on parameters given
		 * earlier to the builder.
		 *
		 * @return a Rosenbrock local optimizer
		 */
		public Rosenbrock build() {

			this.rosenbrock = new Rosenbrock();
			this.rosenbrock.configuration.addAll(this.configuration);

			if (this.lineSearch == null) {
				Logger.error(null,"Rosenbrock.Builder.build() LineSearch must be set!");
				throw new IllegalArgumentException(
						ErrorMessages.NULL_LINE_SEARCH_OPTIMIZER);
			}
			this.rosenbrock.lineSearch = this.lineSearch;
			Logger.info(this,"build() LINE_SEARCH = {0}",
				this.rosenbrock.lineSearch.getClass().getCanonicalName());

			if (!this.rosenbrock.configuration
					.containsKey(PARAM_INIT_STEP_LENGTH)) {
				this.rosenbrock.configuration.addDouble(PARAM_INIT_STEP_LENGTH,
						DEFAULT_MIN_INIT_STEP_LENGTH);
			}
			this.rosenbrock.stepLength = this.rosenbrock.configuration
					.getDouble(PARAM_INIT_STEP_LENGTH);
			Logger.info(this,"build() INIT_STEP_LENGTH = {0}",
				String.valueOf(this.rosenbrock.stepLength));

			if (!this.rosenbrock.configuration
					.containsKey(PARAM_MAX_FUNCTION_EVALUATIONS)) {
				this.rosenbrock.configuration.addLong(
						PARAM_MAX_FUNCTION_EVALUATIONS,
						DEFAULT_MAX_FUNCTION_EVALUATIONS);
			}
			this.rosenbrock.maxFunctionEvaluations = this.rosenbrock.configuration
					.getLong(PARAM_MAX_FUNCTION_EVALUATIONS);
			Logger.info(this,"build() MAX_FUNCTION_EVALUATIONS = {0}",
				String.valueOf(this.rosenbrock.maxFunctionEvaluations));

			if (!this.rosenbrock.configuration
					.containsKey(PARAM_RELATIVE_CONVERGENCE)) {
				this.rosenbrock.configuration.addDouble(
						PARAM_RELATIVE_CONVERGENCE,
						DEFAULT_RELATIVE_CONVERGENCE);
			}
			this.rosenbrock.relativeConvergence = this.rosenbrock.configuration
					.getDouble(PARAM_RELATIVE_CONVERGENCE);
			Logger.info(this,"build() RELATIVE_CONVERGENCE = {0}",
				String.valueOf(this.rosenbrock.relativeConvergence));


			this.rosenbrock.x = new Vector(0);
			this.rosenbrock.directions = new Vector[0];
			this.rosenbrock.xk = new Vector(0);

			// Build finished
			Logger.trace(this,"build() Rosenbrock created");

			return this.rosenbrock;
		}

	}

}
