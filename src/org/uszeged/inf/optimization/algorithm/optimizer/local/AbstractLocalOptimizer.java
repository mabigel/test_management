package org.uszeged.inf.optimization.algorithm.optimizer.local;

import org.uszeged.inf.optimization.algorithm.optimizer.AbstractOptimizer;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.util.ErrorMessages;
import org.uszeged.inf.optimization.util.Logger;

/**
 * Base class for local optimizer classes providing basic implementation of
 * expected functionality.
 *
 * @author Balázs L. Lévai
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            type of objects the local optimizer optimize over
 */
public abstract class AbstractLocalOptimizer<T extends Vector> extends
		AbstractOptimizer<T> implements LocalOptimizer<T> {

	protected T startingPoint;
	protected double startingValue;

	public AbstractLocalOptimizer() {
		super();
	}

	// Implementation of an actual local optimizer should go here
	public abstract void run();

	public void reset() {
		super.reset();
	}

	/**
	 * Set starting point of optimization in the normalized space.
	 */
	public void setStartingPoint(T x, double v) {

		// prevents run before restart, restart must check bounds and dimension
		// for starting point
		isRunnable = false;

		// x must be a valid vector
		if (x == null) {
			Logger.error(this,"setStartingPoint(T extends Vector,double) starting point can't be null");
			throw new IllegalArgumentException(
						ErrorMessages.NULL_STARTING_POINT);
		}

		Logger.trace(this,"setStartingPoint(T extends Vector,double) invoked ({0}) : {1}",
			String.valueOf(v),
			objectiveFunction.toOriginalSpace(x).toString()
			);

		// store parameters
		startingPoint = x;
		startingValue = v;
	}

	/**
	 * Set starting point of optimization in the original space.
	 */
	public void setUnNormalizedStartingPoint(T x, double v){
		//IMPROVEMENT;
		// TODO: set starting point function for original problem space
		Logger.error(this,"setUnNormalizedStartingPoint(T extends Vector,double)"
			+" not implemented yet");
		throw new RuntimeException("Function not implemented");
	}

	public void restart() {
		super.restart();
	}

	public T getLocalOptimum() {
		return super.optimum;
	}

	public double getLocalOptimumValue() {
		return super.optimumValue;
	}
}
