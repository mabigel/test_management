package org.uszeged.inf.optimization.algorithm.optimizer.local.parallel;

import org.uszeged.inf.optimization.algorithm.optimizer.local
		.AbstractLocalOptimizer;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.util.ErrorMessages;

/**
 * Base class for local optimizer classes providing basic implementation of
 * expected functionality.
 *
 * @author Dániel Zombori
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            type of objects the local optimizer optimize over
 */
public abstract class AbstractParallelLocalOptimizer<T extends Vector> extends
		AbstractLocalOptimizer<T> implements ParallelLocalOptimizer<T> {

	public AbstractParallelLocalOptimizer() {
		super();
	}

	public AbstractParallelLocalOptimizer<T> getSerializableInstance(){
		 AbstractParallelLocalOptimizer<T> obj =
		 		(AbstractParallelLocalOptimizer<T>)
				 		super.getSerializableInstance();
		return obj;
	}
}
