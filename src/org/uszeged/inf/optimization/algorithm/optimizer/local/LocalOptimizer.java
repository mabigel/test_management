package org.uszeged.inf.optimization.algorithm.optimizer.local;

import org.uszeged.inf.optimization.algorithm.optimizer.Optimizer;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Interface for local optimizer classes.
 *
 * @author Balázs L. Lévai
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            type of objects the local optimizer optimize over
 */
public interface LocalOptimizer<T extends Vector> extends Optimizer<T> {

	/* Keys for common configuration parameters of local optimizers. */

	public static final String PARAM_INIT_STEP_LENGTH = "PARAM_INIT_STEP_LENGTH";
	public static final String PARAM_STARTING_POINT = "PARAM_STARTING_POINT";
	public static final String PARAM_STARTING_VALUE = "PARAM_STARTING_VALUE";

	/**
	 * Resets the local optimizer to its default state base on its configuration
	 * changing the starting point and the starting value.
	 *
	 * @param point
	 *            the new starting point for the local optimizer
	 * @param value
	 *            the new starting function value for the local optimizer
	 */
	public void setStartingPoint(T point, double value);

	public T getLocalOptimum();

	public double getLocalOptimumValue();
}
