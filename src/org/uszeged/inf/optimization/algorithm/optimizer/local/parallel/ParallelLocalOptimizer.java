package org.uszeged.inf.optimization.algorithm.optimizer.local.parallel;

import org.uszeged.inf.optimization.algorithm.optimizer.local.LocalOptimizer;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Interface for serializable local optimizer classes.
 *
 * @author Dániel Zombori
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            type of objects the local optimizer optimize over
 */
public interface ParallelLocalOptimizer<T extends Vector>
 		extends LocalOptimizer<T>,Cloneable {

	public ParallelLocalOptimizer<T> getSerializableInstance();
}
