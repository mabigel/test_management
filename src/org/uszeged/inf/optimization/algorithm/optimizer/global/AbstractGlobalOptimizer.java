package org.uszeged.inf.optimization.algorithm.optimizer.global;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.uszeged.inf.optimization.algorithm.optimizer.AbstractOptimizer;
import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.util.Logger;


/**
 * Base class for global optimizer classes providing basic implementation of
 * expected functionality.
 *
 * @author Balázs L. Lévai
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            type of objects the global optimizer optimize over
 */
public abstract class AbstractGlobalOptimizer<T extends Vector> extends
		AbstractOptimizer<T> implements GlobalOptimizer<T> {

	protected Map<T, Double> localOptimumValues;

	public AbstractGlobalOptimizer() {
		super();
		this.localOptimumValues = new HashMap<T, Double>();
	}

	// Implementation of an actual global optimizer should go here
	public abstract void run();

	public void reset() {
		super.reset();
		this.localOptimumValues.clear();
		this.optimum = null;
		this.optimumValue = Double.MAX_VALUE;
	}

	public void restart() {
		super.restart();
		this.localOptimumValues.clear();
		this.optimum = null;
		this.optimumValue = Double.MAX_VALUE;
	}

	/**
	* Function for testing.
	*/
	public void setKnownGlobalOptimumValue(double globalOptimumValue) {
		this.configuration.addDouble(PARAM_GLOBAL_OPTIMUM_VALUE, globalOptimumValue);
		Logger.info(this,"setKnownGlobalOptimumValue() value = {0}",String.valueOf(globalOptimumValue));
	}

	public T getGlobalOptimum() {
		return super.optimum;
	}

	public double getGlobalOptimumValue() {
		return super.optimumValue;
	}

	public Set<T> getLocalOptimums() {
		return localOptimumValues.keySet();
	}

	public Map<T, Double> getLocalOptimalValues() {
		return localOptimumValues;
	}

	public AbstractGlobalOptimizer<T> getSerializableInstance(){
		Logger.trace(this,"getSerializableInstance() invoked");
		try{
			AbstractGlobalOptimizer<T> obj =
				(AbstractGlobalOptimizer<T>) this.clone();
			obj.localOptimumValues = new HashMap<T, Double>();
			obj.localOptimumValues.putAll(this.localOptimumValues);
			return obj;
		}catch(CloneNotSupportedException e){
			e.printStackTrace();
			Logger.error(this,"getSerializableInstance() CloneNotSupportedException");
		}
		return null;
	}
}
