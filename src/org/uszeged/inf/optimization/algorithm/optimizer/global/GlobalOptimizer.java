package org.uszeged.inf.optimization.algorithm.optimizer.global;

import java.util.Map;
import java.util.Set;

import org.uszeged.inf.optimization.algorithm.optimizer.Optimizer;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Interface for global optimizer classes.
 *
 * @author Balázs L. Lévai
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            type of objects the global optimizer optimize over
 */
public interface GlobalOptimizer<T extends Vector> extends Optimizer<T> {

	/* Keys for common configuration parameters of global optimizers. */

	public static final String PARAM_GLOBAL_OPTIMUM_VALUE = "PARAM_GLOBAL_OPTIMUM_VALUE";
	public static final String PARAM_MAX_LOCAL_OPTIMUMS = "PARAM_MAX_LOCAL_OPTIMUMS";
	public static final String PARAM_MAX_LOCAL_SEARCHES = "PARAM_MAX_LOCAL_SEARCHES";

	public T getGlobalOptimum();

	public double getGlobalOptimumValue();

	public long getNumberOfLocalSearches();

	public Set<T> getLocalOptimums();

	public Map<T, Double> getLocalOptimalValues();

	public long getRunTime();

	public void setKnownGlobalOptimumValue(double v);

	public default String getRunStatistics(){
		return "";
	}

}
