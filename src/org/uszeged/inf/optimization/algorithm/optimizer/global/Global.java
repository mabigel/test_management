package org.uszeged.inf.optimization.algorithm.optimizer.global;

import static org.uszeged.inf.optimization.algorithm.clustering.Clusterizer.PARAM_MAX_SAMPLE_SIZE;
import static org.uszeged.inf.optimization.algorithm.clustering.Clusterizer.PARAM_NEW_SAMPLE_SIZE;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.uszeged.inf.optimization.algorithm.clustering.Clusterizer;
import org.uszeged.inf.optimization.algorithm.optimizer.OptimizerConfiguration;
import org.uszeged.inf.optimization.algorithm.optimizer.local.LocalOptimizer;
import org.uszeged.inf.optimization.data.Cluster;
import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Point;
import org.uszeged.inf.optimization.data.Point.DescendingFunctionValueComparator;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.data.ConstrainedFunction;
import org.uszeged.inf.optimization.util.ErrorMessages;
import org.uszeged.inf.optimization.util.Logger;

/**
 * Global implements a variant of the probabilistic global optimizer algorithm
 * Global (http://www.inf.u-szeged.hu/actacybernetica/edb/vol08n4/pdf/
 * Csendes_1988_ActaCybernetica.pdf). The implementation follows the previous
 * MATLAB and C versions of Tibor Csendes and his colleagues.
 * (http://www.inf.u-szeged.hu/~csendes/Reg/regform.php).
 *
 * The actual number of function evaluations made during optimization can be
 * larger than the prescribed maximum.
 *
 * @author Balázs L. Lévai
 * @since 1.0
 * @version 1.0
 */
public class Global extends AbstractGlobalOptimizer<Point> implements
		GlobalOptimizer<Point> {

	/* Algorithm specific keys and default values for parameters. */


	private static final long DEFAULT_MAX_RUNTIME = 10 * 60 * 1000000000L;
	private static final long DEFAULT_MAX_ITERATIONS = 10L;
	private static final long DEFAULT_MAX_LOCAL_SEARCHES = 100L;
	private static final long DEFAULT_MAX_LOCAL_OPTIMUMS = 20L;
	private static final long DEFAULT_NEW_SAMPLE_SIZE = 100L;
	private static final long DEFAULT_MAX_SAMPLE_SIZE = 1000L;
	private static final long DEFAULT_MAX_FUNCTION_EVALUATIONS = 5000L;

	private LocalOptimizer<Vector> localOptimizer;
	private Clusterizer<Point> clusterizer;
	private List<Point> sample;
	private Comparator<Point> descendingFunctionValueOrder;

	private long newSampleSize;
	private int reducedSampleSize;
	private long numberOfSamples;
	private long numberOfIterations;
	private long startTime;
	private long runTime;
	private long numberOfLocalSearches;

	private long maxNumberOfFunctionEvaluations;
	private boolean noNewLocalOptimumPlace;
	private long maxNumberOfLocalSearches;
	private long maxNumberOfSamples;
	private long maxNumberOfLocalOptimums;
	private long maxNumberOfIterations;
	private long maxRuntime;
	private boolean hasKnownGlobalOptimumValue;
	private double globalOptimumValue;

	// Prevent direct instantiation of Global
	private Global() {
		super();
	}

	public void reset() {
		super.reset();
		this.objectiveFunction = null;
	}

	public void restart() {
		super.restart();

		super.optimum = new Point(this.dimension);

		localOptimizer.setObjectiveFunction(this.objectiveFunction);
		clusterizer.restart();
	}

	public String getRunStatistics(){
		StringBuilder sb = new StringBuilder();
		sb.append(numberOfLocalSearches);
		//sb.setLength(sb.length() - 1);
		return sb.toString();
	}

	public void run() {

		if (!isRunnable) {

			Logger.error(this,"run() optimizer is not parameterized correctly");
			throw new IllegalArgumentException(
						ErrorMessages.GLOBAL_NOT_PARAMETERIZED_YET);
		}

		hasKnownGlobalOptimumValue = false;
		if (configuration.containsKey(PARAM_GLOBAL_OPTIMUM_VALUE)) {
			hasKnownGlobalOptimumValue = true;
			globalOptimumValue = configuration.getDouble(PARAM_GLOBAL_OPTIMUM_VALUE);
			Logger.info(this,"run() KNOWN_GLOBAL_OPTIMUM_VALUE = {0}",String.valueOf(
				globalOptimumValue
			));
		}

		startTime = System.nanoTime();
		while (true) {
			++numberOfIterations;
			noNewLocalOptimumPlace = true;
			long time;

			// Generating samples
			sampling();

			// Selecting a reduced sample set and clusterizes them
			clusterize();

			// Starts local searches from unclustered samples
			processUnclustered();

			// Checking stopping criteria
			if (hasKnownGlobalOptimumValue) {
				if (isKnownOptimumReached()) {
					Logger.info(this,"run() exit condition: known optimum value reached");
					break;
				}
				if(isKnownOptimumEvaluationLimitReached()) {
					Logger.info(this, "run() exit condition: too many function evaluations");
					break;
				}
			} else {
				if (maxNumberOfSamples <= numberOfSamples){
					Logger.info(this,"run() exit condition: maximum number of samples reached");
					break;
				}
				if (numberOfFunctionEvaluations > maxNumberOfFunctionEvaluations){
					Logger.info(this,"run() exit condition: maximum number of function evaluations reached");
					break;
				}
				if (maxNumberOfLocalOptimums <= clusterizer.getClusters().size()){
					Logger.info(this,"run() exit condition: maximum number of local optimums reached");
					break;
				}
				if (noNewLocalOptimumPlace){
					Logger.info(this,"run() exit condition: no new local optimum found");
					break;
				}
				if (maxNumberOfLocalSearches <= numberOfLocalSearches){
					Logger.info(this,"run() exit condition: maximum number of local searches reached");
					break;
				}
				if (maxNumberOfIterations <= numberOfIterations){
					Logger.info(this,"run() exit condition: maximum number of iterations reached");
					break;
				}
				if (maxRuntime <= System.nanoTime() - startTime){
					Logger.info(this,"run() exit condition: maximum runtime reached");
					break;
				}
			}
		}

		// Storing result and transforming it back to the original problem space
		for (Cluster<Point> c : clusterizer.getClusters()) {
			Point localOptimum = c.getCenter();
			localOptimum.map((left, right) -> left * right,
					this.searchSpaceRadius).map((left, right) -> left + right,
					this.searchSpaceCenter);
			this.localOptimumValues.put(c.getCenter(),
					Double.valueOf(c.getCenter().getFunctionValue()));
		}
		runTime = System.nanoTime() - startTime;

		Logger.info(this,"run() Runtime {0}s",
			String.valueOf((runTime/1000000)/1000d));
	}

	public long getRunTime(){
		return this.runTime;
	}

	public long getNumberOfLocalSearches(){
		return this.numberOfLocalSearches;
	}


	/**
	 * Selects a reduced sample set and tries to clusterize them.
	 */
	private void clusterize() {

		Collections.sort(sample, descendingFunctionValueOrder);
		int end = sample.size() - 1;
		for (int i = 0; i < reducedSampleSize; i++) {
			clusterizer.add(sample.remove(end - i));
		}
		clusterizer.clusterize();
	}

	/**
	 * Starts local searches from unclustered samples. It creates new clusters
	 * around new local optimums.
	 */
	private void processUnclustered() {
		while (clusterizer.getUnclustered().size() > 0) {

			// Getting an unclustered sample and starting a search from it
			int end = clusterizer.getUnclustered().size() - 1;
			Point x = clusterizer.getUnclustered().get(end);
			localOptimizer.setStartingPoint(x, x.getFunctionValue());
			localOptimizer.restart();
			localOptimizer.run();
			++numberOfLocalSearches;
			this.numberOfFunctionEvaluations += localOptimizer
					.getNumberOfFunctionEvaluations();
			Point localOptimum = new Point(localOptimizer.getLocalOptimum());
			localOptimum
					.setFunctionValue(localOptimizer.getLocalOptimumValue());

			// Checking for new global optimum
			checkForGlobalOptimum(localOptimum);

			// Trying to clusterize the local optimum to an existing cluster
			Cluster<Point> c = clusterizer.clusterize(localOptimum);

			// Creating new cluster if the local optimum is unclustered
			if (c == null) {
				noNewLocalOptimumPlace = false;
				c = new Cluster<Point>();
				c.getElements().add(localOptimum);
				c.setCenter(localOptimum);
				clusterizer.add(c);
			}
			c.getElements().add(x);

			if (hasKnownGlobalOptimumValue){
				if (isKnownOptimumReached() || isKnownOptimumEvaluationLimitReached()) {
					break;
				}
			}

			// Trying to clusterize the remaining unclustered samples to newly
			// clustered sample
			clusterizer.getUnclustered().remove(end);
			clusterizer.clusterize(c);
			if (maxNumberOfLocalOptimums <= clusterizer.getClusters().size()) {
				break;
			}
		}

	}

	/**
	 * Uniformly generates random samples in the 0 centered cube with 2 edge
	 * length.
	 */
	private void sampling() {

		for (int i = 0; i < newSampleSize; i++) {
			Point x = new Point(dimension);
			Vector.randomize(x);
			x.map((value) -> 2.0d * value - 1.0d);
			x.setFunctionValue(objectiveFunction);
			sample.add(x);
			checkForGlobalOptimum(x);
		}
		this.numberOfFunctionEvaluations += newSampleSize;
		this.numberOfSamples += newSampleSize;

	}

	private void checkForGlobalOptimum(Point x){
		if (x.getFunctionValue() < super.optimumValue) {
			super.optimumValue = x.getFunctionValue();
			super.optimum = x;
			Logger.info(this,"checkForGlobalOptimum() new optimum found ({1}) : {0}",
				objectiveFunction.toOriginalSpace(x).toString(),
				String.valueOf(x.getFunctionValue())
				);
		}
	}

	private boolean isKnownOptimumReached(){
		if (Math.abs(globalOptimumValue - super.optimumValue)
				<= Math.pow(10,-8) + Math.abs(globalOptimumValue) * Math.pow(10,-6)) {
			return true;
		}
		return false;
	}

	private boolean isKnownOptimumEvaluationLimitReached(){
		return numberOfFunctionEvaluations > (Math.pow(10, 5) + 1);
	}

	private void reset(boolean distinctiveDummy) {
		this.numberOfSamples = 0;
		this.numberOfIterations = 0;
		this.numberOfLocalSearches = 0;
		this.clusterizer.getUnclustered().clear();
		this.clusterizer.getClusters().clear();
		this.localOptimizer.reset();
		this.runTime = 0;
	}

	public long runTime(){
		return this.runTime;
	}

	/**
	 * Helper class to instantiate and configure Global objects properly
	 * following the Builder design pattern.
	 *
	 * @author Balázs L. Lévai
	 * @version 1.0
	 * @since 1.0
	 */
	public static class Builder {

		private Global global;
		private Clusterizer<Point> clusterizer;
		private LocalOptimizer<Vector> localOptimizer;
		private OptimizerConfiguration<Point> configuration;

		private double sampleReducingFactor;

		public Builder() {

			this.configuration = new OptimizerConfiguration<Point>();
			this.clusterizer = null;
			this.localOptimizer = null;
			this.sampleReducingFactor = 0.0;
		}

		public void setSampleReducingFactor(double sampleReducingFactor) {

			if (sampleReducingFactor <= 0.0d || 1.0d < sampleReducingFactor) {
				Logger.error(null,"Global.Builder.setSampleReducingFactor(double) value ({0}) must be in interval (0;1]",
					String.valueOf(sampleReducingFactor)
					);
				throw new IllegalArgumentException(
						ErrorMessages.ARGUMENT_OUT_OF_BOUNDS(
								"Sample reducing factor", 0, 1));
			}
			this.sampleReducingFactor = sampleReducingFactor;

		}

		public void setClusterizer(Clusterizer<Point> clusterizer) {

			if (clusterizer == null) {
				Logger.error(null,"Global.Builder.setClusterizer() Clusterizer cannot be null!");
				throw new IllegalArgumentException(
						ErrorMessages.NULL_CLUSTERIZER);
			}
			this.clusterizer = clusterizer;

		}

		public void setLocalOptimizer(LocalOptimizer<Vector> localOptimizer) {

			if (localOptimizer == null) {
				Logger.error(null,"Global.Builder.setLocalOptimizer() LocalOptimizer cannot be null!");
				throw new IllegalArgumentException(
						ErrorMessages.NULL_LOCAL_OPTIMIZER);
			}
			this.localOptimizer = localOptimizer;

		}

		public void setMaxNumberOfIterations(long value) {

			if (value < 1) {
				Logger.error(null,"Global.Builder.setMaxNumberOfIterations(long) value ({0}) must be positive",
					String.valueOf(value));
				throw new IllegalArgumentException(
						ErrorMessages.ILLEGAL_ARGUMENT(
								"Maximum number of iterations", "positive"));
			}
			this.configuration.addLong(PARAM_MAX_ITERATIONS, value);

		}

		public void setMaxNumberOfSamples(long value) {

			if (value < 1) {
				Logger.error(null,"Global.Builder.setMaxNumberOfSamples(long) value ({0}) must be positive",
					String.valueOf(value));
				throw new IllegalArgumentException(
						ErrorMessages.ILLEGAL_ARGUMENT(
								"Maximum number of samples", "positive"));
			}
			this.configuration
					.addLong(Clusterizer.PARAM_MAX_SAMPLE_SIZE, value);

		}

		public void setNewSampleSize(long value) {

			if (value < 1) {
				Logger.error(null,"Global.Builder.setNewSampleSize(long) value ({0}) must be positive",
					String.valueOf(value));
				throw new IllegalArgumentException(
						ErrorMessages.ILLEGAL_ARGUMENT("New sample size",
								"positive"));
			}
			this.configuration
					.addLong(Clusterizer.PARAM_NEW_SAMPLE_SIZE, value);

		}

		public void setMaxNumberOfFunctionEvaluations(long value) {

			if (value < 1) {
				Logger.error(null,"Global.Builder.setMaxNumberOfFunctionEvaluations(long) value ({0}) must be positive",
					String.valueOf(value));
				throw new IllegalArgumentException(
						ErrorMessages.ILLEGAL_ARGUMENT(
								"Maximum number of function evaluations",
								"positive"));
			}
			this.configuration.addLong(PARAM_MAX_FUNCTION_EVALUATIONS, value);

		}

		public void setMaxNumberOfLocalSearches(long value) {

			if (value < 1) {
				Logger.error(null,"Global.Builder.setMaxNumberOfLocalSearches(long) value ({0}) must be positive",
					String.valueOf(value));
				throw new IllegalArgumentException(
						ErrorMessages.ILLEGAL_ARGUMENT(
								"Maximum number of local searches", "positive"));
			}
			this.configuration.addLong(PARAM_MAX_LOCAL_SEARCHES, value);

		}

		public void setMaxNumberOfLocalOptimums(long value) {

			if (value < 1) {
				Logger.error(null,"Global.Builder.setMaxNumberOfLocalOptimums(long) value ({0}) must be positive",
					String.valueOf(value));
				throw new IllegalArgumentException(
						ErrorMessages.ILLEGAL_ARGUMENT(
								"Maximum number of local optimums", "positive"));
			}
			this.configuration.addLong(PARAM_MAX_LOCAL_OPTIMUMS, value);

		}

		public void setMaxRuntimeInSeconds(long value) {

			if (value < 1) {
				Logger.error(null,"Global.Builder.setMaxRuntimeInSeconds(long) value ({0}) must be positive",
					String.valueOf(value));
				throw new IllegalArgumentException(
						ErrorMessages.ILLEGAL_ARGUMENT("Runtime", "positive"));
			}
			this.configuration.addLong(PARAM_MAX_RUNTIME, value * 1000000000);

		}

		/**
		 * Creates and configures a Global object based on parameters given
		 * earlier to the builder.
		 *
		 * @return a Global global optimizer
		 */
		public Global build() {

			this.global = new Global();

			this.global.configuration.addAll(this.configuration);
			this.global.sample = new ArrayList<Point>();
			this.global.descendingFunctionValueOrder = new DescendingFunctionValueComparator();

			if (this.clusterizer == null) {
				Logger.error(null,"Global.Builder.build() Clusterizer must be set!");
				throw new IllegalArgumentException(
						ErrorMessages.NULL_CLUSTERIZER);
			}
			this.global.clusterizer = this.clusterizer;
			Logger.info(this,"build() CLUSTERIZER = {0}",
				this.global.clusterizer.getClass().getCanonicalName());

			if (this.localOptimizer == null) {
				Logger.error(null,"Global.Builder.build() LocalOptimizer must be set!");
				throw new IllegalArgumentException(
						ErrorMessages.NULL_LOCAL_OPTIMIZER);
			}
			this.global.localOptimizer = this.localOptimizer;
			Logger.info(this,"build() LOCAL_OPTIMIZER = {0}",
				this.global.localOptimizer.getClass().getCanonicalName());

			if (!this.global.configuration
					.containsKey(PARAM_MAX_FUNCTION_EVALUATIONS)) {
				this.global.configuration.addLong(
						PARAM_MAX_FUNCTION_EVALUATIONS,
						DEFAULT_MAX_FUNCTION_EVALUATIONS);
			}
			this.global.maxNumberOfFunctionEvaluations = this.global.configuration
					.getLong(PARAM_MAX_FUNCTION_EVALUATIONS);
			Logger.info(this,"build() MAX_FUNCTION_EVALUATIONS = {0}",String.valueOf(this.global.maxNumberOfFunctionEvaluations));

			if (!this.global.configuration.containsKey(PARAM_MAX_ITERATIONS)) {
				this.global.configuration.addLong(PARAM_MAX_ITERATIONS,
						DEFAULT_MAX_ITERATIONS);
			}
			this.global.maxNumberOfIterations = this.global.configuration
					.getLong(PARAM_MAX_ITERATIONS);
			Logger.info(this,"build() MAX_ITERATIONS = {0}",String.valueOf(this.global.maxNumberOfIterations));

			if (!this.global.configuration
					.containsKey(PARAM_MAX_LOCAL_OPTIMUMS)) {
				this.global.configuration.addLong(PARAM_MAX_LOCAL_OPTIMUMS,
						DEFAULT_MAX_LOCAL_OPTIMUMS);
			}
			this.global.maxNumberOfLocalOptimums = this.global.configuration
					.getLong(PARAM_MAX_LOCAL_OPTIMUMS);
			Logger.info(this,"build() MAX_LOCAL_OPTIMUMS = {0}",String.valueOf(this.global.maxNumberOfLocalOptimums));

			if (!this.global.configuration
					.containsKey(PARAM_MAX_LOCAL_SEARCHES)) {
				this.global.configuration.addLong(PARAM_MAX_LOCAL_SEARCHES,
						DEFAULT_MAX_LOCAL_SEARCHES);
			}
			this.global.maxNumberOfLocalSearches = this.global.configuration
					.getLong(PARAM_MAX_LOCAL_SEARCHES);
			Logger.info(this,"build() MAX_LOCAL_SEARCHES = {0}",String.valueOf(this.global.maxNumberOfLocalSearches));

			if (!this.global.configuration.containsKey(PARAM_MAX_RUNTIME)) {
				this.global.configuration.addLong(PARAM_MAX_RUNTIME,
						DEFAULT_MAX_RUNTIME);
			}
			this.global.maxRuntime = this.global.configuration
					.getLong(PARAM_MAX_RUNTIME);
			Logger.info(this,"build() MAX_RUNTIME = {0}",String.valueOf(this.global.maxRuntime));

			if (!this.global.configuration.containsKey(PARAM_MAX_SAMPLE_SIZE)) {
				this.global.configuration.addLong(PARAM_MAX_SAMPLE_SIZE,
						DEFAULT_MAX_SAMPLE_SIZE);
			}
			this.global.maxNumberOfSamples = this.global.configuration
					.getLong(PARAM_MAX_SAMPLE_SIZE);
			Logger.info(this,"build() MAX_SAMPLE_SIZE = {0}",String.valueOf(this.global.maxNumberOfSamples));

			if (!this.global.configuration.containsKey(PARAM_NEW_SAMPLE_SIZE)) {
				this.global.configuration.addLong(PARAM_NEW_SAMPLE_SIZE,
						DEFAULT_NEW_SAMPLE_SIZE);
			}
			this.global.newSampleSize = this.global.configuration
					.getLong(PARAM_NEW_SAMPLE_SIZE);
			Logger.info(this,"build() NEW_SAMPLE_SIZE = {0}",String.valueOf(this.global.newSampleSize));

			this.global.reducedSampleSize = Math.max(
					((int) Math.floor(this.global.newSampleSize
							* this.sampleReducingFactor)), 1);
			Logger.info(this,"build() REDUCED_SAMPLE_SIZE = {0}",String.valueOf(this.global.reducedSampleSize));

			// Build finished
			Logger.trace(this,"build() Global created");

			return this.global;
		}
	}
}
