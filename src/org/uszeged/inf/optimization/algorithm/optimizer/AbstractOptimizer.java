package org.uszeged.inf.optimization.algorithm.optimizer;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.ConstrainedFunction;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.util.ErrorMessages;
import org.uszeged.inf.optimization.util.Logger;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;

import java.io.File;
import java.lang.reflect.*;

/**
 * Base class for optimizer classes providing basic implementation of expected
 * functionality.
 *
 * @author Balázs L. Lévai
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            type of objects the optimizer optimizes over
 */
public abstract class AbstractOptimizer<T extends Vector> implements Optimizer<T> {

	protected ConstrainedFunction objectiveFunction;
	protected long numberOfFunctionEvaluations;
	protected OptimizerConfiguration<T> configuration;
	protected T optimum;
	protected double optimumValue;

	protected Vector searchSpaceCenter;
	protected Vector searchSpaceRadius;
	private Vector upperBound;
	private Vector lowerBound;

	protected int dimension;
	private Function origObjectiveFunction;

	/**
	 * Indicates that the optimizer is correctly parameterized. The optimizer
	 * implementation should check it before start. Should throw error if false.
	 */
	protected boolean isRunnable;

	/**
	 * Validates the optimizer parameters and enables run if everything is ok.
	 */
	public void restart() {
		Logger.trace(this,"restart() invoked");

		// assume that every parameter is correct
		isRunnable = true;

		if (objectiveFunction == null){
			// if no objective function is given throw exception
			Logger.error(this,"restart() objective function is null");
			throw new IllegalArgumentException(
				ErrorMessages.NULL_OBJECTIVE_FUNCTION);
		}

		// set variables to init value
		this.numberOfFunctionEvaluations = 0;
		this.optimum = null;
		this.optimumValue = Double.POSITIVE_INFINITY;
	}

	public void setObjectiveFunction(Function objectiveFunction) {

		isRunnable = false;
		if (objectiveFunction == null) {
			Logger.error(this,"setObjectiveFunction(Function) objective function can't be null");
			throw new IllegalArgumentException(
					ErrorMessages.NULL_OBJECTIVE_FUNCTION);
		}
		this.origObjectiveFunction = objectiveFunction;

		// if only original objective function is given check parameters
		// and create constrained objective function

		if (this.lowerBound==null) {
			Logger.error(this,"restart() lower bound is null");
			throw new IllegalArgumentException(
					ErrorMessages.NULL_LOWER_BOUND);
		}
		Vector searchSpaceLowerBound =
				new Vector(lowerBound);

		if (this.upperBound == null) {
			Logger.error(this,"restart() upper bound is null");
			throw new IllegalArgumentException(
					ErrorMessages.NULL_UPPER_BOUND);
		}
		Vector searchSpaceUpperBound =
				new Vector(this.upperBound);



		if (searchSpaceLowerBound.getDimension() != searchSpaceUpperBound.getDimension()) {
			Logger.error(this,"restart() lower bound and upper bound dimension mismatch");
			throw new IllegalArgumentException(
				ErrorMessages
					.ARGUMENS_INCOMPATIBLE("Bounds of searchspace"));
		}

		// store dimension count of space
		this.dimension = searchSpaceLowerBound.getDimension();

		// check bounds for monotonity
		for (int i = 1; i <= this.dimension; i++) {
			if (searchSpaceLowerBound.getCoordinate(i) > searchSpaceUpperBound.getCoordinate(i)) {
				Logger.error(this,"restart() lower bound is above upper bound ({0} > {1}) at dimension {2}",
					String.valueOf(searchSpaceLowerBound.getCoordinate(i)),
					String.valueOf(searchSpaceUpperBound.getCoordinate(i)),
					String.valueOf(i)
					);
				throw new IllegalArgumentException(
					ErrorMessages
					.ARGUMENS_INCOMPATIBLE("Bounds of searchspace"));
			}
		}

		// ask the objective function if dimension and bounds are acceptable
		if(!(this.origObjectiveFunction.isParameterAcceptable(searchSpaceLowerBound, searchSpaceUpperBound))) {
			Logger.error(this,"restart() parameters not acceptable for objective function");
			throw new IllegalArgumentException(
				ErrorMessages
				.ARGUMENS_INCOMPATIBLE("Unacceptable parameters"));
		}

		// calculate searchSpaceCenter and searchSpaceRadius
		searchSpaceUpperBound.map(
				(left, right) -> (left - right) / 2.0d,
				searchSpaceLowerBound);
		searchSpaceLowerBound.map((left, right) -> (left + right),
				searchSpaceUpperBound);
		this.searchSpaceCenter = new Vector(
				searchSpaceLowerBound);
		this.searchSpaceRadius = new Vector(
				searchSpaceUpperBound);

		// create constrained objective function
		this.objectiveFunction = new ConstrainedFunction(
				this.origObjectiveFunction, this.searchSpaceCenter,
				this.searchSpaceRadius);
	}

	public void setObjectiveFunction(ConstrainedFunction function) {
		isRunnable = false;
		if (function == null) {
			Logger.error(this,"setObjectiveFunction(ConstrainedFunction) objective function can't be null");
			throw new IllegalArgumentException(
					ErrorMessages.NULL_OBJECTIVE_FUNCTION);
		}
		this.objectiveFunction = function;
	}

	public void setUpperBoundOfSearchSpace(Vector upperBound) {
		isRunnable=false;
		if (upperBound == null) {
			Logger.error(this,"setUpperBoundOfSearchSpace(Vector) upper bound can't be null");
			throw new IllegalArgumentException(
					ErrorMessages.NULL_UPPER_BOUND);
		}
		this.upperBound = upperBound;
	}

	public void setLowerBoundOfSearchSpace(Vector lowerBound) {
		isRunnable=false;
		if (lowerBound == null) {
			Logger.error(this,"setLowerBoundOfSearchSpace(Vector) lower bound can't be null");
			throw new IllegalArgumentException(
					ErrorMessages.NULL_LOWER_BOUND);
		}
		this.lowerBound = lowerBound;
	}

	// Implementation of actual optimizer algorithm
	public abstract void run();

	public AbstractOptimizer() {
		this.objectiveFunction = null;
		this.numberOfFunctionEvaluations = 0;
		this.configuration = new OptimizerConfiguration<T>();
		isRunnable = false;
	}

	// Totally wipe out parameters
	public void reset() {
		Logger.trace(this,"reset() invoked");
		isRunnable = false;
		this.objectiveFunction = null;
		this.origObjectiveFunction = null;
		this.optimum = null;
		this.optimumValue = Double.MAX_VALUE;

		searchSpaceCenter = null;
		searchSpaceRadius = null;
	}

	public T getOptimum() {
		return this.optimum;
	}

	public double getOptimumValue() {
		return this.optimumValue;
	}

	/*public Function getObjectiveFunction() {
		return this.objectiveFunction;
	}
	*/
	public long getNumberOfFunctionEvaluations() {
		return numberOfFunctionEvaluations;
	}

	public AbstractOptimizer<T> getSerializableInstance(){
		Logger.trace(this,"getSerializableInstance() invoked");
		try{
			AbstractOptimizer<T> obj = (AbstractOptimizer<T>) this.clone();
			obj.configuration = new OptimizerConfiguration<T>();
			obj.configuration.addAll(this.configuration);
			obj.isRunnable = false;
			return obj;
		}catch(CloneNotSupportedException e){
			e.printStackTrace();
			Logger.error(this,"getSerializableInstance() CloneNotSupportedException");
		}
		return null;
	}


	/**
	 * Helper class for creating optimizer from XML file
	 */
	static public class Builder {

		private Object createObject(String className, String basePackage, NodeList nodeList) {

			try {

				className = basePackage + className;

				Object[] arguments = {1};
				Object builderObject = Class.forName(className+"$Builder").newInstance();

				for (int i = 0; i < nodeList.getLength(); i++) {
					Node childNode = nodeList.item(i);

					if (childNode.getNodeType() == Node.ELEMENT_NODE) {

						Element nodeElement = (Element)childNode;
						String subBasePackage = basePackage;
						if (nodeElement.hasAttribute("package")){
							subBasePackage = nodeElement.getAttribute("package");
							// if separator is not present append it to the base package
							if (subBasePackage.length() > 0 && subBasePackage.charAt(subBasePackage.length() - 1) != '.'){
								subBasePackage += '.';
							}
						}

						if (nodeElement.hasAttribute("class")) {
							// Inject class like dependency
							String elementClass = nodeElement.getAttribute("class");
							arguments[0] = createObject(elementClass, subBasePackage, childNode.getChildNodes());
							String methodName = "set"+childNode.getNodeName();
							String parameterName = subBasePackage + elementClass;
							Method method = null;

							try{
								method = findApplicableMethod(builderObject.getClass(), methodName, Class.forName(parameterName));
							}catch(IllegalArgumentException e){
								Logger.error(this,"createObject(String, String, NodeList) not found method for  name: {0}", methodName);
								throw e;
							}

							if (method == null){
								Logger.error(this,"createObject(String, String, NodeList) not found applicable method for {0}({1})", methodName, parameterName);
								throw new IllegalArgumentException(String.format("No applicable method found for %s(%s)", methodName, parameterName));
							}

							method.invoke(builderObject, arguments);

						} else if (nodeElement.hasAttribute("type")){
							// Inject primitive like dependency
							String typeString = nodeElement.getAttribute("type").toLowerCase();

							if ("long".equals(typeString)) {
								arguments[0]=(long)Integer.parseInt((nodeElement.getTextContent()).trim());
								builderObject.getClass().getMethod("set"+nodeElement.getNodeName(), long.class).invoke(builderObject, arguments);
							} else if ("double".equals(typeString)) {
								arguments[0]=Double.parseDouble((nodeElement.getTextContent()).trim());
								builderObject.getClass().getMethod("set"+nodeElement.getNodeName(), double.class).invoke(builderObject, arguments);
							} else if ("boolean".equals(typeString)) {
								arguments[0]=Boolean.parseBoolean((nodeElement.getTextContent()).trim());
								builderObject.getClass().getMethod("set"+nodeElement.getNodeName(), boolean.class).invoke(builderObject, arguments);
							} else if ("string".equals(typeString)) {
								arguments[0]=(nodeElement.getTextContent()).trim();
								builderObject.getClass().getMethod("set"+nodeElement.getNodeName(), String.class).invoke(builderObject, arguments);
							} else {
								Logger.error(this,"createObject(String, String, NodeList) Unknown type: {0}",typeString);
								throw new IllegalArgumentException(
									"Unknown type: "+nodeElement.getTextContent());
							}
						} else {
							Logger.error(this,"createObject(String, String, NodeList) No type or class attribute is given");
							throw new IllegalArgumentException("No type or class attribute is given");
						}
					}
				}

				Object res = (builderObject.getClass().getDeclaredMethod("build").invoke(builderObject));

				Logger.trace(this,"createObject(String, String, NodeList) \"{0}\" type object is created",className);

				return res;

			}  catch (Exception ie) {
				ie.printStackTrace();
				Logger.error(this,"createObject(String, String, NodeList) Exception: {0}",ie.getMessage());
			}

			return null;
		}

		public Method findApplicableMethod(Class clazz, String name, Class... types){
			Method[] methods = clazz.getMethods();

			boolean foundForName = false;

			for (int i = 0; i < methods.length; i++){
				if (!methods[i].getName().equals(name)) continue;
				foundForName = true;
				if (methods[i].getParameterCount() != types.length) continue;

				Class[] mtypes = methods[i].getParameterTypes();

				boolean matches = true;
				for (int j = 0; j < mtypes.length; j++){
					if (!mtypes[j].isAssignableFrom(types[j])){
						matches = false;
						break;
					}
				}

				if (matches) return methods[i];
			}

			if (!foundForName){
				throw new IllegalArgumentException("No applicable method found for name: " + name);
			}

			return null;
		}

		public Optimizer build(String xmlFileName) {

			try {

				File fXmlFile = new File(xmlFileName);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(fXmlFile);

				Element optimizer = doc.getDocumentElement();

				String temp = optimizer.getAttribute("class");

				String basePackage = "";
				if (optimizer.hasAttribute("package")){
					basePackage = optimizer.getAttribute("package");
					// if separator is not present append it to the base package
					if (basePackage.length() > 0 && basePackage.charAt(basePackage.length() - 1) != '.'){
						basePackage += '.';
					}
				}

				Optimizer res = null;
				res = (Optimizer) createObject(temp, basePackage, doc.getDocumentElement().getChildNodes());

				if (res != null){
					Logger.info(this,"build(String) \"{0}{1}\" type optimizer is created",basePackage,temp);
					return res;
				}

			} catch (Exception e) {
				e.printStackTrace();
				Logger.error(this,"build(String) Exception: {0}",e.getMessage());
			}

			return null;
		}

	}
}
