package org.uszeged.inf.optimization.algorithm.optimizer;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.ConstrainedFunction;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Interface for optimizer classes.
 *
 * @author Balázs L. Lévai
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            type of objects the local optimizer optimize over
 */
public interface Optimizer<T extends Vector>{

	/* Keys for common configuration parameters of optimizers. */

	public static final String PARAM_MAX_FUNCTION_EVALUATIONS = "PARAM_MAX_FUNCTION_EVALUATION";
	public static final String PARAM_RELATIVE_CONVERGENCE = "PARAM_RELATIVE_CONVERGENCE";
	public static final String PARAM_MAX_ITERATIONS = "PARAM_MAX_ITERATION";
	public static final String PARAM_MAX_RUNTIME = "PARAM_MAX_RUNTIME";

	/**
	 * Implementation of an actual optimizer algorithm.
	 */
	public void run();

	/**
	 * Resets the optimizer to its default state base on its configuration.
	 */
	public void reset();

	public T getOptimum();

	public double getOptimumValue();

	/**
	 * Resets the local optimizer to its default state base on its configuration and setting values.
	 */
	public void restart();

	public void setObjectiveFunction(ConstrainedFunction function);

	public void setObjectiveFunction(Function function);

	public void setUpperBoundOfSearchSpace(Vector upperBound);

	public void setLowerBoundOfSearchSpace(Vector lowerBound);

	//public Function getObjectiveFunction();

	public long getNumberOfFunctionEvaluations();
}
