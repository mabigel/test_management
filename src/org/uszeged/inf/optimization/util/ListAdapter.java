package org.uszeged.inf.optimization.util;

import java.util.List;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;

/**
 * Adapter class implements List interface functions with default exception.
 *
 * @author Dániel Zombori
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            type of objects the list contains
 */
public class ListAdapter<T> implements List<T>{

    private static final String MSG = "Function is not implemented!";

    public boolean add(T e){
        throw new NotImplementedException(MSG);
    }

    public void add(int index, T element){
        throw new NotImplementedException(MSG);
    }

    public boolean addAll(Collection<? extends T> c){
        throw new NotImplementedException(MSG);
    }

    public boolean addAll(int index, Collection<? extends T> c){
        throw new NotImplementedException(MSG);
    }

    public void clear(){
        throw new NotImplementedException(MSG);
    }

    public boolean contains(Object o){
        throw new NotImplementedException(MSG);
    }

    public boolean containsAll(Collection<?> c){
        throw new NotImplementedException(MSG);
    }

    public boolean equals(Object o){
        throw new NotImplementedException(MSG);
    }

    public T get(int index){
        throw new NotImplementedException(MSG);
    }

    public int hashCode(){
        throw new NotImplementedException(MSG);
    }

    public int indexOf(Object o){
        throw new NotImplementedException(MSG);
    }

    public boolean isEmpty(){
        throw new NotImplementedException(MSG);
    }

    public int lastIndexOf(Object o){
        throw new NotImplementedException(MSG);
    }

    public T remove(int index){
        throw new NotImplementedException(MSG);
    }

    public boolean remove(Object o){
        throw new NotImplementedException(MSG);
    }

    public boolean removeAll(Collection<?> c){
        throw new NotImplementedException(MSG);
    }

    public boolean retainAll(Collection<?> c){
        throw new NotImplementedException(MSG);
    }

    public T set(int index, T element){
        throw new NotImplementedException(MSG);
    }

    public int size(){
        throw new NotImplementedException(MSG);
    }

    public List<T> subList(int fromIndex, int toIndex){
        throw new NotImplementedException(MSG);
    }

    public Object[] toArray(){
        throw new NotImplementedException(MSG);
    }

    public <T> T[] toArray(T[] a){
        throw new NotImplementedException(MSG);
    }

    public ListIterator<T> listIterator(){
        throw new NotImplementedException(MSG);
    }

    public ListIterator<T> listIterator(int index){
        throw new NotImplementedException(MSG);
    }

    public Iterator<T> iterator(){
        throw new NotImplementedException(MSG);
    }
}
