package org.uszeged.inf.optimization.util;

import java.io.PrintStream;
import java.util.Date;
import java.util.Calendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.io.*;

/**
 * Helps log of execution.
 *
 * @author Dániel Zombori
 * @version 1.0
 * @since 1.0
 */
public class Logger{

    public enum Level{
        SILENT (1000),
        ERROR (100),
        WARN_ATTENTION (80),
        WARN_OBVIOUS (70),
        INFO (60),
        DEBUG (40),
        TRACE (20);

        private final int val;
        public int val(){return val;}
        Level(int val){
            this.val = val;
        }
    }

    // Level.SILENT
    // Logging is turned off, text printed only in very special cases.

    // Level.ERROR
    // Something terribly wrong had happened, that must be investigated
    // immediately. No system can tolerate items logged on this level. Example:
    // NullPointerException, mission critical use case cannot be continued.

    // Level.WARN_x
    // The process might be continued, but take extra caution. Actually
    // I always wanted to have two levels here: one for obvious problems where
    // work-around exists (for example: “Current data unavailable, using cached
    // values”) and second (name it: ATTENTION) for potential problems and
    // suggestions. Example: “Application running in development mode” or
    // “Administration console is not secured with a password”. The application
    // can tolerate warning messages, but they should always be examined.

    // Level.INFO
    // Important business process has finished. In ideal world, administrator
    // or advanced user should be able to understand Level.INFO messages and quickly
    // find out what the application is doing. For example if an application is
    // all about booking airplane tickets, there should be only one Level.INFO
    // statement per each ticket saying “[Who] booked ticket from [Where] to
    // [Where]”. Other definition of Level.INFO message: each action that changes the
    // state of the application significantly (database update, external
    // system request).

    // Level.DEBUG
    // Developers stuff.

    // Level.TRACE
    // Very detailed information, intended only for development. You might keep
    // trace messages for a short period of time after deployment on production
    // environment, but treat these log statements as temporary, that should or
    // might be turned-off eventually. The distinction between Level.DEBUG and Level.TRACE
    // is the most difficult, but if you put logging statement and remove it
    // after the feature has been developed and tested, it should probably be
    // on Level.TRACE level.

    private static Level minimumLogLevel = Level.INFO;
    private static PrintStream logChannel = System.out;
    private static Object logOrderLock = new Object();
    /* Setters/Getters */

    public static Level getMinimumLogLevel(){
        return minimumLogLevel;
    }

    public static void setMinimumLogLevel(Level val){
        minimumLogLevel = val;
    }

    public static PrintStream getLogChannel(){
        return logChannel;
    }

    public static void setLogChannel(PrintStream channel){
        if (channel == null){
            logString(Level.ERROR,"setLogChannel(PrintStream channel) channel is null");
            throw new NullPointerException("Channel can't be null.");
        }
        logChannel = channel;
    }

    public static void setLogChannel(String file) throws FileNotFoundException, UnsupportedEncodingException{
        if (file == null){
            logString(Level.ERROR,"setLogChannel(String file) file name is null");
            throw new NullPointerException("File name can't be null.");
        }
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        Calendar cal = Calendar.getInstance();
        String fileName = file+dateFormat.format(cal.getTime())+".log";
        PrintStream writer = new PrintStream(new FileOutputStream(fileName, true), true, "UTF-8");

        logChannel = writer;
    }

    /**
     * Prints message in format: {level}: {message} if the log level is low
     * enough.
     */
    public static void logString(Level level, String message){
        if (minimumLogLevel.val <= level.val){
            synchronized(logOrderLock){
                DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                Calendar cal = Calendar.getInstance();
                logChannel.println("["+Thread.currentThread().getName()+"]["+dateFormat.format(cal.getTime())+"]["+level+"] "+message);
            }
        }
    }

    /**
     * Prints message, substitutes arguments into message in format {x}
     * where x is argument index, starts at 0.
     * If not enough arguments logs ERROR and throws NullPointerException.
     * If x format is incorrect logs ERROR and throws NumberFormatException.
     */
    public static void log(Level level,Object o, String message, String[] args){
        log(level,o == null ? null : o.getClass(),message,args);
    }
    public static void log(Level level,Class c, String message, String[] args){
        if (minimumLogLevel.val > level.val) return;
        StringBuilder sb = new StringBuilder();
        int argi = -1;

        if (c != null){
            sb.append(c.getCanonicalName());
            sb.append('.');
        }

        String[] parts = (message+'#').split("[{}]");
        for (int i = 0;i < parts.length;i++){
            if ((i & 1) == 1){
                try{
                    if (parts[i].equals("")){
                        throw new NumberFormatException("Number field can't be empty!");
                    }
                    argi = Integer.parseInt(parts[i]);
                    sb.append(args[argi]);
                }catch(NumberFormatException e){
                    error(c,"log() invalid argument index format ({0})",parts[i]);
                    throw e;
                }catch(IndexOutOfBoundsException e){
                    error(c,"log() argument index ({0}) out of bounds",""+argi);
                    throw e;
                }
            }else{
                sb.append(parts[i]);
            }
        }

        sb.setLength(sb.length() - 1);

        logString(level,sb.toString());
    }

    public static void error(Object o, String message, String... args){
        log(Level.ERROR,o,message,args);
    }
    public static void error(Class c, String message, String... args){
        log(Level.ERROR,c,message,args);
    }

    public static void warnAttention(Object o, String message, String... args){
        log(Level.WARN_ATTENTION,o,message,args);
    }
    public static void warnAttention(Class c, String message, String... args){
        log(Level.WARN_ATTENTION,c,message,args);
    }


    public static void warnObvious(Object o, String message, String... args){
        log(Level.WARN_OBVIOUS,o,message,args);
    }
    public static void warnObvious(Class c, String message, String... args){
        log(Level.WARN_OBVIOUS,c,message,args);
    }


    public static void info(Object o, String message, String... args){
        log(Level.INFO,o,message,args);
    }
    public static void info(Class c, String message, String... args){
        log(Level.INFO,c,message,args);
    }


    public static void debug(Object o, String message, String... args){
        log(Level.DEBUG,o,message,args);
    }
    public static void debug(Class c, String message, String... args){
        log(Level.DEBUG,c,message,args);
    }


    public static void trace(Object o, String message, String... args){
        log(Level.TRACE,o,message,args);
    }
    public static void trace(Class c, String message, String... args){
        log(Level.TRACE,c,message,args);
    }
}
