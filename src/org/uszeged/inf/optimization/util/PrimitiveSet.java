package org.uszeged.inf.optimization.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Basic utility class to store different primitive types together via string
 * conversion. Get methods follow the early detection exception throwing
 * strategy.
 *
 * @author Balázs L. Lévai
 * @version 1.0
 * @since 1.0
 */
public abstract class PrimitiveSet {

	private Map<String, String> texts;

	public PrimitiveSet() {
		super();
		this.texts = new HashMap<>();
	}

	public PrimitiveSet(PrimitiveSet set) {
		super();
		this.texts = new HashMap<>();
		this.texts.putAll(set.texts);
	}

	public void addLong(String key, long value) {
		texts.put(key, Long.toString(value));
	}

	public long getLong(String key) {
		if (texts.containsKey(key)) {
			return Long.parseLong(texts.get(key));
		} else {
			throw new NullPointerException(ErrorMessages.NO_SUCH_KEY(key));
		}
	}

	public void addDouble(String key, double value) {
		texts.put(key, Double.toString(value));
	}

	public double getDouble(String key) {
		if (texts.containsKey(key)) {
			return Double.parseDouble(texts.get(key));
		} else {
			throw new NullPointerException(ErrorMessages.NO_SUCH_KEY(key));
		}
	}

	public void addString(String key, String value) {
		texts.put(key, value);
	}

	public void addAll(PrimitiveSet set) {
		texts.putAll(set.texts);
	}

	public String getString(String key) {
		if (texts.containsKey(key)) {
			return texts.get(key);
		} else {
			throw new NullPointerException(ErrorMessages.NO_SUCH_KEY(key));
		}
	}

	public Set<Map.Entry<String, String>> getEntrySet(){
		return texts.entrySet();
	}

	public boolean containsKey(String key) {
		return texts.containsKey(key);
	}

	public void clear() {
		texts.clear();
	}

}
