package org.uszeged.inf.optimization.util;

import org.uszeged.inf.optimization.data.Vector;

public class VectorOperations {
	
	/** 
	 * Subtract two Vectors from each other.
	 */
	public static Vector subtractVectors(Vector v1, Vector v2){

		Vector v = new Vector(v1.getDimension());

		for(int i =1; i<= v1.getDimension(); ++i){
			v.setCoordinate(i, v1.getCoordinate(i) - v2.getCoordinate(i));
		}

		return v;
	}
	
	/**
	 * Sum up two Vectors.
	 */
	public static Vector sumUpVectors(Vector v1, Vector v2){

		Vector v = new Vector(v1.getDimension());

		for(int i =1; i<= v1.getDimension(); ++i){
			v.setCoordinate(i, v1.getCoordinate(i) + v2.getCoordinate(i));
		}

		return v;
	}
	
	/**
	 * Multiplies a Vector by a given constant.
	 */
	public static Vector multiplyVector(Vector v, double multiplier){

		Vector vm = new Vector(v.getDimension());

		for(int i = 1; i<=v.getDimension(); ++i)
			vm.setCoordinate(i, v.getCoordinate(i)*multiplier);

		return vm;
	}
	
	/**
	 * Evaluates the scalar product of two Vectors.
	 */
	public static double scalarProduct(Vector a, Vector b){
		double scalarProduct = 0;

		for(int i=1; i<=a.getDimension();++i)
			scalarProduct += (a.getCoordinate(i)*b.getCoordinate(i));

		return scalarProduct;
	}
	
	/**
	 * Evaluates the vector projection of v onto u;
	 */
	public static Vector projection(Vector u, Vector v){

		Vector vOnU;

		double multiplier = scalarProduct(u, v) 
				/ (Vector.norm(u) * Vector.norm(u));

		vOnU = multiplyVector(u, multiplier);

		return vOnU;

	}

}
