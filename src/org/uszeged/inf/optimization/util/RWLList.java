package org.uszeged.inf.optimization.util;

import java.util.List;

/**
 * Decorator class implements read-write locked list. Anyone can read the list
 * until someone acquires a write lock.
 *
 * @author Dániel Zombori
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            type of objects the list contains
 */
public class RWLList<T> extends ListAdapter<T>{

    private ReadWriteLock locker;
    private List<T> list;

    public RWLList(List<T> l){
        this.list = l;
        locker = new ReadWriteLock();
    }

    public boolean add(T e){
        boolean ret;
        locker.lockWrite();
        ret = list.add(e);
        locker.unlockWrite();
        return ret;
    }

    public void clear(){
        locker.lockWrite();
        list.clear();
        locker.unlockWrite();
    }

    public int size(){
        int ret;
        locker.lockRead();
        ret = list.size();
        locker.unlockRead();
        return ret;
    }

    public T get(int index){
        T ret;
        locker.lockRead();
        ret = list.get(index);
        locker.unlockRead();
        return ret;
    }
}

class ReadWriteLock{
    private int readers       = 0;
    private int writers       = 0;
    private int writeRequests = 0;

    public synchronized void lockRead(){
        try{
            while(writers > 0 || writeRequests > 0){
                wait();
            }
            readers++;
        }catch(InterruptedException e){
            e.printStackTrace();
            System.exit(1);
        }
    }

    public synchronized void unlockRead(){
        readers--;
        notifyAll();
    }

    public synchronized void lockWrite(){
        try{
            writeRequests++;

            while(readers > 0 || writers > 0){
                wait();
            }
            writeRequests--;
            writers++;
        }catch(InterruptedException e){
            e.printStackTrace();
            System.exit(1);
        }
    }

    public synchronized void unlockWrite(){
        writers--;
        notifyAll();
    }
}
