package org.uszeged.inf.optimization.util;

import java.util.Iterator;
import java.util.Collection;
import java.util.LinkedList;

import java.util.concurrent.atomic.AtomicInteger;
//import java.util.concurrent.locks.ReentrantLock;

/**
 * LinkedList implementation with exclusion on the list item that is currently
 * being added.
 *
 * @author Dániel Zombori
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            type of objects the list contains
 */
public class SLinkedList<T> extends ListAdapter<T>{
    private AtomicInteger size;
    private ListItem<T> head;
    private ListItem<T> tail;

    private Object additionLock;
    private Object removeLock;

    public SLinkedList(){
        size = new AtomicInteger();
        head = new ListItem<T>(null,null);
        tail = head;

        additionLock = new Object();
        removeLock = new Object();
    }

    public int size(){
        return size.get();
    }

    public boolean add(T e){
        synchronized(additionLock){
            synchronized(tail){
                ListItem<T> n = new ListItem<T>(e,null);
                synchronized(n){
                    tail.next = n;
                    tail = tail.next;
                }
                size.incrementAndGet();
            }
        }
        return true;
    }

    public T get(int index){
        ListItem<T> item = getItem(index);
        if (item == null) return null;
        return item.data;
    }

    protected ListItem<T> getItem(int index){
        ListItem<T> i = head;
        while(index >= 0){
            if (i.next == null) return null;
            index--;
            i = i.next;
        }
        return i;
    }

    public T removeHead(){
        T res = null;
        ListItem<T> del;
        synchronized(removeLock){
            synchronized(head){
                del = head.next;
                if (del == null) return null;
                synchronized(del){
                    res = del.data;
                    size.decrementAndGet();
                    head.next = del.next;
                    synchronized(tail){
                        if (tail == del){
                            tail = head;
                        }
                    }
                }
            }
        }
        return res;
    }


    public T remove(int index){
        if (true){
            throw new NotImplementedException("Do not use this now!");
        }
        if (index < 0 ||true){
            throw new IndexOutOfBoundsException("Index: "+index);
        }
        ListItem<T> pre = getItem(index-1);
        if (pre == null || pre.next == null) return null;
        T res = null;
        synchronized(pre.next){
            synchronized(pre){
                ListItem<T> del = pre.next;
                if (del == null) return null;
                size.decrementAndGet();
                if (tail == del){
                    tail = pre;
                }
                pre.next = del.next;
                res = del.data;
            }
        }
        if (tail == null){
            System.out.println("remove NULL");
        }
        return res;
    }

    public boolean addAll(Collection<? extends T> c){
        ListItem<T> fitem = new ListItem<T>(null,null);
        ListItem<T> ftail = fitem;
        int s = 0;
        for(T e : c){
            s++;
            ftail.next = new ListItem<T>(e,null);
            ftail = ftail.next;
        }
        if (fitem.next == null) return true;
        synchronized(additionLock){
            if (tail == null){
                System.out.println("addAll TAIL");
            }
            synchronized(tail){
                synchronized(fitem.next){
                    tail.next = fitem.next;
                    tail = ftail;
                }
                size.addAndGet(s);
            }
        }
        return true;
    }

    public void clear(){
        size.set(0);
        head = new ListItem<T>(null,null);
        tail = head;
    }

    public Iterator<T> iterator(){
        return new SLLIterator();
    }

    public <E> E[] toArray(E[] a){
        LinkedList<E> l = new LinkedList<E>();
        Iterator<T> iter = iterator();
        while(iter.hasNext()){
            l.add((E) iter.next());
        }
        return l.toArray(a);
    }

    public Object[] toArray(){
        LinkedList<T> l = new LinkedList<T>();
        Iterator<T> iter = iterator();
        while(iter.hasNext()){
            l.add((T) iter.next());
        }
        return l.toArray();
    }


    /**
     * Iterator class for SLinkedList.
     *
     * @author Dániel Zombori
     * @since 1.0
     * @version 1.0
     * @param <T>
     *            type of objects the list contains
     */
    private class SLLIterator implements Iterator<T>{

        private ListItem<T> item;
        private int index;

        public SLLIterator(){
            item = head;
            index = -1;
        }

        public boolean hasNext(){
            return item != null && item.next != null;
        }

        public T next(){
            index++;
            item = item.next;
            if (item == null) return null;
            return item.data;
        }
    }
}

class ListItem<T>{
    public T data;
    //public ListItem<T> prev;
    public ListItem<T> next;

    public ListItem(T e,ListItem<T> n){
        data = e;
        //prev = p;
        next = n;
    }
}
