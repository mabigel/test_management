package org.uszeged.inf.optimization.util;

/**
 * Fminbndn is a linesearch optimizer, that fits a curve on three points, and
 * evaluates that curve's minimum point.
 *
 * @author Mester Abigel
 * @version 1.0
 */
public class Fminbndn {

  /**
  * Evaluates the minminum point of the curve.
  */
  public static double fit(double[] x, double[] fx) {

    // x = b - 1/2*((b-a)^2*(fb-fc)-(b-c)^2*(fb-fa)) /
    //              ((b-a)*(fb-fc)-(b-c)*(fb-fa))
    return x[1] - 0.5 * (Math.pow((x[1] - x[0]), 2) * (fx[1] - fx[2]) -
            Math.pow((x[1] - x[2]), 2) * (fx[1] - fx[0])) /
            ((x[1] - x[0]) * (fx[1] - fx[2]) - (x[1] - x[2]) * (fx[1] - fx[0]));

  }

}
