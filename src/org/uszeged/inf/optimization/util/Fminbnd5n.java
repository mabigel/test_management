package org.uszeged.inf.optimization.util;

/**
 * Fminbnd5 is a linesearch optimizer, that fits a curve on five points, and
 * evaluates that curve's minimum point.
 *
 * @author Mester Abigel
 * @version 1.0
 */
public class Fminbnd5n {

	/**
	 * Gaussian elimination with partial pivoting.
	 */
	private static double[] solve(double[][] A, double[] b) {
		int N = b.length;
		for(int p = 0; p < N; p++) {

			// Find pivot row and swap
			int max = p;
			for(int i = p + 1; i < N; i++) {
				max = Math.abs(A[i][p]) > Math.abs(A[max][p]) ? i : max;
			}

			double temp[] = A[p];
			A[p] = A[max];
			A[max] = temp;
			double t = b[p];
			b[p] = b[max];
			b[max] = t;

			// Pivot within A and b
			for(int i = p + 1; i < N; i++) {
				double alpha = A[i][p] / A[p][p];
				b[i] -= alpha * b[p];

				for (int j = p; j < N; j++) {
					A[i][j] -= alpha * A[p][j];
				}
			}
		}

		// Back substitution
		double x[] = new double[N];
		for(int i = N - 1; i >= 0; i--) {
			double sum = 0.0;
			for(int j = i + 1; j < N; j++) {
				sum += A[i][j] * x[j];
			}
			x[i] = (b[i] - sum) / A[i][i];
			if(Double.isNaN(x[i])) {
				x[i] = 1;
			}
		}

		return x;
	}

	/**
	 * Complex cuberoot.
	 */
	private static double[] complexCubic(double re, double im) {

		double cubicRes[] = new double[3];
		double r = Math.sqrt(Math.pow(re, 2) + Math.pow(im, 2));
		double fi = re >= 0.0 ? Math.atan(im / re) : Math.PI - Math.atan(im / re);

		for(int k = 0; k < 3; k++) {
			cubicRes[k] = Math.pow(r, 1.0 / 3) * Math.cos((fi + (2 * k * Math.PI)) / 3);
		}

		return cubicRes;
	}

	private static double thirdDegree(double a, double b, double c, double d) {

		double p, q, alpha = Double.NaN, beta = Double.NaN, sqrt1, sum1;

		/**
		 * Cardano-formula third degree equation solution.
		 */
		p = c / a - Math.pow(b, 2) / (3 * Math.pow(a, 2));
		q = d / a + (2 * Math.pow(b, 3)) / (27 * Math.pow(a, 3)) - (b * c) / (3 * Math.pow(a, 2));

		sum1 = Math.pow((q / 2), 2) + Math.pow((p / 3), 3);
		sqrt1 = Math.sqrt(Math.abs(sum1));

		if (sum1 < 0) {
			// Complex
			double alphaComp[] = new double[3];
			double betaComp[] = new double[3];
			alphaComp = complexCubic(-q / 2, sqrt1);
			betaComp = complexCubic(-q / 2, -sqrt1);
			for(int i = 0; i < 3; i++) {
				if(Math.abs(alphaComp[i] - betaComp[i]) < 1e-4) {
					alpha = alphaComp[i];
					beta = betaComp[i];
				}
			}

		} else {
			// Real
			if((-q / 2 + sqrt1) >= 0) {
				alpha = Math.pow((-q / 2 + sqrt1), 1.0 / 3);
			}	else {
				alpha = -Math.pow(-(-q / 2 + sqrt1), 1.0 / 3);
			}
			if((-q / 2 - sqrt1) >= 0) {
				beta = Math.pow((-q / 2 - sqrt1), 1.0 / 3);
			}	else {
				beta = -Math.pow(-(-q / 2 - sqrt1), 1.0 / 3);
			}
		}

		return alpha + beta + (-b / (3 * a));

	}

	/**
	 * Second degree equation solution.
	 */
	private static double secondDegree(double a, double b, double c) throws IllegalArgumentException {
		double disc = Math.pow(b, 2) - 4 * a * c;

		if(disc < 0) {
			return Double.NaN;
		}

		return (-b + Math.sqrt(disc)) / (2 * a);

	}

	public static double fit(double[] x, double[] fx) {
		int N = 5;

		for(int i = 0; i < N - 1; i++) {
			for(int j = i + 1; j < N; j++) {
				// Check if same x places have different fx values
				if(x[i] == x[j] && fx[i] != fx[j]) {
					return Double.NaN;
				// Check if there are same x places and delete them
				} else if(x[i] == x[j]) {
					for(int k = j; k < N - 1; k++) {
						x[k] = x[k + 1];
						fx[k] = fx[k + 1];
					}

					// Number of indexes we have to check decreased by one
					N--;

					// All array elements shifted to zero by one place and j
					// will be incremented therefore j has to be shifted too
					j--;
				}
			}
		}

		double A[][] = new double[N][N];
		double b[] = new double[N];

		// Create A matrix for Gauss elimination
		for(int i = 0; i < N; i++) {
			int k = N - 1;
			for(int j = 0; j < N; j++) {
				A[i][j] = Math.pow(x[i], k);
				k--;
			}
			b[i] = fx[i];
		}

		// Gauss elimination
		double pol[] = solve(A, b);

		// Derivate and count the number variables in polynomial
		int k = N - 1;
		int numb = N - 1;
		for(int i = 0; i < N; i++) {
			pol[i] *= k;
			k--;
			if(pol[i] == 0.0) {
				numb--;
			}
		}

		if(numb < 1) {
			return Double.NaN;
		}

		double result = Double.NaN;

		// Coefficients
		switch(numb) {
			case 4:
				result = thirdDegree(pol[0], pol[1], pol[2], pol[3]);
				break;
			case 3:
				result = secondDegree(pol[1], pol[2], pol[3]);
				break;
			case 2:
				result = -pol[3] / pol[2];
				break;
			default:
				break;
		}

		return result;

	}

}
