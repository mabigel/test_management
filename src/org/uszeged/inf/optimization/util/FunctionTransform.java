package org.uszeged.inf.optimization.util;

import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.data.Matrix;

/**
 * Utility class for function transformations.
 *
 * @author Dániel Zombori
 * @version 1.0
 */
public class FunctionTransform{

    private Vector translation;
    private Matrix rotation;
    private double valueOffset;

    private FunctionTransform(){

    }

    private FunctionTransform(int dim){
        translation = computeTranslation(dim);
        rotation = computeRotation(dim).transpose();
        valueOffset = computeValueOffset();
    }

    private static Object lock = new Object();
    private static FunctionTransform singletonInstance = null;

    public static void init(int dim){
        synchronized(lock){
            if (singletonInstance == null){
                singletonInstance = new FunctionTransform();
                singletonInstance.translation = new Vector(dim);
                singletonInstance.rotation = Matrix.eye(dim);
                singletonInstance.valueOffset = 0;
            } else {
                singletonInstance.translation = computeTranslation(dim);
                singletonInstance.rotation = computeRotation(dim).transpose();
                singletonInstance.valueOffset = computeValueOffset();
            }
        }
    }

    public static FunctionTransform singleton(){
        if (singletonInstance == null){
            synchronized(lock){
                singletonInstance = new FunctionTransform();
            }
        }
        return singletonInstance;
    }

    private static Vector computeTranslation(int dim){
        Vector v = Vector.randomVector(dim).map(
            x -> 8 * Math.floor(1e4*x) * 1e-4 - 4
        ).map(
            x -> x == 0 ? -1e-5 : x
        );

        return v;
    }

    private static Matrix computeRotation(int dim){
        Matrix m = gaussianMatrix(dim);
        double val;

        for(int i = 1; i <= dim; i++){
            Vector icol = m.getCol(i);
            for(int j = 1; j < i; j++){
                Vector jcol = m.getCol(j);
                double scalar = Vector.scalarMul(icol,jcol);
                icol = icol.map((x,y)->x-scalar*y,jcol);
            }
            Vector.normalize(icol);
        }
        return m;
    }

    private static double computeValueOffset(){
        double random = 100*gaussian()/gaussian();
        return Math.min(1000, Math.max(-1000, Math.round(100*random)/100));
    }

    public Vector rotVector(Vector x){
        // 1xN matrix
        Matrix mat = x.map((l,r)->l-r,translation).toMatrix();
        mat = mat.rightMul(rotation);
        return mat.toVector();
    }

    public double rotValue(double value){
        return value + valueOffset;
    }

    public double inverseRotValue(double value){
        return value - valueOffset;
    }

    private static double gaussian(){
        Vector v = Vector.randomVector(2);
        double val = Math.sqrt(-2*Math.log(v.getCoordinate(1)))
                    * Math.cos(2*Math.PI*v.getCoordinate(2));
        return val == 0 ? 1e-99 : val;
    }

    private static Matrix gaussianMatrix(int dim){
        Matrix m = new Matrix(dim,dim);
        for(int i = 1; i <= dim; i++){
            for(int j = 1; j <= dim; j++){
                m.setCoordinate(i,j,gaussian());
            }
        }
        return m;
    }
}
