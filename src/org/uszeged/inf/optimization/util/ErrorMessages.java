package org.uszeged.inf.optimization.util;

/**
 * Collection of exceptions' error messages.
 *
 * @author Balázs L. Lévai
 * @since 1.0
 * @version 1.0
 */
public class ErrorMessages {

	public static final String NO_SUCH_KEY(String key) {
		return "There is no such key in the map as: " + key + "!";
	}

	public static final String VECTOR_INDEX(int index, int dimension) {
		return "Vector index " + (index) + " is out of bounds [" + 0 + ", "
				+ dimension + "]!";
	}

	public static final String VECTOR_DIMENSION(int dimensionA,
			int dimensionB) {
		return "Vectors have different dimension: " + dimensionA
				+ " is not equal " + dimensionB + "!";
	}

	public static final String DIMENSION_MUST_BE_POSITIVE = "Dimension must be positive.";

	public static final String ALPHA_MUST_BE_NUMBER = "Alpha must be a number.";

	public static final String GLOBAL_NOT_PARAMETERIZED_YET = "Global optimizer not parameterized yet.";

	public static final String LINESEARCH_NOT_PARAMETERIZED_YET = "Line search optimizer not parameterized yet.";

	public static final String LOCAL_NOT_PARAMETERIZED_YET = "Local optimizer not parameterized yet.";

	public static final String GRADIENT_NOT_PARAMETERIZED_YET = "Gradient function not parameterized yet.";

	public static final String NULL_STARTING_POINT = "Starting point cannot be null!";

	public static final String NULL_OBJECTIVE_FUNCTION = "Objective function cannot be null!";

	public static final String NULL_CLUSTERIZER = "Clusterizer cannot be null!";

	public static final String NULL_CLUSTER = "Cluster cannot be null!";

	public static final String EMPTY_CLUSTER = "Cluster cannot be empty!";

	public static final String NULL_LOCAL_OPTIMIZER = "Local optimizer cannot be null!";

	public static final String NULL_UPPER_BOUND = "Upper bound of search space cannot be null!";

	public static final String NULL_LOWER_BOUND = "Lower bound of search space cannot be null!";

	public static final String NULL_LINE_SEARCH_OPTIMIZER = "Linesearch optimizer cannot be null!";

	public static final String NULL_STEP_DIRECTION = "Starting direction cannot be null!";

	public static final String NULL_STEP_LENGTH = "Init step length cannot be null!";

	public static final String NULL_GRADIENT_FUNCTION = "Gradient function cannot be null!";

	public static final String MIN_DELTA_H_NEGATIVE = "Lower bound of delta h cannot be negative!";

	public static final String MIN_DELTA_H_NAN = "Lower bound of delta h cannot be NaN!";

	public static final String DELTA_H_SCALE_NAN = "Delta h scale coefficient cannot be NaN!";

	public static final String DELTA_H_SCALE_NOT_POSITIVE = "Delta h scale coefficient must be positive!";

	public static final String ZERO_PRODUCT_NAN = "Zero product coefficient cannot be NaN!";

	public static final String ZERO_PRODUCT_NEGATIVE = "Zero product coefficient cannot be negative!";

	public static final String GRADIENT_NOT_COMPUTED = "Gradient function not evaluated!";

	public static final String ILLEGAL_ARGUMENT(String argument,
			String condition) {
		return argument + " must be " + condition + "!";
	}

	public static final String ARGUMENT_OUT_OF_BOUNDS(String argument,
			double lowerBound, double upperBound) {
		return argument + " must be within [" + lowerBound + "," + upperBound
				+ "]!";
	}

	public static final String ARGUMENS_INCOMPATIBLE(String arguments) {
		return arguments + " must be compatible!";
	}

}
