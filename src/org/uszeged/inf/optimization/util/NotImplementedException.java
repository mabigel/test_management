package org.uszeged.inf.optimization.util;

/**
 * Exception for not implemented codes.
 *
 * @author Dániel Zombori
 * @since 1.0
 * @version 1.0
 */
class NotImplementedException extends RuntimeException{
    public NotImplementedException(){
        super();
    }
    public NotImplementedException(String msg){
        super(msg);
    }
}
