package org.uszeged.inf.optimization.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Utility class extending the functionality of PrimitiveSets allowing the user
 * to store chosen type of objects beside primitive types.
 * 
 * @author Balázs L. Lévai
 * @since 1.0
 * @version 1.0
 * @param <T>
 *            additional object type you also wish to store beside primitive
 *            types
 */
public class DataSet<T> extends PrimitiveSet {

	private Map<String, T> objects;

	public DataSet() {
		super();
		this.objects = new HashMap<String, T>();
	}

	public DataSet(DataSet<T> dataSet) {
		super(dataSet);
		this.objects = new HashMap<String, T>();
		this.objects.putAll(dataSet.objects);
	}

	public void addObject(String key, T value) {
		objects.put(key, value);
	}

	public void addAll(DataSet<T> set) {
		super.addAll(set);
		objects.putAll(set.objects);
	}

	public T getObject(String key) {
		if (objects.containsKey(key)) {
			return objects.get(key);
		} else {
			throw new NullPointerException(ErrorMessages.NO_SUCH_KEY(key));
		}
	}

	@Override
	public boolean containsKey(String key) {
		return super.containsKey(key) || objects.containsKey(key);
	}

	public boolean containsPrimitive(String key) {
		return super.containsKey(key);
	}

	public boolean containsObject(String key) {
		return objects.containsKey(key);
	}

}
