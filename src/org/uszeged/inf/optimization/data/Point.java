package org.uszeged.inf.optimization.data;

import java.util.Comparator;

/**
 * A point is a pair of a place and a value. The class makes us able to tie
 * function values to vectors.
 *
 * @author Balázs L. Lévai
 * @version 1.0
 * @since 1.0
 */
public class Point extends Vector {

	private double functionValue;

	public Point(double[] coordinates) {
		super(coordinates);
		this.functionValue = 0.0d;
	}

	public Point(double[] coordinates, double functionValue) {
		super(coordinates);
		this.functionValue = functionValue;
	}

	public Point(int dimension) {
		super(dimension);
		this.functionValue = 0.0d;
	}

	public Point(Vector v) {
		super(v);
		this.functionValue = 0.0d;
	}

	public Point(Point p) {
		super(p);
		this.functionValue = p.functionValue;
	}

	public double getFunctionValue() {
		return functionValue;
	}

	public void setFunctionValue(double functionValue) {
		this.functionValue = functionValue;
	}

	public void setFunctionValue(Function function) {
		this.functionValue = function.evaluate(this);
	}

	public String toString() {
		return this.functionValue + " : " + super.toString();
	}

	/**
	 * Comparator implementation to be able to sort points by function value in
	 * ascending order.
	 *
	 * @author Balázs L. Lévai
	 * @version 1.0
	 * @since 1.0
	 */
	public static class AscendingFunctionValueComparator implements
			Comparator<Point> {

		public int compare(Point o1, Point o2) {

			if (o1.functionValue < o2.functionValue) {
				return -1;
			} else if (o1.functionValue == o2.functionValue) {
				return 0;
			} else {
				return 1;
			}

		}

	}

	/**
	 * Comparator implementation to be able to sort points by function value in
	 * descending order.
	 *
	 * @author Balázs L. Lévai
	 * @version 1.0
	 * @since 1.0
	 */
	public static class DescendingFunctionValueComparator implements
			Comparator<Point> {

		public int compare(Point o1, Point o2) {

			if (o1.functionValue < o2.functionValue) {
				return 1;
			} else if (o1.functionValue == o2.functionValue) {
				return 0;
			} else {
				return -1;
			}

		}

	}

}
