package org.uszeged.inf.optimization.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Basic cluster class to be able to handle cluster elements and their centers
 * together delegating all expected functionality to the java List interface.
 * 
 * @author Balázs L. Lévai
 * @version 1.0
 * @since 1.0
 * @param <T>
 *            type of objects in the cluster
 */
public class Cluster<T extends Vector> {

	/**
	 * Cluster center can be null.
	 */
	private T center;
	private List<T> elements;

	public Cluster() {

		center = null;
		elements = new ArrayList<T>();

	}

	public T getCenter() {
		return center;
	}

	public void setCenter(T center) {
		this.center = center;
	}

	public List<T> getElements() {
		return elements;
	}

	public void setElements(List<T> elements) {
		this.elements = elements;
	}

	public int hashCode() {
		return elements.hashCode();
	}

	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("Center: ").append(center).append("\n");
		sb.append("Elements: ").append(elements.size()).append("\n");
		for (T t : elements) {
			sb.append("   ").append(t).append("\n");
		}

		return super.toString();
	}

}
