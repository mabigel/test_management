package org.uszeged.inf.optimization.data;

/**
 * Simple base interface for function classes.
 * 
 * @author Balázs L. Lévai
 * @version 1.0
 * @since 1.0
 */
public interface Function {

	/**
	* Check if a value is between of lower and upper bound.
	*/
	public boolean isParameterAcceptable(Vector lb, Vector ub);
	
	/**
	* Check if dimension is acceptable.
	*/	
	public boolean isDimensionAcceptable(int dim);
	public double evaluate(Vector x);

}
