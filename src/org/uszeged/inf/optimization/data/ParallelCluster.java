package org.uszeged.inf.optimization.data;

import java.util.Collections;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.uszeged.inf.optimization.util.RWLList;
import org.uszeged.inf.optimization.util.SLinkedList;

/**
 * Basic cluster class to be able to handle cluster elements and their centers
 * together delegating all expected functionality to the java List interface.
 *
 * @author Dániel Zombori
 * @version 1.0
 * @since 1.0
 * @param <T>
 *            type of objects in the cluster
 */
public class ParallelCluster<T extends Vector> {

	/**
	 * Cluster center can be null.
	 */
	private T center;
	private List<T> elements;

	public ParallelCluster() {

		center = null;
		//elements = Collections.synchronizedList(new ArrayList<T>());
		//elements = Collections.synchronizedList(new LinkedList<T>());
		//elements = new RWLList(new ArrayList<T>());
		elements = new SLinkedList<T>();

	}

	public T getCenter() {
		return center;
	}

	public void setCenter(T center) {
		this.center = center;
	}

	public List<T> getElements() {
		return elements;
	}

	public boolean add(T e) {
		inc();
		return elements.add(e);
	}

	public int s = 0;
	public synchronized void inc(){s++;}
	public int size() {
		return s;
		//return elements.size();
	}

	public int hashCode() {
		return elements.hashCode();
	}

	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("Center: ").append(center).append("\n");
		sb.append("Elements: ").append(elements.size()).append("\n");
		for (T t : elements) {
			sb.append("   ").append(t).append("\n");
		}

		return sb.toString();
	}

}
