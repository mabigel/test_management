package org.uszeged.inf.optimization.data;

import java.util.LinkedList;
import java.util.List;
import java.util.Collections;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.util.Arrays;
import java.util.Random;
import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;

import org.uszeged.inf.optimization.util.ErrorMessages;

/**
 * The Vector class implements common n dimensional vectors along some basic
 * utility functionality.
 *
 * @author Balázs L. Lévai
 * @version 1.0
 * @since 1.0
 */
public class Vector implements Cloneable{

	/**
	 * Variable to generate random double values from the range of [0,1].
	 */
	private static Random rand;
	private static long currentSeed;

	private int dimension;
	private double[] coordinates;

	static {
		currentSeed = System.currentTimeMillis();
		rand = new Random(currentSeed);

		//rand = new Random(12345);

	}

	public Vector clone(){
		Vector obj = null;
		try{
			obj = (Vector)super.clone();
			obj.coordinates = new double[dimension];
			System.arraycopy(obj.coordinates,0,this.coordinates,0,dimension);
		}catch(CloneNotSupportedException e){
			e.printStackTrace();
		}
		return obj;
	}

	public static void setRandSeed(long i) {
		currentSeed = i;
		rand = new Random(currentSeed);
	}

	public static long getRandSeed() {
		return currentSeed;
	}

	/**
	 * Generates a random vector of given dimension.
	 *
	 * @param dimension
	 *            dimension of the generated random vector
	 * @return a random vector of the specified dimension
	 */
	public static Vector randomVector(int dimension) {

		Vector v = new Vector(dimension);

		for (int i = 0; i < dimension; i++) {
			v.coordinates[i] = rand.nextDouble();
		}
		return v;

	}

	public static void randomize(Vector v) {

		for (int i = 0; i < v.getDimension(); i++) {
			v.coordinates[i] = rand.nextDouble();
		}
	}


	// randomize with gaussian normal distribution, std = 1, mean = 0
	public static void gaussianRandomize(Vector v) {

		for (int i = 0; i < v.getDimension(); i++) {
			v.coordinates[i] = rand.nextGaussian();
		}
	}

	public static double scalarMul(Vector v1,Vector v2){
		if (v1.dimension != v2.dimension){
			throw new IllegalArgumentException("Dimension mismatch!");
		}
		double[] c1 = v1.getCoordinates();
		double[] c2 = v2.getCoordinates();
		double res = 0;
		for (int i = 0;i < v1.dimension;i++){
			res += c1[i]*c2[i];
		}
		return res;
	}

	/**
	 * Calculates the 2-norm of vectors ( or length of vectors in other words).
	 *
	 * @param v
	 *            an argument vector
	 * @return the 2-norm of v
	 */
	public static double norm(Vector v) {

		double sum2 = 0.0d;

		for (double d : v.getCoordinates()) {
			sum2 += d * d;
		}

		return Math.sqrt(sum2);

	}

	/**
	 * Scales argument vectors to have unit length.
	 *
	 * @param v
	 *            an argument vector
	 */
	public static void normalize(Vector v) {

		double sum2 = 0.0d;

		for (double d : v.coordinates) {
			sum2 += d * d;
		}
		sum2 = Math.sqrt(sum2);
		if (sum2 > 0.0d) {
			for (int i = 0; i < v.coordinates.length; i++) {
				v.coordinates[i] /= sum2;
			}
		}
	}

	private Vector(){}

	public Vector(Vector v) {
		this.coordinates = new double[v.dimension];
		this.dimension = v.dimension;
		System.arraycopy(v.coordinates, 0, this.coordinates, 0, dimension);
	}

	public Vector(Matrix m) {
		if (m.getRows() != 1) throw new IllegalArgumentException("Dimension mismatch!");

		this.dimension = m.getCols();
		this.coordinates = new double[this.dimension];
		System.arraycopy(m.getCoordinates()[0], 0, this.coordinates, 0, this.dimension);
	}

	public Vector(int dimensions) {
		this.coordinates = new double[dimensions];
		this.dimension = dimensions;
	}

	public Vector(double[] coordinates) {

		this.dimension = coordinates.length;
		this.coordinates = new double[dimension];
		System.arraycopy(coordinates, 0, this.coordinates, 0, dimension);
	}

	public double[] getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(double[] coordinates) {

		if (coordinates.length != this.coordinates.length) {
			this.dimension = coordinates.length;
			this.coordinates = new double[dimension];
		}
		if (coordinates != this.coordinates) {
			System.arraycopy(coordinates, 0, this.coordinates, 0, dimension);
		}
	}

	public double getCoordinate(int index) {

		--index;
		if (0 <= (index) && (index) < coordinates.length) {
			return coordinates[index];
		} else {
			throw new IndexOutOfBoundsException(
					ErrorMessages.VECTOR_INDEX(index, dimension));
		}

	}

	public void setCoordinate(int index, double value) {

		--index;
		if (0 <= (index) && (index) < coordinates.length) {
			this.coordinates[index] = value;
		} else {
			throw new IndexOutOfBoundsException(
					ErrorMessages.VECTOR_INDEX(index, dimension));
		}
	}

	public Matrix toMatrix(){
		Matrix m = new Matrix(1,dimension);
		System.arraycopy(coordinates, 0, m.getCoordinates()[0], 0, dimension);
		return m;
	}

	public Matrix toMatrix(boolean standing){
		if (!standing) return toMatrix();

		Matrix m = new Matrix(dimension,1);
		double[][] data = m.getCoordinates();
		for (int i = 0;i<data.length;i++){
			data[i][0] = coordinates[i];
		}
		return m;
	}

	public int getDimension() {
		return dimension;
	}

	/**
	 * Executes element wise unary operations on vector coordinates.
	 *
	 * @param mapper
	 *            a unary operation
	 * @return the modified operand
	 */
	public Vector map(DoubleUnaryOperator mapper) {

		for (int i = 0; i < coordinates.length; i++) {
			coordinates[i] = mapper.applyAsDouble(coordinates[i]);
		}
		return this;
	}

	/**
	 * Executes element wise binary operations of two vectors. The object is
	 * always the left operand.
	 *
	 * @param mapper
	 *            a binary operation
	 * @param rightOperand
	 *            a vector
	 * @return the modified left operand
	 */
	public Vector map(DoubleBinaryOperator mapper, Vector rightOperand) {

		if (this.dimension == rightOperand.dimension) {
			for (int i = 0; i < coordinates.length; i++) {
				this.coordinates[i] = mapper.applyAsDouble(this.coordinates[i],
						rightOperand.coordinates[i]);
			}
		} else {
			throw new UnsupportedOperationException(
					ErrorMessages.VECTOR_DIMENSION(dimension,
							rightOperand.dimension));
		}
		return this;
	}

	/**
	 * Executes element wise binary operations of a vector and a constant. The
	 * object is always the left operand.
	 *
	 * @param mapper
	 *            a binary operation
	 * @param rightOperand
	 *            a vector
	 * @return the modified left operand
	 */
	public Vector map(DoubleBinaryOperator mapper, double rightOperand) {

		for (int i = 0; i < coordinates.length; i++) {
			this.coordinates[i] = mapper.applyAsDouble(this.coordinates[i],
					rightOperand);
		}
		return this;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder(100);

		sb.append('[');
		if (coordinates.length > 0){
			sb.append(coordinates[0]);
		}
		for(int i = 1; i < coordinates.length; i++){
			sb.append(", ");
			sb.append(coordinates[i]);
		}
		sb.append(']');

		return sb.toString();
	}

	public int hashCode() {
		return Arrays.hashCode(this.coordinates);
	}

}
