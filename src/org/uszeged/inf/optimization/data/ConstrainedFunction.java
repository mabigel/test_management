package org.uszeged.inf.optimization.data;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.util.Logger;

import java.lang.Math;

public class ConstrainedFunction implements Function {

	private Function function;
	private Vector xOffset;
	private Vector xScale;

	public static int longRun = 0;

	public ConstrainedFunction(Function function, Vector xOffset,
			Vector xScale) {
		super();
		this.function = function;
		this.xOffset = xOffset;
		this.xScale = xScale;
	}

	public boolean isParameterAcceptable(Vector lb, Vector ub){
		return this.function.isParameterAcceptable(lb,ub);
	}

	public boolean isDimensionAcceptable(int dim) {
		return this.function.isDimensionAcceptable(dim);
	}

	public Vector toOriginalSpace(Vector v){
		Vector temp = new Vector(v.getCoordinates());
		temp.map((left, right) -> left * right, this.xScale).map(
				(left, right) -> left + right, this.xOffset);
		return temp;
	}

	public double evaluate(Vector x) {
		Vector temp = toOriginalSpace(x);
		double res=0.0;
		for (long i=0;i<Math.pow(10,longRun);i++)
			res = this.function.evaluate(temp);

		Logger.debug(this,"evaluate(Vector) Function value is {0} : {1}",
			String.valueOf(res),
			temp.toString());

		return res;
	}

}
