package org.uszeged.inf.optimization.data;

import org.uszeged.inf.optimization.util.ErrorMessages;

/**
 * The Matrix class implements an n*m dimensional matrix along some basic
 * utility functionality.
 *
 * Index is 1 based.
 *
 * @author Zombori Dániel
 * @version 1.0
 * @since 1.0
 */
public class Matrix implements Cloneable{

    public static Matrix eye(int n){
        Matrix m = new Matrix(n,n);
        for (int i = 0;i < n;i++){
            m.data[i][i] = 1;
        }
        return m;
    }

    private int rows,cols;
    private double[][] data;

    public Matrix(int r,int c){
        rows = r;
        cols = c;

        data = new double[r][c];
    }

    public Matrix(Matrix m){
        rows = m.rows;
        cols = m.cols;

        data = new double[rows][cols];
        for (int i = 0;i < rows;i++) {
            System.arraycopy(m.data[i],0,data[i],0,cols);
        }
    }

    public int getRows(){return rows;}
    public int getCols(){return cols;}

	public double getCoordinate(int row, int col) {
        return data[row-1][col-1];
    }

	public void setCoordinate(int row,int col, double value) {
        data[row-1][col-1] = value;
	}

	public void setCoordinates(double[][] values) {
        data = values;
        rows = data.length;
        cols = rows == 0 ? 0 : data[0].length;
	}

	public double[][] getCoordinates() {
		return data;
	}

    public Matrix leftMul(Matrix left){
        if (rows != left.cols){
            throw new IllegalArgumentException("Dimension mismatch, "+left.cols+" != "+rows);
        }
        double[][] tmp = new double[left.rows][cols];

        for (int i = 0;i < left.rows;i++){
            for (int j = 0;j < cols;j++){
                for (int k = 0;k < rows;k++){
                    tmp[i][j] += left.data[i][k]*data[k][j];
                }
            }
        }
        data = tmp;
        rows = left.rows;
        return this;
    }

    public Matrix rightMul(Matrix right){
        if (cols != right.rows){
            throw new IllegalArgumentException("Dimension mismatch, "+cols+" != "+right.rows);
        }
        double[][] tmp = new double[rows][right.cols];

        for (int i = 0;i < rows;i++){
            for (int j = 0;j < right.cols;j++){
                for (int k = 0;k < cols;k++){
                    tmp[i][j] += data[i][k] * right.data[k][j];
                }
            }
        }
        data = tmp;
        rows = rows;
        cols = right.cols;
        return this;
    }

    public Matrix mul(double scalar){
        for (int i = 0;i < rows;i++){
            for (int j = 0;j < cols;j++){
                data[i][j] *= scalar;
            }
        }
        return this;
    }

    public Matrix transpose(){
        double[][] tmp = new double[cols][rows];
        for (int i = 0;i < rows;i++){
            for (int j = 0;j < cols;j++){
                tmp[j][i] = data[i][j];
            }
        }
        int t = rows;
        rows = cols;
        cols = t;
        data = tmp;
        return this;
    }

    public Matrix add(Matrix right){
        if (rows != right.rows || cols != right.cols){
            throw new IllegalArgumentException("Dimension mismatch");
        }
        for (int i = 0;i < rows;i++){
            for (int j = 0;j < cols;j++){
                data[i][j] += right.data[i][j];
            }
        }
        return this;
    }

    public Vector getRow(int i){
        return new Vector(data[i-1]);
    }

    public void setRow(int i, Vector r){
        if (cols != r.getDimension()){
            throw new IllegalArgumentException("Dimension mismatch.");
        }
        System.arraycopy(r.getCoordinates(),0,data[i-1],0,cols);
    }

    public Vector getCol(int j){
        double[] c = new double[rows];
        for(int i = 0; i < rows; i++){
            c[i] = data[i][j-1];
        }
        return new Vector(c);
    }

    public void setCol(int j, Vector c){
        if (rows != c.getDimension()){
            throw new IllegalArgumentException("Dimension mismatch.");
        }
        double[] cc = c.getCoordinates();
        for(int i = 0; i < rows; i++){
            data[i][j-1] = cc[i];
        }
    }

	public Vector toVector(){
		return new Vector(this);
	}

	public Vector toVector(boolean standing){
		if (!standing) return toVector();

		Vector m = new Vector(rows);
		double[] coordinates = m.getCoordinates();
		for (int i = 0;i<rows;i++){
			coordinates[i] = data[i][0];
		}
		return m;
	}

    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append("[[");
        sb.append(data[0][0]);
        for(int j = 1;j < cols; j++){
            sb.append(',');
            sb.append(data[0][j]);
        }
        sb.append(']');
        for(int i = 1;i < rows; i++){
            sb.append(";\n[");
            sb.append(data[i][0]);
            for(int j = 1;j < cols; j++){
                sb.append(',');
                sb.append(data[i][j]);
            }
            sb.append(']');
        }
        sb.append(']');

        return sb.toString();
    }
}
