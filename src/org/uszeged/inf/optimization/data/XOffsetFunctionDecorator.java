package org.uszeged.inf.optimization.data;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Function decorator following the decorator design patter to implement
 * argument translation.
 * 
 * @author Balázs L. Lévai
 * @version 1.0
 * @since 1.0
 */
public class XOffsetFunctionDecorator implements Function {

	private Function function;
	private Vector xOffset;
	
	/**
	 * Auxiliary variable to prevent unintended modification of function
	 * arguments.
	 */
	private Vector temp;

	public XOffsetFunctionDecorator(Function function, Vector xOffset) {

		super();
		this.function = function;
		this.xOffset = xOffset;
		this.temp = new Vector(xOffset.getDimension());

	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub){
	
		return this.function.isParameterAcceptable(lb,ub);
	}
	
	public boolean isDimensionAcceptable(int dim){
	
		return this.function.isDimensionAcceptable(dim);
	}

	public double evaluate(Vector x) {

		this.temp.setCoordinates(x.getCoordinates());
		this.temp.map((left, right) -> left + right, this.xOffset);

		return this.function.evaluate(this.temp);

	}

}
