package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Booth in 2 dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Booth implements Function {

	public double evaluate (Vector x) {
	
		double x1 = x.getCoordinate(1), x2 = x.getCoordinate(2);
		
		return Math.pow((x1 + 2 * x2 - 7), 2) + Math.pow((2 * x1 + x2 - 5), 2);
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return isDimensionAcceptable(lb.getDimension());
	}
	
	public boolean isDimensionAcceptable(int dim) {
			
			return dim == 2;
	}

}