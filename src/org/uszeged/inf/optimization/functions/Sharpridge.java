package org.uszeged.inf.optimization.functions;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Sharpridge in n dimensions.
 *
 * @author Dániel Zombori
 * @version 1.0
 * @since 1.0
 */
public class Sharpridge implements Function {

	public double evaluate (Vector x) {
        double sum = 0;

        for(int i = 2; i <= x.getDimension(); i++){
            sum += Math.pow(x.getCoordinate(i), 2);
        }

        return Math.pow(x.getCoordinate(1), 2) + 100 * Math.sqrt(sum);
	}

	public boolean isParameterAcceptable(Vector lb, Vector ub) {

		return true;
	}

	public boolean isDimensionAcceptable(int dim) {

		return true;
	}

}
