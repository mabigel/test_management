package org.uszeged.inf.optimization.functions;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function sum(x_i^2) in n dimension.
 *
 * @author Balázs L. Lévai
 * @version 1.0
 * @since 1.0
 */
public class TestEvaluationFunc implements Function {

	public static int numOfEvaluations = 0;
	public static Function supportedFunc = new Rosenbrock();

	public double evaluate(Vector x) {

		numOfEvaluations++;

		//*
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		sb.append(x.getCoordinate(1));
		boolean first = true;
		for(double c : x.getCoordinates()){
			if (first){
				first = false;
				continue;
			}
			sb.append(',');
			sb.append(c);
		}
		sb.append("];");
		System.out.println(sb);/**/


		if (supportedFunc == null){
			double sum = 0.0d, d;

			for (int i = 1; i <= x.getDimension(); i++) {
				d = x.getCoordinate(i);
				sum += d * d;
			}

			return x.getCoordinate(1)*Math.exp(-(sum))+(sum)/20;
		} else {
			return supportedFunc.evaluate(x);
		}
	}


	public boolean isParameterAcceptable(Vector lb, Vector ub) {

		return true;
	}

	public boolean isDimensionAcceptable(int dim) {

		return true;
	}

}
