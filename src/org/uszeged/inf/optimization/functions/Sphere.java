package org.uszeged.inf.optimization.functions;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Sphere in n dimensions.
 *
 * @author Dániel Zombori
 * @version 1.0
 * @since 1.0
 */
public class Sphere implements Function {

	public double evaluate (Vector x) {
        double val = 0;

        for(int i = 1; i <= x.getDimension(); i++){
            val += Math.pow(x.getCoordinate(i), 2);
        }

        return val;
	}

	public boolean isParameterAcceptable(Vector lb, Vector ub) {

		return true;
	}

	public boolean isDimensionAcceptable(int dim) {

		return true;
	}

}
