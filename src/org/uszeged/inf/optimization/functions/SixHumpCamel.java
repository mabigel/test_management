package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function six-hump camel in 2 dimension.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class SixHumpCamel implements Function {

	public double evaluate (Vector x) {

		double x1 = x.getCoordinate(1), x2 = x.getCoordinate(2);

		return (4 - 2.1 * Math.pow(x1, 2) + Math.pow(x1, 4) / 3) * Math.pow(x1, 2) + x1 * x2 + (-4 + 4 * Math.pow(x2, 2)) * Math.pow(x2, 2);
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return isDimensionAcceptable(lb.getDimension());
	}
	
	public boolean isDimensionAcceptable(int dim) {
			
			return dim == 2;
	}

}