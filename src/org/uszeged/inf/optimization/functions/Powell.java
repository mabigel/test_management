package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Powell in n dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Powell implements Function {

	public double evaluate (Vector x) {
	
		double sum1 = 0.0, sum2 = 0.0, sum3 = 0.0, sum4 = 0.0;
		
		for (int i = 1; i <= x.getDimension() / 4; i++) {
			sum1 += Math.pow((x.getCoordinate(4*i - 3) + 10 * x.getCoordinate(4*i - 2)), 2);
			sum2 += 5 * Math.pow((x.getCoordinate(4*i - 1) - x.getCoordinate(4*i)), 2);
			sum3 += Math.pow((x.getCoordinate(4*i - 2) - 2 * x.getCoordinate(4*i - 1)), 4);
			sum4 += 10 * Math.pow((x.getCoordinate(4*i - 3) - x.getCoordinate(4*i)), 4);
		}
		
		return sum1 + sum2 + sum3 + sum4;
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return true;
	}
	
	public boolean isDimensionAcceptable(int dim) {
		
		return true;
	}

}