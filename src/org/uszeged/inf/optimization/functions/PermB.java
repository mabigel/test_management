package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Perm in n dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class PermB implements Function {

	public double evaluate (Vector x) {
		
		double sum1 = 0.0, sum2 = 0.0;
		
		for (int i = 1; i <= x.getDimension(); i++) {
			sum1 = 0;
			for (int j = 1; j <=  x.getDimension(); j++) {
				sum1 += (j + 10) * (Math.pow(x.getCoordinate(j), i) - (1 / Math.pow(j, i)));    
			}
			sum2 += Math.pow(sum1, 2);
		}
		
		return sum2;
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return true;
	}
	
	public boolean isDimensionAcceptable(int dim) {
		
		return true;
	}

}