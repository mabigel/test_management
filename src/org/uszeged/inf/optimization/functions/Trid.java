package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Trid in n dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Trid implements Function {

	public double evaluate (Vector x) {
	
		double sum1 = 0.0, sum2 = 0.0;
		
		for (int i = 1; i <= x.getDimension(); i++) {
			sum1 += Math.pow((x.getCoordinate(i) - 1), 2);
		}
		
		for (int i = 2; i <= x.getDimension(); i++) {
			sum2 += x.getCoordinate(i) * x.getCoordinate(i-1);
		}
		
		return sum1 - sum2;
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return true;
	}
	
	public boolean isDimensionAcceptable(int dim) {
		
		return true;
	}

}