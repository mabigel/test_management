package org.uszeged.inf.optimization.functions;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Test function implementing the function x*exp(-(x^2+y^2)) in 2 dimensions.
 * Minimum place: x1=-1/sqrt(2),x2=0 Minimum value: -1/sqrt(2*e)
 *
 * @author Dániel Zombori
 * @version 1.0
 * @since 1.0
 */
public class XexpMX2Y2 implements Function {
	public static final double normScale =
			0.42888194248035339824009482063938623906039930387628d;

	public XexpMX2Y2(){}

	public double evaluate(Vector x) {
		double sum = 0.0, d;

		sum = 0.0d;
		for (int i = 1,limit = x.getDimension(); i <= limit; i++) {
			d = x.getCoordinate(i);
			sum += d * d;
		}

		sum = x.getCoordinate(1)*Math.exp(-sum)/normScale;

		return sum;
	}

	public boolean isParameterAcceptable(Vector lb, Vector ub) {

		return isDimensionAcceptable(lb.getDimension());
	}

	public boolean isDimensionAcceptable(int dim) {

			return dim == 2;
	}

}
