package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function DIxon-Price in n dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class DixonPrice implements Function {

	public double evaluate (Vector x) {
		
		double sum = 0.0;
		
		for (int i = 2; i <=  x.getDimension(); i++) {
			sum += i * Math.pow((2 * Math.pow(x.getCoordinate(i), 2) - x.getCoordinate(i-1)), 2);    
		}
		
		return Math.pow((x.getCoordinate(1) - 1), 2) + sum;
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return true;
	}
	
	public boolean isDimensionAcceptable(int dim) {
		
		return true;
	}

}