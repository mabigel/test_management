package org.uszeged.inf.optimization.functions;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Ellipsoid in n dimensions.
 *
 * @author Dániel Zombori
 * @version 1.0
 * @since 1.0
 */
public class Ellipsoid implements Function {

	public double evaluate (Vector x) {
        double val = 0;
		
		int n = x.getDimension();
        for(int i = 1; i <= x.getDimension(); i++){
            val += Math.pow(10, 4*(i-1)/(n-1)) * Math.pow(x.getCoordinate(i), 2);
        }

        return val;
	}

	public boolean isParameterAcceptable(Vector lb, Vector ub) {

		return true;
	}

	public boolean isDimensionAcceptable(int dim) {

		return true;
	}

}
