package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Shekel in 4 dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Shekel5 implements Function {

	public double evaluate(Vector x) {
	
		double C[][] = {{4.0, 1.0, 8.0, 6.0, 3.0, 2.0, 5.0, 8.0, 6.0, 7.0},
						{4.0, 1.0, 8.0, 6.0, 7.0, 9.0, 5.0, 1.0, 2.0, 3.6},
						{4.0, 1.0, 8.0, 6.0, 3.0, 2.0, 3.0, 8.0, 6.0, 7.0},
						{4.0, 1.0, 8.0, 6.0, 7.0, 9.0, 3.0, 1.0, 2.0, 3.6}};
						
		double beta[] = {0.1, 0.2, 0.2, 0.4, 0.4, 0.6, 0.3, 0.7, 0.5, 0.5};
							
		double sumj, sum = 0.0;
	
		for(int i = 1; i <= 5; i++) {
			sumj = 0.0;
			for(int j = 1; j <= 4; j++) {
				sumj += Math.pow((x.getCoordinate(j) - C[j-1][i-1]), 2) ;
			}
			sum += 1 / (sumj + beta[i-1]);
		}

		return -sum;
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return isDimensionAcceptable(lb.getDimension());
	}
	
	public boolean isDimensionAcceptable(int dim) {
			
			return dim == 4;
	}

}