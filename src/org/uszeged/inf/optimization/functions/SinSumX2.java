package org.uszeged.inf.optimization.functions;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function sin(sum(x_i^2)) in n dimension.
 * 
 * @author Balázs L. Lévai
 * @version 1.0
 * @since 1.0
 */
public class SinSumX2 implements Function {
	
	public double evaluate(Vector x) {

		double sum = 0.0d, d;

		for (int i = 1; i <= x.getDimension(); i++) {
			d = x.getCoordinate(i);
			sum += d * d;
		}

		return Math.sin(sum);
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return true;
	}
	
	public boolean isDimensionAcceptable(int dim) {
		
		return true;
	}

}
