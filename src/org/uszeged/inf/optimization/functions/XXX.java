package org.uszeged.inf.optimization.functions;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function sum(x_i^2) in n dimension.
 *
 * @author Balázs L. Lévai
 * @version 1.0
 * @since 1.0
 */
public class XXX extends Rosenbrock {

	public static int funceval= 0;

	public double evaluate(Vector x) {
		++funceval;

		return super.evaluate(x);

	}

}
