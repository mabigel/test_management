package org.uszeged.inf.optimization.algorithm.optimizer.global;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.uszeged.inf.optimization.algorithm.clustering.Clusterizer;
import org.uszeged.inf.optimization.algorithm.clustering.GlobalSingleLinkageClusterizer;
import org.uszeged.inf.optimization.algorithm.optimizer.local.LocalOptimizer;
import org.uszeged.inf.optimization.algorithm.optimizer.local.NUnirandi;
import org.uszeged.inf.optimization.algorithm.optimizer.local.Unirandi;

class GlobalOptimizerTester {

	@Test
	void testGlobalBuilder() {
		Global.Builder globalBuilder = new Global.Builder();
		assertThrows(IllegalArgumentException.class, () -> {
			globalBuilder.build();
	    });

		
		GlobalSingleLinkageClusterizer clusterizer = null;
		assertThrows(IllegalArgumentException.class, () -> {
			globalBuilder.setClusterizer(clusterizer);
	    });
		assertThrows(IllegalArgumentException.class, () -> {
			globalBuilder.build();
	    });

		GlobalSingleLinkageClusterizer clusterizerset = new GlobalSingleLinkageClusterizer(0.2);
		globalBuilder.setClusterizer(clusterizerset);
		assertThrows(IllegalArgumentException.class, () -> {
			globalBuilder.build();
	    });
		
		Unirandi unirandiNull = null;
		assertThrows(IllegalArgumentException.class, () -> {
			globalBuilder.setLocalOptimizer(unirandiNull);
	    });
		
		
		Unirandi.Builder unirandiBuilder = new Unirandi.Builder();
		Unirandi unirandi = unirandiBuilder.build();
		globalBuilder.setLocalOptimizer(unirandi);
		assertThrows(IllegalArgumentException.class, () -> {
			globalBuilder.build();
	    });
		
		NUnirandi.Builder nunirandiBuilder = new NUnirandi.Builder();
		NUnirandi nunirandi = nunirandiBuilder.build();
		globalBuilder.setLocalOptimizer(nunirandi);
		assertThrows(IllegalArgumentException.class, () -> {
			globalBuilder.build();
	    });
		
		Global global = globalBuilder.build();
		global.restart();
	}

	
	@Test
	void testGlobalBuilderWithNUnirandi() {
		Global.Builder globalBuilder = new Global.Builder();
		assertThrows(IllegalArgumentException.class, () -> {
			globalBuilder.build();
	    });

		
		GlobalSingleLinkageClusterizer clusterizer = null;
		assertThrows(IllegalArgumentException.class, () -> {
			globalBuilder.setClusterizer(clusterizer);
	    });
		assertThrows(IllegalArgumentException.class, () -> {
			globalBuilder.build();
	    });

		GlobalSingleLinkageClusterizer clusterizerset = new GlobalSingleLinkageClusterizer(0.2);
		globalBuilder.setClusterizer(clusterizerset);
		assertThrows(IllegalArgumentException.class, () -> {
			globalBuilder.build();
	    });
		
		Unirandi unirandiNull = null;
		assertThrows(IllegalArgumentException.class, () -> {
			globalBuilder.setLocalOptimizer(unirandiNull);
	    });
		
		NUnirandi.Builder nunirandiBuilder = new NUnirandi.Builder();
		NUnirandi nunirandi = nunirandiBuilder.build();
		globalBuilder.setLocalOptimizer(nunirandi);
		assertThrows(IllegalArgumentException.class, () -> {
			globalBuilder.build();
	    });
	}
}
