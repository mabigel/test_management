package org.uszeged.inf.optimization.algorithm.optimizer.local.parallel;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.uszeged.inf.optimization.algorithm.optimizer.line.*;
import org.uszeged.inf.optimization.algorithm.optimizer.line.parallel.Fminbnd;
import org.uszeged.inf.optimization.algorithm.optimizer.line.parallel.Fminbnd5;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.algorithm.gradient.*;
import org.uszeged.inf.optimization.functions.*;

class LocalParalellTests {

	@Test
	void testBFGS() {
		Vector lbnd = new Vector(2);
		lbnd.setCoordinate(1, -1);
		lbnd.setCoordinate(2, -1);
		Vector ubnd = new Vector(2);
		ubnd.setCoordinate(1, 10);
		ubnd.setCoordinate(2, 10);
		Vector sp = new Vector(2);
		sp.setCoordinate(1, 0.5);
		sp.setCoordinate(2, 0.5);
		
		Fminbnd.Builder bldr_line = new Fminbnd.Builder();
		Fminbnd linesrch = bldr_line.build();
		TangentialGradient.Builder bldr_grad = new TangentialGradient.Builder();
		TangentialGradient grad = bldr_grad.build();
		
		
		BFGS.Builder bldr = new BFGS.Builder();
		bldr.setLineSearchFunction(linesrch);
		bldr.setGradientFunction(grad);
		
		
		BFGS opt = bldr.build();
		//opt.reset();
		opt.setLowerBoundOfSearchSpace(lbnd);
		opt.setUpperBoundOfSearchSpace(ubnd);
		opt.setObjectiveFunction(new Branin());
		opt.setStartingPoint(sp, 1.0);
		opt.restart();
		opt.getSerializableInstance();
		
		Assertions.assertTrue(opt.getLocalOptimumValue() <= 0.5);
		opt.getLocalOptimum();
		opt.getNumberOfFunctionEvaluations();
		opt.getOptimumValue();
		opt.getOptimum();
	}
	
	@Test
	void testBFGSWithFminBnd5() {
		Vector lbnd = new Vector(2);
		lbnd.setCoordinate(1, -1);
		lbnd.setCoordinate(2, -1);
		Vector ubnd = new Vector(2);
		ubnd.setCoordinate(1, 10);
		ubnd.setCoordinate(2, 10);
		Vector sp = new Vector(2);
		sp.setCoordinate(1, 0.5);
		sp.setCoordinate(2, 0.5);
		
		Fminbnd5.Builder bldr_line = new Fminbnd5.Builder();
		Fminbnd5 linesrch = bldr_line.build();
		TangentialGradient.Builder bldr_grad = new TangentialGradient.Builder();
		TangentialGradient grad = bldr_grad.build();
		
		
		BFGS.Builder bldr = new BFGS.Builder();
		bldr.setLineSearchFunction(linesrch);
		bldr.setGradientFunction(grad);
		
		
		BFGS opt = bldr.build();
		//opt.reset();
		opt.setLowerBoundOfSearchSpace(lbnd);
		opt.setUpperBoundOfSearchSpace(ubnd);
		opt.setObjectiveFunction(new Branin());
		opt.setStartingPoint(sp, 1.0);
		opt.restart();
		opt.getSerializableInstance();
		
		Assertions.assertTrue(opt.getLocalOptimumValue() <= 0.5);
		opt.getLocalOptimum();
		opt.getNumberOfFunctionEvaluations();
		opt.getOptimumValue();
		opt.getOptimum();
	}
	
	@Test
	void testUnirandiCLS() {
		Vector lbnd = new Vector(2);
		lbnd.setCoordinate(1, -1);
		lbnd.setCoordinate(2, -1);
		Vector ubnd = new Vector(2);
		ubnd.setCoordinate(1, 1);
		ubnd.setCoordinate(2, 1);
		Vector sp = new Vector(2);
		sp.setCoordinate(1, 0.5);
		sp.setCoordinate(2, 0.5);
		
		Fminbnd.Builder bldr_line = new Fminbnd.Builder();
		Fminbnd linesrch = bldr_line.build();
		TangentialGradient.Builder bldr_grad = new TangentialGradient.Builder();
		TangentialGradient grad = bldr_grad.build();
		
		
		UnirandiCLS.Builder bldr = new UnirandiCLS.Builder();
		bldr.setLineSearchFunction(linesrch);
		//bldr.setGradientFunction(grad);
		
		
		UnirandiCLS opt = bldr.build();
		//opt.reset();
		opt.setLowerBoundOfSearchSpace(lbnd);
		opt.setUpperBoundOfSearchSpace(ubnd);
		opt.setObjectiveFunction(new Branin());
		opt.setStartingPoint(sp, 1.0);
		opt.restart();
		opt.getSerializableInstance();
		
		Assertions.assertTrue(opt.getLocalOptimumValue() <= 0.5);
		opt.getLocalOptimum();
		opt.getNumberOfFunctionEvaluations();
		opt.getOptimumValue();
		opt.getOptimum();
	}

	@Test
	void testRosenbrock() {
		Vector lbnd = new Vector(2);
		lbnd.setCoordinate(1, -1);
		lbnd.setCoordinate(2, -1);
		Vector ubnd = new Vector(2);
		ubnd.setCoordinate(1, 1);
		ubnd.setCoordinate(2, 1);
		Vector sp = new Vector(2);
		sp.setCoordinate(1, 0.5);
		sp.setCoordinate(2, 0.5);
		
		Fminbnd.Builder bldr_line = new Fminbnd.Builder();
		Fminbnd linesrch = bldr_line.build();
		TangentialGradient.Builder bldr_grad = new TangentialGradient.Builder();
		TangentialGradient grad = bldr_grad.build();
		
		
		Rosenbrock.Builder bldr = new Rosenbrock.Builder();
		bldr.setLineSearchFunction(linesrch);
		//bldr.setGradientFunction(grad);
		
		
		Rosenbrock opt = bldr.build();
		//opt.reset();
		opt.setLowerBoundOfSearchSpace(lbnd);
		opt.setUpperBoundOfSearchSpace(ubnd);
		opt.setObjectiveFunction(new Branin());
		opt.setStartingPoint(sp, 1.0);
		opt.restart();
		opt.getSerializableInstance();
		
		Assertions.assertTrue(opt.getLocalOptimumValue() <= 0.5);
		opt.getLocalOptimum();
		opt.getNumberOfFunctionEvaluations();
		opt.getOptimumValue();
		opt.getOptimum();
	}
	
	@Test
	void testNUnirandiCLS() {
		Vector lbnd = new Vector(2);
		lbnd.setCoordinate(1, -1);
		lbnd.setCoordinate(2, -1);
		Vector ubnd = new Vector(2);
		ubnd.setCoordinate(1, 1);
		ubnd.setCoordinate(2, 1);
		Vector sp = new Vector(2);
		sp.setCoordinate(1, 0.5);
		sp.setCoordinate(2, 0.5);
		
		Fminbnd.Builder bldr_line = new Fminbnd.Builder();
		Fminbnd linesrch = bldr_line.build();
		TangentialGradient.Builder bldr_grad = new TangentialGradient.Builder();
		TangentialGradient grad = bldr_grad.build();
		
		
		NUnirandiCLS.Builder bldr = new NUnirandiCLS.Builder();
		bldr.setLineSearchFunction(linesrch);
		//bldr.setGradientFunction(grad);
		
		
		NUnirandiCLS opt = bldr.build();
		//opt.reset();
		opt.setLowerBoundOfSearchSpace(lbnd);
		opt.setUpperBoundOfSearchSpace(ubnd);
		opt.setObjectiveFunction(new Branin());
		opt.setStartingPoint(sp, 1.0);
		opt.restart();
		opt.getSerializableInstance();
		
		Assertions.assertTrue(opt.getLocalOptimumValue() <= 0.5);
		opt.getLocalOptimum();
		opt.getNumberOfFunctionEvaluations();
		opt.getOptimumValue();
		opt.getOptimum();
	}
	
	@Test
	void testDBHC() {
		Vector lbnd = new Vector(2);
		lbnd.setCoordinate(1, -1);
		lbnd.setCoordinate(2, -1);
		Vector ubnd = new Vector(2);
		ubnd.setCoordinate(1, 1);
		ubnd.setCoordinate(2, 1);
		Vector sp = new Vector(2);
		sp.setCoordinate(1, 0.5);
		sp.setCoordinate(2, 0.5);
		
		Fminbnd.Builder bldr_line = new Fminbnd.Builder();
		Fminbnd linesrch = bldr_line.build();
		TangentialGradient.Builder bldr_grad = new TangentialGradient.Builder();
		TangentialGradient grad = bldr_grad.build();
		
		
		DBHC.Builder bldr = new DBHC.Builder();
		bldr.setLineSearchFunction(linesrch);
		bldr.setGradientFunction(grad);
		
		
		DBHC opt = bldr.build();
		//opt.reset();
		opt.setLowerBoundOfSearchSpace(lbnd);
		opt.setUpperBoundOfSearchSpace(ubnd);
		opt.setObjectiveFunction(new Branin());
		opt.setStartingPoint(sp, 1.0);
		opt.restart();
		opt.getSerializableInstance();
		
		Assertions.assertTrue(opt.getLocalOptimumValue() <= 0.5);
		opt.getLocalOptimum();
		opt.getNumberOfFunctionEvaluations();
		opt.getOptimumValue();
		opt.getOptimum();
	}
}
