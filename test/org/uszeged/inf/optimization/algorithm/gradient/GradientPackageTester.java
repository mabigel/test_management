package org.uszeged.inf.optimization.algorithm.gradient;


import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.lang.reflect.Field;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.uszeged.inf.optimization.algorithm.gradient.AbstractGradient;
import org.uszeged.inf.optimization.algorithm.gradient.TangentialGradient;
import org.uszeged.inf.optimization.algorithm.gradient.TangentialGradient.Builder;
import org.uszeged.inf.optimization.data.ConstrainedFunction;
import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;


public class GradientPackageTester {
	@Mock
	AbstractGradient<Vector> abstractGradientObject;
	TangentialGradient tangentialGradientObject;
	
	@Test
    public void testTangentialInstatiation() {
		TangentialGradient.Builder gradientBuilder = new TangentialGradient.Builder();
		abstractGradientObject = gradientBuilder.build();
    }

	@Test
	public void testIfIsGradientComputedFalse() {
		TangentialGradient.Builder gradientBuilder = new TangentialGradient.Builder();
		abstractGradientObject = gradientBuilder.build();
		abstractGradientObject.reset();
		Assertions.assertEquals(false, abstractGradientObject.isGradientComputed());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGetNumberOfFunctionEvaluations() {
		TangentialGradient.Builder gradientBuilder = new TangentialGradient.Builder();
		abstractGradientObject = gradientBuilder.build();
		abstractGradientObject.restart();
		Assertions.assertEquals(0, abstractGradientObject.getNumberOfFunctionEvaluations());
	}
	
	@Test
	public void testSetDeltaHScale() {
		TangentialGradient.Builder gradientBuilder = new TangentialGradient.Builder();
		
		gradientBuilder.setDeltaHScale(1.0);
	}
	
	@Test
	public void testSetDeltaHScaleException() throws IllegalArgumentException {
		TangentialGradient.Builder gradientBuilder = new TangentialGradient.Builder();
		
		assertThrows(IllegalArgumentException.class, () -> {
			gradientBuilder.setDeltaHScale(-1.0);
	    });
	}
	
	@Test
	public void testMinDeltaH() {
		TangentialGradient.Builder gradientBuilder = new TangentialGradient.Builder();
		
		gradientBuilder.setMinDeltaH(1.0);
	}
	
	@Test
	public void testMinDeltaHException() throws IllegalArgumentException {
		TangentialGradient.Builder gradientBuilder = new TangentialGradient.Builder();
		
		assertThrows(IllegalArgumentException.class, () -> {
			gradientBuilder.setMinDeltaH(-1.0);
	    });
	}
	
	@Test
    public void testSetGradientConfiguration() {
		TangentialGradient.Builder gradientBuilder = new TangentialGradient.Builder();
		tangentialGradientObject = gradientBuilder.build();

		ConstrainedFunction objectiveFunction = new ConstrainedFunction(null, null, null);
		tangentialGradientObject.setObjectiveFunction(objectiveFunction);

		ConstrainedFunction nullObjectiveFunction = null;
    }
	
	@Test//(expected = NullPointerException.class)
    public void testSetGradientConfigurationNull() throws IllegalArgumentException{
		TangentialGradient.Builder gradientBuilder = new TangentialGradient.Builder();
		tangentialGradientObject = gradientBuilder.build();

		ConstrainedFunction nullObjectiveFunction = null;
		assertThrows(IllegalArgumentException.class, () -> {
			tangentialGradientObject.setObjectiveFunction(nullObjectiveFunction);
	    });
    }
	
}
