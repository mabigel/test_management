package org.uszeged.inf.optimization.algorithm.clustering;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.uszeged.inf.optimization.data.Cluster;
import org.uszeged.inf.optimization.data.Point;

class ClusteringTester {

	@Test
	void testGlobalSingleLinkageClusterizerBuilder() throws IllegalArgumentException {
		GlobalSingleLinkageClusterizer.Builder clusterizerBuilder = new GlobalSingleLinkageClusterizer.Builder();
		
		assertThrows(IllegalArgumentException.class, () -> {
			clusterizerBuilder.setAlpha(Double.NaN);
	    });
		assertThrows(IllegalArgumentException.class, () -> {
			GlobalSingleLinkageClusterizer clusterizer = clusterizerBuilder.build();
	    });
		
		assertThrows(IllegalArgumentException.class, () -> {
			clusterizerBuilder.setAlpha(-1.0);
	    });
		clusterizerBuilder.setAlpha(0.15);
		
		GlobalSingleLinkageClusterizer clusterizer = clusterizerBuilder.build();
	}
	
	@Test
	void testClusterizeFunction() throws IndexOutOfBoundsException {
		GlobalSingleLinkageClusterizer clusterizer = new GlobalSingleLinkageClusterizer(0.1);
		Cluster<Point> cluster = new Cluster<Point>();
		double[] center = {(0.0), (0.0)};
		Point centerPoint = new Point(center);
		cluster.setCenter(centerPoint);
		
		double[] left = {-1.0, -1.0};
		double[] right = {-2.0, -2.0};
		ArrayList<Point> clusterElements = new ArrayList<Point>();
		clusterElements.add(new Point(right));
		clusterElements.add(new Point(left));
		
		cluster.setElements(clusterElements);
		clusterizer.add(cluster);
		
		double[] pointList = {-0.5, -0.5};
		Point input = new Point(pointList);
		assertThrows(IndexOutOfBoundsException.class, () -> {
			clusterizer.clusterize(input);
	    });
	}

}
