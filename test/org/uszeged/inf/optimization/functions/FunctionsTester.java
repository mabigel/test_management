package org.uszeged.inf.optimization.functions;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.uszeged.inf.optimization.data.Vector;

class functions {

	double eps = 0.01;
	
	@Test
	void testAckley() {
		Ackley f = new Ackley();
		Assertions.assertEquals(true, f.isDimensionAcceptable(1) );
		
		Vector v1 = new Vector(1);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt = 0.0;
		Vector v3 = new Vector(1);
		v3.setCoordinate(1, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testBeale() {
		Beale f = new Beale();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt = 0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 3.0);
		v3.setCoordinate(2, 0.5);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testBohachevsky() {
		Bohachevsky f = new Bohachevsky();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt = 0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testBooth() {
		Booth f = new Booth();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt = 0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 1.0);
		v3.setCoordinate(2, 3.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testBranin() {
		Branin f = new Branin();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.39;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 9.42478);
		v3.setCoordinate(2, 2.475);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testCam() {
		Cam f = new Cam();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt = 0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testCigar() {
		Cigar f = new Cigar();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt = 0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testCigarRot() {
		CigarRot f = new CigarRot();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt = 0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		//reason of failing this case : FunctionTransform object not created properly
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testColville() {
		Colville f = new Colville();
		Assertions.assertEquals(true, f.isDimensionAcceptable(4) );
		
		Vector v1 = new Vector(4);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt = 0.0;
		Vector v3 = new Vector(4);
		v3.setCoordinate(1, 1.0);
		v3.setCoordinate(2, 1.0);
		v3.setCoordinate(3, 1.0);
		v3.setCoordinate(4, 1.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testDeJong() { //condition in if statement renders following block inaccessible 
		DeJong f = new DeJong();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		Vector v3 = new Vector(2);
		Assertions.assertEquals(0.0, f.evaluate(v3) );
	}
	
	@Test
	void testDiffPow() {
		DiffPow f = new DiffPow();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testDiscus() {
		Discus f = new Discus();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testDiscusRot() {
		DiscusRot f = new DiscusRot();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		Vector v3 = new Vector(2);
		//reason of failing this case : FunctionTransform object not created properly
		Assertions.assertEquals(0.0, f.evaluate(v3) );
	}
	
	@Test
	void testDixonPrice() {
		DixonPrice f = new DixonPrice();
		Assertions.assertEquals(true, f.isDimensionAcceptable(1) );
		
		Vector v1 = new Vector(1);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(1);
		v3.setCoordinate(1, 1.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testEasom() {
		Easom f = new Easom();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=-1.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 3.14);
		v3.setCoordinate(2, 3.14);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testEllipsoid() {
		Ellipsoid f = new Ellipsoid();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testEllipsoidRot() {
		EllipsoidRot f = new EllipsoidRot();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(1);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		//fails due to missing implementation
		Vector v3 = new Vector(2);
		Assertions.assertEquals(0.0, f.evaluate(v3) );
	}
	
	@Test
	void testGoldsteinPrice() {
		GoldsteinPrice f = new GoldsteinPrice();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=3.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, -1.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testGriewank() {
		Griewank f = new Griewank();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testHartmann3() {
		Hartmann3 f = new Hartmann3();
		Assertions.assertEquals(true, f.isDimensionAcceptable(3) );
		
		Vector v1 = new Vector(3);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=-3.86;
		Vector v3 = new Vector(3);
		v3.setCoordinate(1, 0.115);
		v3.setCoordinate(2, 0.556);
		v3.setCoordinate(3, 0.853);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testHartmann6() {
		Hartmann6 f = new Hartmann6();
		Assertions.assertEquals(true, f.isDimensionAcceptable(6) );
		
		Vector v1 = new Vector(6);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=-3.32;
		Vector v3 = new Vector(6);
		v3.setCoordinate(1, 0.202);
		v3.setCoordinate(2, 0.150);
		v3.setCoordinate(3, 0.477);
		v3.setCoordinate(4, 0.275);
		v3.setCoordinate(5, 0.312);
		v3.setCoordinate(6, 0.657);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testLevy() {
		Levy f = new Levy();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 1.0);
		v3.setCoordinate(2, 1.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testMatyas() {
		Matyas f = new Matyas();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testPerm1per2() {
		Perm1per2 f = new Perm1per2();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 1.0);
		v3.setCoordinate(2, 0.5);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testPerm20() {
		Perm20 f = new Perm20();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 1.0);
		v3.setCoordinate(2, 2.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testPermA() {
		PermA f = new PermA();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 1.0);
		v3.setCoordinate(2, 2.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testPermB() {
		PermB f = new PermB();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 1.0);
		v3.setCoordinate(2, 2.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testPowell() {
		Powell f = new Powell();
		Assertions.assertEquals(true, f.isDimensionAcceptable(4) );
		
		Vector v1 = new Vector(4);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(4);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		v3.setCoordinate(3, 0.0);
		v3.setCoordinate(4, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	
	@Test
	void testPower() {
		Power f = new Power();
		Assertions.assertEquals(true, f.isDimensionAcceptable(4) );
		
		Vector v1 = new Vector(4);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(4);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		v3.setCoordinate(3, 0.0);
		v3.setCoordinate(4, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testRastrigin() {
		Rastrigin f = new Rastrigin();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testRosenbrock() {
		Rosenbrock f = new Rosenbrock();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 1.0);
		v3.setCoordinate(2, 1.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testRosenbrockRot() {
		RosenbrockRot f = new RosenbrockRot();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testSchaffer() {
		Schaffer f = new Schaffer();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testSchwefel() {
		Schwefel f = new Schwefel();
		Assertions.assertEquals(true, f.isDimensionAcceptable(1) );
		
		Vector v1 = new Vector(1);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(1);
		v3.setCoordinate(1, 420.97);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testSharpridge() {
		Sharpridge f = new Sharpridge();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testShekel10() {
		Shekel10 f = new Shekel10();
		Assertions.assertEquals(true, f.isDimensionAcceptable(4) );
		
		Vector v1 = new Vector(4);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=-10.54;
		Vector v3 = new Vector(4);
		v3.setCoordinate(1, 4.0);
		v3.setCoordinate(2, 4.0);
		v3.setCoordinate(3, 4.0);
		v3.setCoordinate(4, 4.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testShekel5() {
		Shekel5 f = new Shekel5();
		Assertions.assertEquals(true, f.isDimensionAcceptable(4) );
		
		Vector v1 = new Vector(4);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=-10.15;
		Vector v3 = new Vector(4);
		v3.setCoordinate(1, 4.0);
		v3.setCoordinate(2, 4.0);
		v3.setCoordinate(3, 4.0);
		v3.setCoordinate(4, 4.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testShekel7() {
		Shekel7 f = new Shekel7();
		Assertions.assertEquals(true, f.isDimensionAcceptable(4) );
		
		Vector v1 = new Vector(4);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=-10.40;
		Vector v3 = new Vector(4);
		v3.setCoordinate(1, 4.0);
		v3.setCoordinate(2, 4.0);
		v3.setCoordinate(3, 4.0);
		v3.setCoordinate(4, 4.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testShubert() {
		Shubert f = new Shubert();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=-186.73;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testSinSumX2() {
		SinSumX2 f = new SinSumX2();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		Assertions.assertEquals(0.0, f.evaluate(v3) );
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testSixHumpCamel() {
		SixHumpCamel f = new SixHumpCamel();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=-1.03;
		Vector v3 = new Vector(2);
		Assertions.assertEquals(0.0, f.evaluate(v3) );
		v3.setCoordinate(1, 0.09);
		v3.setCoordinate(2, -0.71);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testSphere() {
		Sphere f = new Sphere();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testSumSquares() {
		SumSquares f = new SumSquares();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(1);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		Vector v3 = new Vector(1);
		Assertions.assertEquals(0.0, f.evaluate(v3) );
	}
	
	@Test
	void testSumSquaresRot() { //nulptr exception, constructor not implemented properly
		SumSquaresRot f = new SumSquaresRot();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		Vector v3 = new Vector(2);
		Assertions.assertEquals(0.0, f.evaluate(v3) );
	}
	
	@Test
	void testTestEvaluationFunc() {
		TestEvaluationFunc f = new TestEvaluationFunc();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(1);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		Vector v3 = new Vector(1);
		Assertions.assertEquals(0.0, f.evaluate(v3) );
	}
	
	@Test
	void testTrid() {
		Trid f = new Trid();
		Assertions.assertEquals(true, f.isDimensionAcceptable(1) );
		
		Vector v1 = new Vector(1);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(1);
		v3.setCoordinate(1, 1.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testXexpMX2Y2() {
		XexpMX2Y2 f = new XexpMX2Y2();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testXXX() {
		XXX f = new XXX();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}
	
	@Test
	void testZakharov() {
		Zakharov f = new Zakharov();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		double opt=0.0;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 0.0);
		v3.setCoordinate(2, 0.0);
		Assertions.assertEquals(true, opt-eps < f.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > f.evaluate(v3) );
	}

	@Test
	void testZakharovRot() { //throws nullptr exception
		ZakharovRot f = new ZakharovRot();
		Assertions.assertEquals(true, f.isDimensionAcceptable(2) );
		
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(1);
		Assertions.assertEquals(true, f.isParameterAcceptable(v1, v2) );
		
		Vector v3 = new Vector(2);
		Assertions.assertEquals(0.0, f.evaluate(v3) );
	}
}
