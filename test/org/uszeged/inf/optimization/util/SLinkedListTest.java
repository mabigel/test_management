package org.uszeged.inf.optimization.util;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SLinkedListTest {

	@Test
	void test() {
		SLinkedList l = new SLinkedList();

		Assertions.assertTrue(l.add(3.14));
		Assertions.assertTrue(l.add(42));
		
		Assertions.assertEquals(2, l.size());
		Assertions.assertEquals(42, l.get(1));
		Assertions.assertEquals(3.14, l.removeHead());
		Assertions.assertEquals(1, l.size());
		
		List arr = new ArrayList();
		arr.add(1);
		arr.add(2);
		l.addAll(arr);
		Assertions.assertEquals(3, l.size());
		Assertions.assertEquals(42, l.removeHead());
		
		l.removeHead();
		Iterator i = l.iterator();
		Assertions.assertEquals(true, i.hasNext());
		i.next();
		Assertions.assertEquals(false, i.hasNext());
		Assertions.assertEquals(null, i.next());
		
		Object[] tomb = l.toArray();
		Assertions.assertEquals(2, tomb[0]);
		
		l.clear();
		Assertions.assertEquals(null, l.get(3));
		Assertions.assertEquals(null, l.removeHead());
		
	}

}
