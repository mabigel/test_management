package org.uszeged.inf.optimization.util;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class LoggerTest {

	@Test
	void test() {
		Assertions.assertEquals(Logger.Level.INFO, Logger.getMinimumLogLevel());
		Logger.setMinimumLogLevel(Logger.Level.WARN_OBVIOUS);
		Assertions.assertEquals(Logger.Level.WARN_OBVIOUS, Logger.getMinimumLogLevel());
		
		PrintStream ps = null;
		Assertions.assertThrows(NullPointerException.class, () -> Logger.setLogChannel(ps));
		Logger.setLogChannel(Logger.getLogChannel());
		try {
			Logger.setLogChannel("log.txt");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String fn = null;
		Assertions.assertThrows(NullPointerException.class, () -> Logger.setLogChannel(fn));

		Logger.info(int.class, "integer sht", "");
		Logger.debug(int.class, "integer sht", "");
		Logger.trace(int.class, "integer sht", "");
		Logger.error(int.class, "integer sht", "");
		Logger.warnAttention(int.class, "integer sht", "");
		Logger.warnObvious(int.class, "integer sht", "");
		Assertions.assertThrows(Exception.class, () -> Logger.error(new NullPointerException(), "int[] is{ bad} to write!", ""));
		Assertions.assertThrows(Exception.class, () -> Logger.warnAttention(new NullPointerException(), "int[] is{} to write!", ""));
		Assertions.assertThrows(Exception.class, () -> Logger.warnObvious(new NullPointerException(), "int]555[555]555[ is{555} to write!", ""));
		Logger.error(new NullPointerException(), "int[] is to write!", "");
		Logger.warnAttention(new NullPointerException(), "int[] is to write!", "");
		Logger.warnObvious(new NullPointerException(), "int[] is to write!", "");
		Logger.debug(new NullPointerException(), "int[] is to write!", "");
		Logger.trace(new NullPointerException(), "int[] is to write!", "");
		Logger.info(new NullPointerException(), "int[] is to write!", "");
		
	}

}
