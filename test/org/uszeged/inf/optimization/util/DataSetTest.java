package org.uszeged.inf.optimization.util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class DataSetTest {

	@Test
	void test() {
		DataSet ds = new DataSet();
		ds.addObject("abc", 3);
		ds.addObject("abc2", 42);
		Assertions.assertEquals(42, ds.getObject("abc2"));
		Assertions.assertThrows(NullPointerException.class, () -> ds.getObject("abcd"));
		
		DataSet ds2 = new DataSet(ds);
		Assertions.assertEquals(3, ds2.getObject("abc"));

		Assertions.assertTrue(ds2.containsKey("abc2"));
		Assertions.assertFalse(ds2.containsKey("abcde"));
		
		Assertions.assertFalse(ds2.containsPrimitive("abcde"));
		
		ds2.addObject("abcd", 6);
		ds.addAll(ds2);
		Assertions.assertTrue(ds.containsObject("abcd"));
	}

}
