package org.uszeged.inf.optimization.util;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.Vector;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RWLListTest {

	@Test
	void test() {
		List l0 = new Vector();
		RWLList l = new RWLList(l0);
		Assertions.assertTrue(l.add(3));
		Assertions.assertTrue(l.add(42));
		Assertions.assertEquals(42, l.get(1));
		Assertions.assertEquals(2, l.size());
		l.clear();
		Assertions.assertEquals(0, l.size());
	}

}
