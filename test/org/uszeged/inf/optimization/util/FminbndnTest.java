package org.uszeged.inf.optimization.util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FminbndnTest {

	@Test
	void test() {
		double[] x = {0.0, 1.5, 3.0};
		double[] fx = {2, 1, 2};
		
		Assertions.assertEquals(1.5, Fminbndn.fit(x, fx));
	}

}
