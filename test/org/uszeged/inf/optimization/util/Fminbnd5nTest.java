package org.uszeged.inf.optimization.util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class Fminbnd5nTest {

	@Test
	void test() {
		double[] x = {0.0, 2.0, 4.0, 5.0, 8.0};
		double[] fx = {4, 1, 0, 0.25, 4};
		Assertions.assertTrue(4.0000000001 >= Fminbnd5n.fit(x, fx));
		Assertions.assertTrue(3.9999999999 <= Fminbnd5n.fit(x, fx));

		x[4] = 2.0;
		Assertions.assertEquals(Double.NaN, Fminbnd5n.fit(x, fx));
		
		fx[4] = 1;
		x[3] = 2;
		fx[3] = 1;
		Assertions.assertEquals(Double.NaN, Fminbnd5n.fit(x, fx));
		//Assertions.assertTrue(3.9999999999 <= Fminbnd5n.fit(x, fx));
		//Assertions.assertTrue(4.0000000001 >= Fminbnd5n.fit(x, fx));
		
		x[0] = 0;
		x[1] = 1;
		x[2] = 2;
		x[3] = 3;
		x[4] = 4;
		fx[0] = -8;
		fx[1] = -1;
		fx[2] = 0;
		fx[3] = 1;
		fx[4] = 8;
		Assertions.assertTrue(1.9999999 <= Fminbnd5n.fit(x, fx));
		Assertions.assertTrue(2.0000001 >= Fminbnd5n.fit(x, fx));
		

		x[0] = 0;
		x[1] = 1;
		x[2] = 2;
		x[3] = 3;
		x[4] = 4;
		fx[0] = 16;
		fx[1] = 1;
		fx[2] = 0;
		fx[3] = 1;
		fx[4] = 16;
		Assertions.assertEquals(Double.NaN, Fminbnd5n.fit(x, fx));
		

		fx[0] = 1;
		fx[1] = 2;
		fx[2] = 2;
		fx[3] = 2;
		fx[4] = 2;
		Assertions.assertEquals(Double.NaN, Fminbnd5n.fit(x, fx));

		
	}

}
