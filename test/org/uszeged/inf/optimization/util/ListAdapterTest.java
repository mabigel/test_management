package org.uszeged.inf.optimization.util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ListAdapterTest {

	@Test
	void test() {
		ListAdapter la = new ListAdapter();

		Assertions.assertThrows(NotImplementedException.class, () -> la.add(la));
		Assertions.assertThrows(NotImplementedException.class, () -> la.add(2, la));
		Assertions.assertThrows(NotImplementedException.class, () -> la.addAll(la));
		Assertions.assertThrows(NotImplementedException.class, () -> la.addAll(4, la));
		Assertions.assertThrows(NotImplementedException.class, () -> la.clear());
		Assertions.assertThrows(NotImplementedException.class, () -> la.contains(la));
		Assertions.assertThrows(NotImplementedException.class, () -> la.containsAll(la));
		Assertions.assertThrows(NotImplementedException.class, () -> la.equals(la));
		Assertions.assertThrows(NotImplementedException.class, () -> la.get(2));
		Assertions.assertThrows(NotImplementedException.class, () -> la.hashCode());
		Assertions.assertThrows(NotImplementedException.class, () -> la.indexOf(la));
		Assertions.assertThrows(NotImplementedException.class, () -> la.isEmpty());
		Assertions.assertThrows(NotImplementedException.class, () -> la.iterator());
		Assertions.assertThrows(NotImplementedException.class, () -> la.lastIndexOf(la));
		Assertions.assertThrows(NotImplementedException.class, () -> la.listIterator());
		Assertions.assertThrows(NotImplementedException.class, () -> la.listIterator(54));
		Assertions.assertThrows(NotImplementedException.class, () -> la.remove(42));
		Assertions.assertThrows(NotImplementedException.class, () -> la.remove(la));
		Assertions.assertThrows(NotImplementedException.class, () -> la.removeAll(la));
		Assertions.assertThrows(NotImplementedException.class, () -> la.retainAll(la));
		Assertions.assertThrows(NotImplementedException.class, () -> la.set(6, la));
		Assertions.assertThrows(NotImplementedException.class, () -> la.size());
		Assertions.assertThrows(NotImplementedException.class, () -> la.subList(9, 99));
		Assertions.assertThrows(NotImplementedException.class, () -> la.toArray());
	}

}
