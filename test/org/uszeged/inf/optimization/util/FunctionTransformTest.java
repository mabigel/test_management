package org.uszeged.inf.optimization.util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FunctionTransformTest {

	@Test
	void test() {
		FunctionTransform ft = FunctionTransform.singleton();
		FunctionTransform.init(2);
		ft = FunctionTransform.singleton();
	}
	
	@Test
	void test2() {
		FunctionTransform.init(3);
		FunctionTransform.init(2);
		FunctionTransform ft = FunctionTransform.singleton();
		
		Assertions.assertEquals(2.0, ft.inverseRotValue(ft.rotValue(2.0)));
	}

}
