package org.uszeged.inf.optimization.util;

import org.uszeged.inf.optimization.data.Vector;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class VectorOperationsTest {

	@Test
	void testSubtractVectors() {
		Vector v1 = new Vector(3);
		v1.setCoordinate(1, 3.0);
		v1.setCoordinate(2, 4.5);
		v1.setCoordinate(3, 0.0);
		
		Vector v2 = new Vector(3);
		v2.setCoordinate(1, 2.0);
		v2.setCoordinate(2, -2.5);
		v2.setCoordinate(3, 2.2);
		
		Vector v3 = VectorOperations.subtractVectors(v1, v2);
		Assertions.assertEquals(3, v3.getDimension());
		Assertions.assertEquals(1.0, v3.getCoordinate(1));
		Assertions.assertEquals(7.0, v3.getCoordinate(2));
		Assertions.assertEquals(-2.2, v3.getCoordinate(3));
	}

	@Test
	void testSumUpVectors() {
		Vector v1 = new Vector(3);
		v1.setCoordinate(1, 3.0);
		v1.setCoordinate(2, 4.5);
		v1.setCoordinate(3, 0.0);
		
		Vector v2 = new Vector(3);
		v2.setCoordinate(1, 2.0);
		v2.setCoordinate(2, -2.5);
		v2.setCoordinate(3, 2.2);
		
		Vector v3 = VectorOperations.sumUpVectors(v1, v2);
		Assertions.assertEquals(3, v3.getDimension());
		Assertions.assertEquals(5.0, v3.getCoordinate(1));
		Assertions.assertEquals(2.0, v3.getCoordinate(2));
		Assertions.assertEquals(2.2, v3.getCoordinate(3));
	}
	
	@Test
	void testMultiplyVector() {
		Vector v1 = new Vector(3);
		v1.setCoordinate(1, 3.0);
		v1.setCoordinate(2, 4.5);
		v1.setCoordinate(3, -2.2);
		
		double m = 2.5;
		
		Vector v3 = VectorOperations.multiplyVector(v1, m);
		Assertions.assertEquals(3, v3.getDimension());
		Assertions.assertEquals(7.5, v3.getCoordinate(1));
		Assertions.assertEquals(11.25, v3.getCoordinate(2));
		Assertions.assertEquals(-5.5, v3.getCoordinate(3));
	}

	@Test
	void testScalarProduct() {
		Vector v1 = new Vector(3);
		v1.setCoordinate(1, 3.0);
		v1.setCoordinate(2, 4.5);
		v1.setCoordinate(3, 0.0);
		
		Vector v2 = new Vector(3);
		v2.setCoordinate(1, 2.0);
		v2.setCoordinate(2, -2.5);
		v2.setCoordinate(3, 2.2);
		
		double p = VectorOperations.scalarProduct(v1, v2);
		Assertions.assertEquals(-5.25, p);
	}

	@Test
	void testProjection() {
		Vector v1 = new Vector(3);
		v1.setCoordinate(1, 2.0);
		v1.setCoordinate(2, 2.0);
		v1.setCoordinate(3, 0.0);
		
		Vector v2 = new Vector(3);
		v2.setCoordinate(1, 3.0);
		v2.setCoordinate(2, 3.0);
		v2.setCoordinate(3, -2.2);
		
		Vector v3 = VectorOperations.projection(v1, v2);
		Assertions.assertEquals(3, v3.getDimension());
		Assertions.assertTrue(3.0000000001 >= v3.getCoordinate(1));
		Assertions.assertTrue(2.9999999999 <= v3.getCoordinate(1));
		Assertions.assertTrue(3.0000000001 >= v3.getCoordinate(2));
		Assertions.assertTrue(2.9999999999 <= v3.getCoordinate(2));
		Assertions.assertEquals(0, v3.getCoordinate(3));
	}
	

}
