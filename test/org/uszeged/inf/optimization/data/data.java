package org.uszeged.inf.optimization.data;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.uszeged.inf.optimization.functions.Branin;

class data {

	@Test
	void testMatrixEye() {
		//eye()
		Matrix m1 = Matrix.eye(2);
		Assertions.assertEquals(1.0, m1.getCoordinate(1,1) );
		Assertions.assertEquals(1.0, m1.getCoordinate(2,2) );
		Assertions.assertEquals(0.0, m1.getCoordinate(1,2) );
		Assertions.assertEquals(0.0, m1.getCoordinate(2,1) );
	}
	
	@Test
	void testMatrixGetRows() {
		//getRows()
		Matrix m1 = Matrix.eye(2);
		Assertions.assertEquals(2, m1.getRows() );
		Assertions.assertEquals(2, m1.getCols() );
	}
	
	@Test
	void testMatrixCopyConst() {
		//Copy constructor
		Matrix m1 = Matrix.eye(2);
		Assertions.assertEquals(1.0, m1.getCoordinate(1,1) );
		Assertions.assertEquals(1.0, m1.getCoordinate(2,2) );
		Assertions.assertEquals(0.0, m1.getCoordinate(1,2) );
		Assertions.assertEquals(0.0, m1.getCoordinate(2,1) );
	}
	
	@Test
	void testMatrixSetCoordinate() {
		//setCoordinate
		Matrix m1 = Matrix.eye(2);
		m1.setCoordinate(1,1,5.0);
		Assertions.assertEquals(5.0, m1.getCoordinate(1,1) );
	}	
	
	@Test
	void testMatrixSetCoordinates() {
		//setCoordinates
		Matrix m1 = Matrix.eye(2);
		double[][] d1 = new double[2][2];
		m1.setCoordinates(d1);
		Assertions.assertEquals(0.0, m1.getCoordinate(1,1) );
		Assertions.assertEquals(0.0, m1.getCoordinate(2,2) );
	}	
	
	@Test
	void testMatrixGetCoordinates() {
		//getCoordinates
		Matrix m1 = Matrix.eye(2);
		double[][] d2 = m1.getCoordinates();
		Assertions.assertEquals(1.0, d2[0][0] );
		Assertions.assertEquals(1.0, d2[1][1] );
	}

	@Test
	void testMatrixLeftMul() {
		Matrix m1 = Matrix.eye(2);
		Matrix m2 = Matrix.eye(2);
		Matrix m3 = m1.leftMul(m2);
		
		Assertions.assertEquals(1.0, m3.getCoordinate(1,1) );
		Assertions.assertEquals(1.0, m3.getCoordinate(2,2) );
		Assertions.assertEquals(0.0, m3.getCoordinate(1,2) );
		Assertions.assertEquals(0.0, m3.getCoordinate(2,1) );
		
		//TODO: non-eye, (n,m)x(m,n), (n,m)x(k,n): k!=m 
	}
	
	@Test
	void testMatrixRightMul() {
		Matrix m1 = Matrix.eye(2);
		Matrix m2 = Matrix.eye(2);
		Matrix m3 = m1.rightMul(m2);
		
		Assertions.assertEquals(1.0, m3.getCoordinate(1,1) );
		Assertions.assertEquals(1.0, m3.getCoordinate(2,2) );
		Assertions.assertEquals(0.0, m3.getCoordinate(1,2) );
		Assertions.assertEquals(0.0, m3.getCoordinate(2,1) );
		
		//TODO: non-eye, (n,m)x(m,n), (n,m)x(k,n): k!=m 
	}
	
	@Test
	void testMatrixMul() {
		Matrix m1 = Matrix.eye(2);
		m1.mul(2.0);
		
		Assertions.assertEquals(2.0, m1.getCoordinate(1,1) );
		Assertions.assertEquals(2.0, m1.getCoordinate(2,2) );
		Assertions.assertEquals(0.0, m1.getCoordinate(1,2) );
		Assertions.assertEquals(0.0, m1.getCoordinate(2,1) );
	}
	
	@Test
	void testTranspose() {
		Matrix m1 = Matrix.eye(2);
		m1.setCoordinate(1,2,1.0);
		m1.setCoordinate(2,2,0.0);
		m1.transpose();
		
		Assertions.assertEquals(1.0, m1.getCoordinate(1,1) );
		Assertions.assertEquals(0.0, m1.getCoordinate(2,2) );
		Assertions.assertEquals(0.0, m1.getCoordinate(1,2) );
		Assertions.assertEquals(1.0, m1.getCoordinate(2,1) );
	}
	
	@Test
	void testMatrixAdd() {
		Matrix m1 = Matrix.eye(2);
		Matrix m2 = Matrix.eye(2);
		Matrix m3 = m1.add(m2);
		
		Assertions.assertEquals(2.0, m3.getCoordinate(1,1) );
		Assertions.assertEquals(2.0, m3.getCoordinate(2,2) );
		Assertions.assertEquals(0.0, m3.getCoordinate(1,2) );
		Assertions.assertEquals(0.0, m3.getCoordinate(2,1) );
		
		//TODO: non-eye, (n,m)+(n,m), (n,m)+(k,l): k!=n || m!=l 
	}
	
	@Test
	void testMatrixGetRow() {
		Matrix m1 = Matrix.eye(2);
		Vector v1 = m1.getRow(1);
		
		Assertions.assertEquals(1.0, v1.getCoordinate(1) );
		Assertions.assertEquals(0.0, v1.getCoordinate(2) );
	}
	
	@Test
	void testMatrixGetCol() {
		Matrix m1 = Matrix.eye(2);
		Vector v1 = m1.getCol(1);
		
		Assertions.assertEquals(1.0, v1.getCoordinate(1) );
		Assertions.assertEquals(0.0, v1.getCoordinate(2) );
	}
	
	@Test
	void testMatrixSetCol() {
		Matrix m1 = Matrix.eye(2);
		Vector v1 = new Vector(2);
		m1.setCol(1, v1);
		
		Assertions.assertEquals(0.0, m1.getCoordinate(1,1) );
		Assertions.assertEquals(0.0, m1.getCoordinate(2,1) );
	}
	
	@Test
	void testMatrixToVector() {
		Matrix m1 = new Matrix(1,2);
		Vector v1 = m1.toVector();
		
		Assertions.assertEquals(0.0, v1.getCoordinate(1) );
		Assertions.assertEquals(0.0, v1.getCoordinate(2) );
	}
	
	@Test
	void testMatrixToVectorStanding() {
		Matrix m1 = new Matrix(2,1);
		Vector v1 = m1.toVector(true);
		
		Assertions.assertEquals(0.0, v1.getCoordinate(1) );
		Assertions.assertEquals(0.0, v1.getCoordinate(2) );
	}
	
	@Test
	void testMatrixToString() {
		Matrix m1 = Matrix.eye(2);
		String s = m1.toString();
		//TODO: fix this
		Assertions.assertEquals("[[1,0];"+"\n"+"[0,1]]", s );
	}
	
	@Test
	void testPointConstr() {
		double[] d1 = new double[2];
		Point p1 = new Point(d1);
		
		Assertions.assertEquals(0.0, p1.getCoordinates()[0]);
		Assertions.assertEquals(0.0, p1.getCoordinates()[1]);
		Assertions.assertEquals(0.0, p1.getFunctionValue());
	}
	
	@Test
	void testPointConstrDim() {
		Point p1 = new Point(2);
		
		Assertions.assertEquals(0.0, p1.getCoordinates()[0]);
		Assertions.assertEquals(0.0, p1.getCoordinates()[1]);
		Assertions.assertEquals(0.0, p1.getFunctionValue());
	}
	
	@Test
	void testPointConstrVec() {
		Vector v1 = new Vector(2);
		v1.setCoordinate(1, 1.0);
		v1.setCoordinate(2, 0.5);
		Point p1 = new Point(v1);
		
		Assertions.assertEquals(1.0, p1.getCoordinates()[0]);
		Assertions.assertEquals(0.5, p1.getCoordinates()[1]);
		Assertions.assertEquals(0.0, p1.getFunctionValue());
	}
	
	@Test
	void testPointCopyConstr() {
		Point p1 = new Point(2);
		p1.setFunctionValue(2.0);
		p1.setCoordinate(1, 1.0);
		p1.setCoordinate(2, 0.5);
		Point p2 = new Point(p1);
		
		Assertions.assertEquals(1.0, p2.getCoordinates()[0]);
		Assertions.assertEquals(0.5, p2.getCoordinates()[1]);
		Assertions.assertEquals(2.0, p2.getFunctionValue());
	}
	
	@Test
	void testPointConstrFunctionValue() {
		double[] d1 = new double[2];
		Point p1 = new Point(d1, 1.0);
		
		Assertions.assertEquals(0.0, p1.getCoordinates()[0]);
		Assertions.assertEquals(0.0, p1.getCoordinates()[1]);
		Assertions.assertEquals(1.0, p1.getFunctionValue());
	}
	
	@Test
	void testPointSetFunctionValue() {
		double[] d1 = new double[2];
		Point p1 = new Point(d1);
		p1.setFunctionValue(2.0);
		
		Assertions.assertEquals(2.0, p1.getFunctionValue());
	}
	
	@Test
	void testPointSetFuncValByEval() {
		double[] d1 = new double[2];
		d1[0]=9.42478; d1[1]=2.475;
		Point p1 = new Point(d1);
		Branin a = new Branin();
		final double optBranin = 0.39;
		final double eps = 0.39;
		p1.setFunctionValue(a);
		
		Assertions.assertEquals(true, optBranin-eps < p1.getFunctionValue() );
		Assertions.assertEquals(true, optBranin+eps > p1.getFunctionValue() );
	}
	
	@Test
	void testPointToString() {
		Point p1 = new Point(2);
		Assertions.assertEquals("0.0 : [0,0]",p1.toString() );
	}
	
	@Test
	void testPointAscendCompComp() {
		Point p1 = new Point(2);
		Point p2 = new Point(2);
		
		Point.AscendingFunctionValueComparator a = new Point.AscendingFunctionValueComparator();
		Assertions.assertEquals(0, a.compare(p1, p2) );
		
		p2.setFunctionValue(1.0);
		Assertions.assertEquals(-1, a.compare(p1, p2) );
		
		p2.setFunctionValue(-1.0);
		Assertions.assertEquals(1, a.compare(p1, p2) );
	}
	
	@Test
	void testPointDescendCompComp() {
		Point p1 = new Point(2);
		Point p2 = new Point(2);
		
		Point.DescendingFunctionValueComparator a = new Point.DescendingFunctionValueComparator();
		Assertions.assertEquals(0, a.compare(p1, p2) );
		
		p2.setFunctionValue(-1.0);
		Assertions.assertEquals(-1, a.compare(p1, p2) );
		
		p2.setFunctionValue(1.0);
		Assertions.assertEquals(1, a.compare(p1, p2) );
	}
	
	@Test
	void testConstrainedFuncIsParamAccept() {
		Branin b = new Branin();
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(2);
		ConstrainedFunction cf = new ConstrainedFunction(b, v1, v2);
		Vector v3 = new Vector(2);
		Vector v4 = new Vector(1);
		Assertions.assertEquals(true, cf.isParameterAcceptable(v3, v4) );
	}
	
	@Test
	void testConstrainedFuncIsDimAccept() {
		Branin b = new Branin();
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(2);
		ConstrainedFunction cf = new ConstrainedFunction(b, v1, v2);
		Assertions.assertEquals(true, cf.isDimensionAcceptable(2) );
	}
	
	@Test
	void testConstrainedFuncToOriginalSp() {
		Branin b = new Branin();
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(2);
		v2.setCoordinate(1, 1.0);
		v2.setCoordinate(2, 1.0);
		ConstrainedFunction cf = new ConstrainedFunction(b, v1, v2);
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 1.0);
		v3.setCoordinate(2, 1.0);
		Vector v4 = cf.toOriginalSpace(v3);
		Assertions.assertEquals(1.0, v4.getCoordinate(1) );
		Assertions.assertEquals(1.0, v4.getCoordinate(2) );
	}
	
	@Test
	void testEvaluate() {
		Branin b = new Branin();
		Vector v1 = new Vector(2);
		Vector v2 = new Vector(2);
		v2.setCoordinate(1, 1.0);
		v2.setCoordinate(2, 1.0);
		ConstrainedFunction cf = new ConstrainedFunction(b, v1, v2);
		
		double eps=0.01;
		double opt=0.39;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 9.42478);
		v3.setCoordinate(2, 2.475);
		Assertions.assertEquals(true, opt-eps < cf.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > cf.evaluate(v3) );
	}
	
	@Test
	void testXOffsFuncDecIsParamAccept() {
		Branin b = new Branin();
		Vector v1 = new Vector(2);
		XOffsetFunctionDecorator df = new XOffsetFunctionDecorator(b, v1);
		Vector v3 = new Vector(2);
		Vector v4 = new Vector(1);
		Assertions.assertEquals(true, df.isParameterAcceptable(v3, v4) );
	}
	
	@Test
	void testXOffsFuncDecIsDimAccept() {
		Branin b = new Branin();
		Vector v1 = new Vector(2);
		XOffsetFunctionDecorator df = new XOffsetFunctionDecorator(b, v1);
		Assertions.assertEquals(true, df.isDimensionAcceptable(2) );
	}
	
	@Test
	void testXOffsFuncDecEvaluate() {
		Branin b = new Branin();
		Vector v1 = new Vector(2);
		XOffsetFunctionDecorator df = new XOffsetFunctionDecorator(b, v1);
		
		double eps=0.01;
		double opt=0.39;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 9.42478);
		v3.setCoordinate(2, 2.475);
		Assertions.assertEquals(true, opt-eps < df.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > df.evaluate(v3) );
	}
	
	@Test
	void testXScaleFuncDecEvaluate() {
		Branin b = new Branin();
		Vector v1 = new Vector(2);
		v1.setCoordinate(1, 1.0);
		v1.setCoordinate(2, 1.0);
		XScaleFunctionDecorator sf = new XScaleFunctionDecorator(b, v1);
		
		double eps=0.01;
		double opt=0.39;
		Vector v3 = new Vector(2);
		v3.setCoordinate(1, 9.42478);
		v3.setCoordinate(2, 2.475);
		Assertions.assertEquals(true, opt-eps < sf.evaluate(v3) );
		Assertions.assertEquals(true, opt+eps > sf.evaluate(v3) );
	}
	
	@Test
	void testXScaleFuncDecIsParamAccept() {
		Branin b = new Branin();
		Vector v1 = new Vector(2);
		XScaleFunctionDecorator df = new XScaleFunctionDecorator(b, v1);
		Vector v3 = new Vector(2);
		Vector v4 = new Vector(1);
		Assertions.assertEquals(true, df.isParameterAcceptable(v3, v4) );
	}
	
	@Test
	void testXScaleFuncDecIsDimAccept() {
		Branin b = new Branin();
		Vector v1 = new Vector(2);
		XScaleFunctionDecorator df = new XScaleFunctionDecorator(b, v1);
		Assertions.assertEquals(true, df.isDimensionAcceptable(2) );
	}
}
