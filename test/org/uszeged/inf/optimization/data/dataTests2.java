package org.uszeged.inf.optimization.data;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class dataTests2 {

	@Test
	void testCluster() {
		List l = new ArrayList();
		Vector v = new Vector(2);
		l.add(v);
		Cluster c = new Cluster();
		c.setElements(l);
		Assertions.assertEquals(null, c.getCenter());
		c.setCenter(v);
		Assertions.assertEquals(v, c.getCenter());
		Assertions.assertEquals(v,  c.getElements().get(0));
		c.hashCode();
		c.toString();
	}
	
	@Test
	void testParallelCluster() {
		Vector v = new Vector(2);
		ParallelCluster c = new ParallelCluster();
		c.add(v);
		Assertions.assertEquals(null, c.getCenter());
		c.setCenter(v);
		Assertions.assertEquals(v, c.getCenter());
		Assertions.assertEquals(v,  c.getElements().get(0));
		c.toString();
		Assertions.assertThrows(Exception.class, () -> c.hashCode());
	}
	
	@Test
	void testVector() {
		Vector v = new Vector(3);
		v = new Vector(2);
		Assertions.assertEquals(2, v.getDimension());
		
		double cords[] = {1.5, -4};
		v.setCoordinates(cords);
		Assertions.assertEquals(-4, v.getCoordinates()[1]);
		
		Vector v2 = v.clone();
		Assertions.assertEquals(2, v2.getDimension());
		v2.setCoordinate(2, 3);
		Assertions.assertFalse(v.equals(v2));
		
		Vector.setRandSeed(Vector.getRandSeed());
		
		Vector v3 = new Vector(3);
		Vector.randomize(v3);
		v2.setCoordinates(v3.getCoordinates());
		Assertions.assertEquals(3, v2.getDimension());
		Assertions.assertEquals(v2.getCoordinate(3), v3.getCoordinate(3));
		
		v = new Vector(4);
		Vector.gaussianRandomize(v);
		
		Vector v4 = new Vector(v);
		Assertions.assertEquals(v4.getCoordinate(2), v.getCoordinate(2));

		Assertions.assertThrows(Exception.class, () -> v3.setCoordinate(6, 42));
		Assertions.assertThrows(Exception.class, () -> v3.getCoordinate(75));
		Assertions.assertThrows(Exception.class, () -> Vector.scalarMul(v4, v3));
		
		Matrix m = new Matrix(1, 2);
		Vector v5 = new Vector(m);
		Assertions.assertEquals(2, v5.getDimension());
		
		m = v5.toMatrix();
		Assertions.assertEquals(1, m.getRows());
		m = v5.toMatrix(true);
		Assertions.assertEquals(2, m.getRows());
		
	}
	
	
}
