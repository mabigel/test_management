package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Levy in n dimensions.
 *
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Levy implements Function {

	public double evaluate (Vector x) {

		int dim = x.getDimension();

		double omega[] = new double[dim], sum = 0.0;


		for (int i = 0; i < dim; i++) {
			omega[i] = 1 + (x.getCoordinate(i + 1) - 1) / 4;
		}

		for (int i = 1; i <  dim; i++) {
			sum += Math.pow((omega[i - 1] - 1), 2) * (1 + 10 * Math.pow(Math.sin(Math.PI * omega[i - 1] + 1), 2));    
		}

		return Math.pow(Math.sin(Math.PI * omega[0]), 2) + sum + Math.pow((omega[dim - 1] - 1), 2) * (1 + Math.pow(Math.sin(2 * Math.PI * omega[dim - 1]), 2));

	}

	public boolean isParameterAcceptable(Vector lb, Vector ub) {

		return true;
	}

	public boolean isDimensionAcceptable(int dim) {

		return true;
	}

}
