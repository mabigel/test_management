package org.uszeged.inf.optimization.functions;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.util.FunctionTransform;

/**
 * Test function implementing the transformed function Ellipsoid in n dimensions.
 *
 * @author Dániel Zombori
 * @version 1.0
 * @since 1.0
 */
public class EllipsoidRot extends Ellipsoid {

	private FunctionTransform transformation;

	public EllipsoidRot(){
		transformation = FunctionTransform.singleton();
	}

	public double evaluate (Vector x) {
		x = transformation.rotVector(x);
        return transformation.rotValue(super.evaluate(x));
	}

}
