package org.uszeged.inf.optimization.functions;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Schaffer N. 2 in 2 dimensions.
 *
 * @author Dániel Zombori
 * @version 1.0
 * @since 1.0
 */
public class Schaffer implements Function {

	public double evaluate (Vector x) {
		double x1sqr = Math.pow(x.getCoordinate(1), 2);
		double x2sqr = Math.pow(x.getCoordinate(2), 2);

        double val1 = Math.pow(Math.sin(x1sqr - x2sqr), 2) - 0.5;
		double val2 = Math.pow(1 + 0.001 * (x1sqr + x2sqr), 2);

		double val = 0.5 + val1 / val2;

        return val;
	}

	public boolean isParameterAcceptable(Vector lb, Vector ub) {

		return true;
	}

	public boolean isDimensionAcceptable(int dim) {

		return dim == 2;
	}

}
