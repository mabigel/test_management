package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Rastrigin in n dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Rastrigin implements Function {

	public double evaluate (Vector x) {
	
		double sum = 0.0;
		
		for (int i = 1; i <= x.getDimension(); i++) {
			sum += Math.pow(x.getCoordinate(i), 2) - 10 * Math.cos(2 * Math.PI * x.getCoordinate(i));
		}
		
		return 10 * x.getDimension() + sum;
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return true;
	}
	
	public boolean isDimensionAcceptable(int dim) {
		
		return true;
	}

}