package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Zakharov in n dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Zakharov implements Function {

	public double evaluate (Vector x) {
	
		double sum1 = 0.0, sum2 = 0.0;
		
		for(int i = 1; i <= x.getDimension(); i++) {
			sum1 += Math.pow(x.getCoordinate(i), 2);
			sum2 += 0.5 * i * x.getCoordinate(i);
		}
	
		return sum1 + Math.pow(sum2, 2) + Math.pow(sum2, 4);
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return true;
	}
	
	public boolean isDimensionAcceptable(int dim) {
		
		return true;
	}

}