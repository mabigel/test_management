package org.uszeged.inf.optimization.functions;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.util.FunctionTransform;

/**
 * Test function implementing the transformed function Rosenbrock in n dimensions.
 *
 * @author Dániel Zombori
 * @version 1.0
 * @since 1.0
 */
public class RosenbrockRot extends Rosenbrock {

	private FunctionTransform transformation;

	public RosenbrockRot(){
		transformation = FunctionTransform.singleton();
	}

	public double evaluate (Vector x) {
		x = transformation.rotVector(x);
        return transformation.rotValue(super.evaluate(x));
	}

}
