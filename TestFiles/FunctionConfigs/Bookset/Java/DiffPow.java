package org.uszeged.inf.optimization.functions;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function DiffPow in n dimensions.
 *
 * @author Dániel Zombori
 * @version 1.0
 * @since 1.0
 */
public class DiffPow implements Function {

	public double evaluate (Vector x) {
        double val = 0;

        int n = x.getDimension();
        for(int i = 1; i <= x.getDimension(); i++){
            val += Math.pow(Math.abs(x.getCoordinate(i)), 2.0 + 4.0*(i-1.0)/(n-1.0));
        }

        return Math.sqrt(val);
	}

	public boolean isParameterAcceptable(Vector lb, Vector ub) {

		return true;
	}

	public boolean isDimensionAcceptable(int dim) {

		return true;
	}

}
