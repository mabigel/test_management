package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Schwefel in n dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Schwefel implements Function {

	public double evaluate (Vector x) {
	
		double sum = 0.0;
		
		for (int i = 1; i <= x.getDimension(); i++) {
			sum += x.getCoordinate(i) * Math.sin(Math.sqrt(Math.abs(x.getCoordinate(i))));
		}
		
		return 418.9829 * x.getDimension() - sum;
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return true;
	}
	
	public boolean isDimensionAcceptable(int dim) {
		
		return true;
	}

}