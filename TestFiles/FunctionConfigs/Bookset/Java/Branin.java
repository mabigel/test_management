package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Branin in 2 dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Branin implements Function {

	public double evaluate (Vector x) {
	
		double x1 = x.getCoordinate(1), x2 = x.getCoordinate(2);

		double a = 1.0, b = 5.1 / (4 * Math.pow(Math.PI, 2)), c = 5 / Math.PI, r = 6.0, s = 10.0, t = 1 / (8 * Math.PI);

		return a * Math.pow((x2 - b * Math.pow(x1, 2) + c * x1 - r) ,2) + s * (1 - t) * Math.cos(x1) + s;
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return isDimensionAcceptable(lb.getDimension());
	}
	
	public boolean isDimensionAcceptable(int dim) {
			
			return dim == 2;
	}

}
