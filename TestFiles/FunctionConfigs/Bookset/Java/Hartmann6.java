package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Hartmann in 6 dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Hartmann6 implements Function {

	public double evaluate(Vector x) {
	
		double A[][] = {{10.0, 3.0, 17.0, 3.5, 1.7, 8.0},
						{0.05, 10.0, 17.0, 0.1, 8.0, 14.0},
						{3.0, 3.5, 1.7, 10.0, 17.0, 8.0},
						{17.0, 8.0, 0.05, 10.0, 0.1, 14.0}};
				
		double P[][] = {{0.1312, 0.1696, 0.5569, 0.0124, 0.8283, 0.5886},
						{0.2329, 0.4135, 0.8307, 0.3736, 0.1004, 0.9991},
						{0.2348, 0.1451, 0.3522, 0.2883, 0.3047, 0.6650},
						{0.4047, 0.8828, 0.8732, 0.5743, 0.1091, 0.0381}};
										
		double alfa[] = {1.0, 1.2, 3.0, 3.2};
		
		double sumj, sum = 0.0;
	
		for(int i = 0; i < 4; i++) {
			sumj = 0.0;
			for(int j = 0; j < 6; j++) {
				sumj += A[i][j] * Math.pow((x.getCoordinate(j+1) - P[i][j]), 2);
			}
			sum += alfa[i] * Math.exp(-sumj);
		}

		return -sum;
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return isDimensionAcceptable(lb.getDimension());
	}
	
	public boolean isDimensionAcceptable(int dim) {
			
			return dim == 6;
	}

}