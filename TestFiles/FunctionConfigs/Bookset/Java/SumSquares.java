package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function SumSquares in n dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class SumSquares implements Function {

	public double evaluate (Vector x) {
	
		double sum = 0.0;
		
		for (int i = 1; i <= x.getDimension(); i++) {
			sum += i * Math.pow(x.getCoordinate(i), 2);
		}
		
		return sum;
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return true;
	}
	
	public boolean isDimensionAcceptable(int dim) {
		
		return true;
	}

}