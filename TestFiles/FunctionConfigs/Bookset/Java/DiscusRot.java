package org.uszeged.inf.optimization.functions;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;
import org.uszeged.inf.optimization.util.FunctionTransform;

/**
 * Test function implementing the transformed function Discus in n dimensions.
 *
 * @author Dániel Zombori
 * @version 1.0
 * @since 1.0
 */
public class DiscusRot extends Discus {

	private FunctionTransform transformation;

	public DiscusRot(){
		transformation = FunctionTransform.singleton();
	}

	public double evaluate (Vector x) {
		x = transformation.rotVector(x);
        return transformation.rotValue(super.evaluate(x));
	}

}
