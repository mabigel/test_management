package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Goldstein-price in 2 dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class GoldsteinPrice implements Function {

	public double evaluate (Vector x) {
	
		double x1 = x.getCoordinate(1), x2 = x.getCoordinate(2);
		
		double a = 1.0 + Math.pow((x1 + x2 + 1.0), 2) * (19.0 - 14.0 * x1 + 3 * Math.pow(x1, 2) - 14.0 * x2 + 6.0 * x1 * x2 + 3.0 * Math.pow(x2, 2));
		double b = 30.0 + Math.pow((2.0 * x1 - 3 * x2), 2) * (18.0 - 32.0 * x1 + 12.0 * Math.pow(x1, 2) + 48.0 * x2 - 36.0 * x1 * x2 + 27.0 * Math.pow(x2, 2));
		
		return a * b;
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return isDimensionAcceptable(lb.getDimension());
	}
	
	public boolean isDimensionAcceptable(int dim) {
			
			return dim == 2;
	}

}