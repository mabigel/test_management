package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Easom in 2 dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Easom implements Function {

	public double evaluate (Vector x) {

		double x1 = x.getCoordinate(1), x2 = x.getCoordinate(2);

		return - Math.cos(x1) * Math.cos(x2) * Math.exp(- (Math.pow((x1 - Math.PI), 2) + Math.pow((x2 - Math.PI), 2)));
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return isDimensionAcceptable(lb.getDimension());
	}
	
	public boolean isDimensionAcceptable(int dim) {
			
			return dim == 2;
	}

}