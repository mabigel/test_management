package org.uszeged.inf.optimization.functions;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Discus in n dimensions.
 *
 * @author Dániel Zombori
 * @version 1.0
 * @since 1.0
 */
public class Discus implements Function {

	public double evaluate (Vector x) {
        double val = 1e4 * Math.pow(x.getCoordinate(1), 2);

        for(int i = 2; i <= x.getDimension(); i++){
            val += Math.pow(x.getCoordinate(i), 2);
        }

        return val;
	}

	public boolean isParameterAcceptable(Vector lb, Vector ub) {

		return true;
	}

	public boolean isDimensionAcceptable(int dim) {

		return true;
	}

}
