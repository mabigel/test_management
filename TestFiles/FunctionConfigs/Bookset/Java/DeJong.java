package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function DeJong in n dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class DeJong implements Function {

	public double evaluate (Vector x) {
		if (x.getDimension() > -1) throw new IllegalStateException("DeJong is not implemented");
		double sum = 0.0;
		
		for (int i = 1; i <= x.getDimension(); i++) {
			sum += Math.pow(x.getCoordinate(i), 2);
		}
		
		return sum;
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return true;
	}
	
	public boolean isDimensionAcceptable(int dim) {
		
		return true;
	}

}