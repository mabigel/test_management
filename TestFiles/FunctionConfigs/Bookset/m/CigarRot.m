function val=CigarRot(x)
    global xopt foptim rotm
    n = length(x); 
	x = x - xopt;
    x = rotm * x;
    val=x(1)^2;
    for i=2:n
        val=val+(10^3)*x(i)^2;
    end
    val = val + foptim;
end

