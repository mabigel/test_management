function y = Sharpridge(x)
    n = length(x);
    y=x(1).^2 + 100 * sqrt(sum(x(2:end).^2, 1));
end

