function val = diffpow(x)
    val=0;
    n=length(x);
    for i=1:n
        val=val + abs(x(i))^(2 + 4*(i-1)/(n-1));
    end
    val = sqrt(val);
end
