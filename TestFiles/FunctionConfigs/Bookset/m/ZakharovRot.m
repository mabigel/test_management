function y = ZakharovRot(x)
    global xopt foptim rotm
    n = length(x);
    x = x - xopt;
    x = rotm * x;
	s1 = 0;
	s2 = 0;
	for j = 1:n
		s1 = s1+x(j)^2;
		s2 = s2+0.5*j*x(j);
	end
	y = s1+s2^2+s2^4+ foptim;
end