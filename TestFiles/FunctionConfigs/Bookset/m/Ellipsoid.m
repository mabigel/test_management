function val=Ellipsoid(x)
val=0;
n=length(x);
for i=1:n
    val=val+(10^(4*(i-1)/(n-1)))*x(i)^2;
end
%x from [-5 -5] 