function val=DiscusRot(x)
    global xopt foptim rotm
    n=length(x);
    x = x - xopt;
    x = rotm * x;
    val=10^4*x(1)^2;
    for i=2:n
        val=val+x(i)^2;
    end
    val = val + foptim;
end