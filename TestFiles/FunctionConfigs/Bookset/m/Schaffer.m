function y = Schaffer(x)
 
fact1 = (sin(x(1)^2-x(2)^2))^2 - 0.5;
fact2 = (1 + 0.001*(x(1)^2+x(2)^2))^2;

y = 0.5 + fact1/fact2;