function val=Cigar(x)

n = length(x);
val=x(1)^2;

for i=2:n
    val=val+(10^3)*x(i)^2;
end
%x from [-5 5]
