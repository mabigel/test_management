function y = RosenbrockRot(x)
	global xopt foptim rotm
    n = length(x);
    x = x - xopt;
    x = rotm * x;

	sum = 0;
	for j = 1:n-1;
		sum = sum+100*(x(j)^2-x(j+1))^2+(x(j)-1)^2;
	end
	y = sum + foptim;
end