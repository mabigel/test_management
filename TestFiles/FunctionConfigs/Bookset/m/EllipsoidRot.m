function val=EllipsoidRot(x)
    global xopt foptim rotm
    n=length(x);
    x = x - xopt;
    x = rotm * x;
    val=0;
    for i=1:n
        val=val+(10^(4*(i-1)/(n-1)))*x(i)^2;
    end
    val = val + foptim;
end