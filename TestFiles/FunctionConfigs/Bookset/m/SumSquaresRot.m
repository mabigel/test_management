function y = SumSquaresRot(x)
    global xopt foptim rotm
    n=length(x);
    x = x - xopt;
    x = rotm * x;
s = 0;
for j = 1:n  
    s=s+j*x(j)^2; 
end
y = s + foptim;
end
