nc = system("awk '{ print NF; exit }' ".filename)
set terminal png size 1500,1000 enhanced
set key autotitle columnhead
set output "res.png" 
set yrange [-0.1:1.1]
set key outside

plot for [i=2:nc] filename using (column(0)):i:xtic(1) ti col
