#!/bin/bash
open_sem(){
    mkfifo pipe-$$
    exec 3<>pipe-$$
    rm pipe-$$
    local i=$1
    for((;i>0;i--)); do
        printf %s 000 >&3
    done
}
run_with_lock(){
    local x
    read -u 3 -n 3 x && ((0==x)) || exit $x
    (
	echo $1
	echo $2
	i=$1
	j=$2
	for d in $(seq $runcount)
	do
		java -cp global.jar:libs/* Calculate -o TestGlobalConfigs/$i.xml -f TestInputFiles/$j.bnd >> "results/$i$j.res"
	done
	printf '%.3d' $? >&3
    )&
}

N=20
open_sem $N

runcount=100

if [ "$1" != "" ]; then
	runcount=$1
fi

rm -rf results
mkdir results
for i in TestGlobalConfigs/*.xml 
do 
	i=${i##*/}
	i=${i%.*}
	for j in TestInputFiles/*.bnd
	do
		j=${j##*/}
		j=${j%.*}

		run_with_lock $i $j
	done
done

for job in `jobs -p`
do
   wait $job
done

for i in TestGlobalConfigs/*.xml 
do 
	i=${i##*/}
	i=${i%.*}
	for j in TestInputFiles/*.bnd
	do
		j=${j##*/}
		j=${j%.*}
		echo -n "$i $j" >>results/res.txt
		awk -v stat="mean" -v fevlimit=100000 -f fullStat.awk "results/$i$j.res" >>results/res.txt	
	done
done

awk -f simpleTable.awk "results/res.txt" >>results/simpleTable.txt

awk -f normalize.awk "results/res.txt" >>results/normalize.txt
awk -f createDiagrams.awk "results/allEvas.txt" > "results/plottable.txt"
gnuplot -e "filename='results/plottable.txt'" EvaFigure.pl

