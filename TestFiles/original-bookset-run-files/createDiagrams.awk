BEGIN {
	regex = "^[0-9]+([.][0-9]+)?$"
	i=2
	j=2
	array[1,1] = "Percentages"
}
{
	if($1 !~ regex) {
		if( $1 != array[1,j-1]) {
			array[1,j] = $1;
			j = j+1;
			i = 2;
		}
	} else {
		array[i,j-1] = $1;
		i = i+1;
		temp = $1
	}
	funcs = j-1;
	rows = i-1;
}
END {
	for(i = 2; i <= rows; i++) {
		array[i,1] = (i-1)/(rows-1);
	}
	
	for(j = 2; j <= funcs; j++) {
		k = 1;
		for(i = 2; i <= rows; i++) {
			sort[k] = array[i,j];
			k=k+1;
		} 
		asort(sort);
		k = 1;
		for(i = 2; i <= rows; i++) {
			array[i,j] = sort[k];
			k=k+1;
		} 
	}
	
	for(j = 1; j <= funcs; j++) {
		printf "%s ", array[1,j]
	}
	printf "\n"
	for(i = 2; i <= rows; i++) {
		for(j = 1; j <= funcs; j++) {
			printf "%f ", array[i,j]
		}
		printf "\n"
	}
}