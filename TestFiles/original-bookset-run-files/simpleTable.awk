BEGIN {
}
{
	F[$1]=1
	S[$2]=1
	A[$1" "$2]=$3
}
END {

	printf "func"
	for (i in F) {
		printf " "i
	}
	print ""
	
	for (j in S) {
		printf j
		for (i in F) {
			printf " "(A[i" "j])
		}
		print ""
	}
}