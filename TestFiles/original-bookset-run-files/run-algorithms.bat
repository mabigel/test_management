@echo off

SET runcount=10;

IF "%~1"=="" GOTO endparse
REM Execution count
SET runcount=%~1

:endparse

rmdir results /q 2>nul
mkdir results
for /f %%i in ('dir /b TestGlobalConfigs\*.xml') do (
	echo ***** %%i *****
	for /f %%j in ('dir /b TestInputFiles\*') do (
		echo %%j
		for /L %%d in (1,1,%runcount%) do (
			call java -cp global.jar;libs\* Calculate -o TestGlobalConfigs\%%i -f TestInputFiles\%%j >> "results\%%~ni %%~nj.res"
			echo|set /p="."
		)
		echo.
		echo | set /p="%%~ni %%~nj" >>results\res.txt
		echo %%~ni >> "results/allEvas.txt"
		awk -v stat="mean" -f fullStat.awk "results\%%~ni %%~nj.res" >>results\res.txt
	)
)
awk -f normalize.awk "results\res.txt" >>results\normalize.txt
awk -f createDiagrams.awk "results\allEvas.txt" > "results\plottable.txt"
gnuplot -e "filename='results\plottable.txt'" EvaFigure.pl
pause
echo on
