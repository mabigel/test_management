nc = system('awk "{ print NF; exit }" results/plottable.txt')
set terminal png size 1500,1000 enhanced
set output "results/res.png"
set key autotitle columnhead
set yrange [0:1.05]
set xrange [0:100000]
set ylabel "Proportion of solved problems"
set xlabel "Number of function evaluations"

plot for [i=2:nc] filename using i:1 with lines title columnheader(i)