#!/bin/bash

runcount=10

if [ "$1" != "" ]; then
	runcount=$1
fi

rm -rf results
mkdir results
for i in TestGlobalConfigs/*.xml 
do 
	i=${i##*/}
	i=${i%.*}
	echo "***** $i *****"
	for j in TestInputFiles/*.bnd
	do
		j=${j##*/}
		j=${j%.*}
		echo $j
		for d in $(seq $runcount)
		do
			java -cp global.jar:libs/* Calculate -o TestGlobalConfigs/$i.xml -f TestInputFiles/$j.bnd >> "results/$i$j.res"
			echo -n .
		done
		echo ""
		echo -n "$i $j " >>results/res.txt
		echo $i >> results/allEvas.txt
		awk -v stat="mean" -f fullStat.awk "results/$i$j.res" >>results/res.txt	
	done
done
#awk -f simpleTable.awk "results/res.txt" >>results/simpleTable.txt
awk -f normalize.awk "results/res.txt" >>results/normalize.txt
awk -f createDiagrams.awk "results/allEvas.txt" > results/plottable.txt
#gnuplot -e "filename='results\plottable.txt'" EvaFigure.pl
#gnuplot -e 'filename="results/normalize.txt"' Figure.pl
