BEGIN {
	if (length(stat) == 0) {
		stat="mean"
	}
	if (length(fevlimit) == 0) {
		fevlimit="off"
	}
}
{
	total=total+1
	col=NF;
	if (fevlimit=="off" || fevlimit > $1) {
		n=n+1;
		printf "%f\n", $1 >> "results/allEvas.txt"
		for (i = 1; i <= NF; i++) {	
			if (max[i]<$i) {
				max[i]=$i
			}
			if (min[i] == "") {
				min[i]=$i
			}
			if (min[i]>$i) {
				min[i]=$i
			}
			sumx[i]=sumx[i]+$i
			sumx2[i]=sumx2[i]+$i*$i
		}
	}
}
END {
	if (stat=="max") {
		for (i=1; i<=col; i++) {
			printf " " max[i];
		}
		printf "%f\n", max[1] >> "results/shortEvas.txt"
	}
	if (stat=="min") {
		for (i=1; i<=col; i++) {
			printf " " min[i];
		}
		printf "%f\n", min[1] >> "results/shortEvas.txt"
	}
	if (stat=="mean") {
		for (i=1; i<=col; i++) {
			if (n > 0) {
				printf " " sumx[i]/n;
			} else {
				printf " 0";
			}
		}
		printf "%f\n", sumx[1] >> "results/shortEvas.txt"
	}
	if (fevlimit != "off"){
		printf " " n/total;
	}
	print ""
}