BEGIN {
}
{
	F[$1]=1
	S[$2]=1
	A[$1" "$2]=$3
}
END {
	for (j in S) {
		for (i in F) {
			if (Amin[j] == "")
				Amin[j]=A[i" "j]
			if (Amin[j]>A[i" "j])
				Amin[j]=A[i" "j]
			if (Amax[j]<A[i" "j])
				Amax[j]=A[i" "j]
		}
	}

	printf "func"
	for (i in F) {
		printf " "i
	}
	print ""
	
	for (j in S) {
		printf j
		for (i in F) {
			if (Amax[j]==Amin[j]) {
				printf " 0.5"
			} else {
				printf " "(A[i" "j]-Amin[j])/(Amax[j]-Amin[j])
			}
		}
		print ""
	}
}