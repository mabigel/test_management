package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Hartmann in 3 dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Hartmann3 implements Function {

	public double evaluate (Vector x) {
	
		double A[][] = {{3.0, 10.0, 30.0},
						{0.1, 10.0, 35.0},
						{3.0, 10.0, 30.0},
						{0.1, 10.0, 35.0}};
				
		double P[][] = {{0.36890, 0.11700, 0.26730},
						{0.46990, 0.43870, 0.74700},
						{0.10910, 0.87320, 0.55470},
						{0.03815, 0.57430, 0.88280}};
										
		double alfa[] = {1.0, 1.2, 3.0, 3.2};
		
		double sumj, sum = 0.0;
	
		for(int i = 0; i < 4; i++) {
			sumj = 0.0;
			for(int j = 0; j < 3; j++) {
				sumj += A[i][j] * Math.pow((x.getCoordinate(j+1) - P[i][j]), 2);
			}
			sum += alfa[i] * Math.exp(-sumj);
		}

		return -sum;
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return isDimensionAcceptable(lb.getDimension());
	}
	
	public boolean isDimensionAcceptable(int dim) {
			
			return dim == 3;
	}

}