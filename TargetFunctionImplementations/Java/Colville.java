package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Colville in 4 dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Colville implements Function {

	public double evaluate (Vector x) {
	
		double x1 = x.getCoordinate(1), x2 = x.getCoordinate(2), x3 = x.getCoordinate(3), x4 = x.getCoordinate(4);
		
		return 100 * Math.pow((Math.pow(x1, 2) - x2), 2) + Math.pow((x1 - 1), 2) + Math.pow((x3 - 1), 2) + 90 * Math.pow((Math.pow(x3, 2) - x4), 2) + 10.1 * (Math.pow((x2 - 1), 2) + Math.pow((x4 - 1), 2)) + 19.8 * (x2 - 1) * (x4 - 1);
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return isDimensionAcceptable(lb.getDimension());
	}
	
	public boolean isDimensionAcceptable(int dim) {
			
			return dim == 4;
	}

}