package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Ackley in n dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Ackley implements Function {

	public double evaluate (Vector x) {
	
		double a = 20.0, b = 0.2, c = 2 * Math.PI, sum1 = 0.0, sum2 = 0.0;
		
		for(int i = 1; i <= x.getDimension(); i++) {
			sum1 = Math.pow(x.getCoordinate(i), 2);
			sum2 = Math.cos(c * x.getCoordinate(i));
		}
		
		return -a * Math.exp(-b * Math.sqrt(1 / x.getDimension() * sum1)) - Math.exp(1 / x.getDimension() * sum2) + a + Math.exp(1);
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return true;
	}
	
	public boolean isDimensionAcceptable(int dim) {
		
		return true;
	}

}