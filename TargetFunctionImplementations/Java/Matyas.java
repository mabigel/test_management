package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Matyas in 2 dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Matyas implements Function {

	public double evaluate (Vector x) {

		double x1 = x.getCoordinate(1), x2 = x.getCoordinate(2);

		return 0.26 * (Math.pow(x1, 2) + Math.pow(x2, 2)) - 0.48 * x1 * x2;
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return isDimensionAcceptable(lb.getDimension());
	}
	
	public boolean isDimensionAcceptable(int dim) {
			
			return dim == 2;
	}

}