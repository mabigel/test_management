package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Beale in 2 dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Beale implements Function {

	public double evaluate (Vector x) {
	
		double x1 = x.getCoordinate(1), x2 = x.getCoordinate(2);
		
		return Math.pow((1.5 - x1 * (1 - x2)), 2) + Math.pow((2.25 - x1 * (1 - Math.pow(x2, 2))), 2) + Math.pow((2.625 - x1 * (1 - Math.pow(x2, 3))), 2);

	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return isDimensionAcceptable(lb.getDimension());
	}
	
	public boolean isDimensionAcceptable(int dim) {
			
			return dim == 2;
	}

}