package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Power in 4 dimensions.
 * 
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Power implements Function {

	public double evaluate (Vector x) {
		
		double sum1 = 0.0, sum2 = 0.0;
		
		double b[] = {8.0, 18.0, 44.0, 114.0};
		
		for (int i = 1; i <= 4; i++) {
			for (int j = 1; j <= 4; j++) {
				sum1 += Math.pow(x.getCoordinate(j), i);
			}
			sum2 += Math.pow((sum1 - b[i - 1]), 2);
		}
		
		return sum2;
		
	}
	
	public boolean isParameterAcceptable(Vector lb, Vector ub) {
	
		return isDimensionAcceptable(lb.getDimension());
	}
	
	public boolean isDimensionAcceptable(int dim) {
			
			return dim == 4;
	}

}