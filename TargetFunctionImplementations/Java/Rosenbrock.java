package org.uszeged.inf.optimization.functions;

import java.lang.Math;

import org.uszeged.inf.optimization.data.Function;
import org.uszeged.inf.optimization.data.Vector;

/**
 * Test function implementing the function Rosenbrock in n dimensions.
 *
 * @author Abigél Mester
 * @version 1.0
 * @since 1.0
 */
public class Rosenbrock implements Function {
	public double evaluate (Vector x) {
		
		double sum = 0.0;

		for(int i = 1; i < x.getDimension(); i++) {
			sum += 100.0 * (Math.pow((x.getCoordinate(i+1) - Math.pow(x.getCoordinate(i), 2)), 2)) + Math.pow((x.getCoordinate(i) - 1.0), 2);
		}

		return sum;

	}

	public boolean isParameterAcceptable(Vector lb, Vector ub) {

		return true;
	}

	public boolean isDimensionAcceptable(int dim) {

		return true;
	}

}
